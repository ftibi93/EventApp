import database.*
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.routing.route
import io.ktor.routing.routing
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import routes.*
import com.tiborfarago.resource_module.utils.TestData
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.math.BigDecimal

fun Application.main() {
    install(CallLogging)

    Database.connect("jdbc:mysql://localhost/event_database", driver = "com.mysql.jdbc.Driver", user = "user", password = "user")

    transaction {
        SchemaUtils.createMissingTablesAndColumns(Users)
        SchemaUtils.createMissingTablesAndColumns(Events, Locations, UserToEvents, Categories)

        if (Locations.selectAll().count() == 0) {
            Locations.insert {
                it[id] = 1
                it[name] = "Egy hely"
                it[city] = "varos"
                it[address] = "cim"
                it[latitude] = BigDecimal(12)
                it[longitude] = BigDecimal(12)
                it[mod_date] = DateTime.now()
            }
        }

        if (Users.selectAll().count() == 0) {
            TestData.generateUsers().forEach { user ->
                Users.insert {
                    it[name] = user.name
                    it[password] = user.password
                    it[mod_date] = user.modDate
                }
            }

            Users.insert {
                it[name] = "admin"
                it[password] = BCrypt.hashpw("admin", BCrypt.gensalt(10))
                it[mod_date] = DateTime()
                it[permission] = 999
            }
        }

        if (Categories.selectAll().count() == 0) {
            TestData.generateCategories().forEach { category ->
                Categories.insert {
                    it[id] = category.id
                    it[name] = category.name
                    it[mod_date] = category.modDate
                }
            }
        }

        if (Events.selectAll().count() == 0) {
            TestData.generateEvents().forEach { event ->
                Events.insert {
                    it[categoryId] = event.categoryId
                    it[locationId] = 1
                    it[name] = event.name
                    it[date] = event.date
                    it[description] = event.description
                    it[schedule] = event.schedule
                    it[mod_date] = event.modDate
                }
            }
        }
    }

    routing {
        route("api") {
            UserAdmin(this).init()
            EventRoute(this).init()
            CategoryRoute(this).init()
            AdminRoute(this).init()
            LocationRoute(this).init()
            UserRoute(this).init()
            FileRoute(this).init()
        }
    }
}