package utils

import com.tiborfarago.resource_module.moshi.MoshiObject
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.sql.ResultSet
import java.util.*

fun <T> String.execAndMap(transform: (ResultSet) -> T): List<T> {
    val result = arrayListOf<T>()
    TransactionManager.current().exec(this) { rs ->
        while (rs.next()) {
            result += transform(rs)
        }
    }
    return result
}

// val idWithModDateList = call.receiveText()
// val moshi = Moshi.Builder().add(DateTimeMoshiAdapter()).build()
// val adapter2 = moshi.adapter<LoginRequest>(LoginRequest::class.java)
// val request = adapter2.fromJson(idWithModDateList)
inline fun <reified T> String.toObject(): T? =
        MoshiObject.moshiObject.adapter<T>(T::class.java).fromJson(this)


// val moshi = Moshi.Builder().add(DateTimeMoshiAdapter()).build()
// val loginResponse = LoginResponse(id, modDate, userToEvents)
// val adapter = moshi.adapter<LoginResponse>(LoginResponse::class.java)
// call.respondText(adapter.toJson(loginResponse))
inline fun <reified T> T.toJsonString(): String =
        MoshiObject.moshiObject.adapter<T>(T::class.java).toJson(this)

fun InputStream.convertToString(): String {
    val buffer = ByteArray(1024)
    val fileOutputStream = ByteArrayOutputStream()

    while (true) {
        val byteCount = this.read(buffer)
        if (byteCount < 0) break
        fileOutputStream.write(buffer, 0, byteCount)
    }

    return fileOutputStream.toString("UTF-8")
}

fun ResultSet.getHungarianDateTime(fieldName: String): DateTime = DateTime
        .parse(this.getString(fieldName).substring(IntRange(0, 18)), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"))
        .withZoneRetainFields(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Europe/Budapest")))