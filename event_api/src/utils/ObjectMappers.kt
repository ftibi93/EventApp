package utils

import com.tiborfarago.resource_module.classes.NewEvent
import com.tiborfarago.resource_module.classes.NewLocation
import com.tiborfarago.resource_module.room.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.sql.ResultSet
import java.util.*


inline fun <reified T> ResultSet.mapToObject(): T {
    return when (T::class.java) {
        Event::class.java -> ObjectMappers.mapToEvent(this) as T
        UserToEvents::class.java -> ObjectMappers.mapToUserToEvents(this) as T
        NewEvent::class.java -> ObjectMappers.mapToNewEventsResponse(this) as T
        NewLocation::class.java -> ObjectMappers.mapToNewLocationsResponse(this) as T
        Location::class.java -> ObjectMappers.mapToLocationDetails(this) as T
        Category::class.java -> ObjectMappers.mapToCategory(this) as T
        User::class.java -> ObjectMappers.mapToUsers(this) as T
        else -> Unit as T
    }
}

object ObjectMappers {
    @PublishedApi
    internal fun mapToEvent(resultSet: ResultSet) = Event(
            resultSet.getInt(Event.ID),
            resultSet.getInt(Event.LOCATION_ID),
            resultSet.getInt(Event.CATEGORY_ID),
            resultSet.getString(Event.NAME),
            resultSet.getHungarianDateTime(Event.DATE),
            resultSet.getString(Event.DESCRIPTION),
            resultSet.getString(Event.SCHEDULE),
            DateTime(resultSet.getDate(Event.MOD_DATE)).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToUserToEvents(resultSet: ResultSet) = UserToEvents(
            resultSet.getInt(UserToEvents.USER_ID),
            resultSet.getInt(UserToEvents.EVENT_ID),
            DateTime(resultSet.getDate(UserToEvents.MOD_DATE)).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToNewEventsResponse(resultSet: ResultSet) = NewEvent(
            resultSet.getInt("e.id"),
            resultSet.getInt("e.location_id"),
            resultSet.getInt("e.category_id"),
            resultSet.getString("e.name"),
            resultSet.getHungarianDateTime("e.date"),
            DateTime(resultSet.getDate("e.mod_date")).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToNewLocationsResponse(resultSet: ResultSet) = NewLocation(
            resultSet.getInt("l.id"),
            resultSet.getString("l.name"),
            resultSet.getString("l.city"),
            DateTime(resultSet.getDate("l.mod_date")).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToLocationDetails(resultSet: ResultSet) = Location(
            resultSet.getInt(Location.ID),
            resultSet.getString(Location.NAME),
            resultSet.getString(Location.CITY),
            resultSet.getString(Location.ADDRESS),
            resultSet.getBigDecimal(Location.LONGITUDE),
            resultSet.getBigDecimal(Location.LATITUDE),
            DateTime(resultSet.getDate(Location.MOD_DATE)).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToCategory(resultSet: ResultSet) = Category(
            resultSet.getInt(Category.ID),
            resultSet.getString(Category.NAME),
            DateTime(resultSet.getDate(Category.MOD_DATE)).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )

    @PublishedApi
    internal fun mapToUsers(resultSet: ResultSet) = User(
            resultSet.getInt(User.ID),
            resultSet.getString(User.NAME),
            resultSet.getString(User.PASSWORD),
            resultSet.getInt(User.PERMISSION),
            DateTime(resultSet.getDate(User.MOD_DATE)).toDateTime(DateTimeZone.forTimeZone(TimeZone.getDefault()))
    )
}