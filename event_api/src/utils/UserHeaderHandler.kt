package utils

import repositories.UserRepository
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.request.header
import io.ktor.response.header
import io.ktor.util.pipeline.PipelineContext

fun PipelineContext<Unit, ApplicationCall>.handleUserHeader(): List<Int> {
    if (call.request.header("User-Ids").isNullOrEmpty()) return emptyList()

    val headers = this.call.request.header("User-Ids").orEmpty().split(",").map { it.trim().toInt() }.toMutableList()
    val existingUsers = UserRepository.getExistingUserIds(headers.joinToString())

    headers.removeAll(existingUsers)
    call.response.header("User-Ids", headers.joinToString())

    return existingUsers
}