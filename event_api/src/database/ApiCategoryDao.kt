package database

import com.tiborfarago.resource_module.room.Category
import utils.ObjectMappers
import utils.execAndMap
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update

object ApiCategoryDao {
    fun getAllCategories() = """
        SELECT * FROM ${Category.TABLE_NAME}
    """.trimIndent().execAndMap { ObjectMappers.mapToCategory(it) }

    fun getCategoryById(id: Int) = """
        SELECT * FROM ${Category.TABLE_NAME} WHERE ${Category.ID} = $id
    """.trimIndent().execAndMap { ObjectMappers.mapToCategory(it) }

    fun insertCategory(category: Category) {
        Categories.insert {
            it[Categories.id] = category.id
            it[Categories.name] = category.name
            it[Categories.mod_date] = category.modDate
        }
    }

    fun updateCategory(category: Category) {
        Categories.update({ Categories.id eq category.id }) { cat ->
            cat[Categories.name] = category.name
            cat[Categories.mod_date] = category.modDate
        }
    }

    fun deleteCategory(id: Int) {
        Categories.deleteWhere { Categories.id eq id }
    }

    fun isCategoryExists(id: Int): Boolean = Categories.select { Categories.id eq id }.count() != 0
}