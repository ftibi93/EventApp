package database

import com.tiborfarago.resource_module.room.Location
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.update

object ApiLocationDao {

    fun insertLocation(location: Location) {
        Locations.insert {
            it[id] = location.id
            it[name] = location.name
            it[city] = location.city
            it[address] = location.address
            it[latitude] = location.latitude
            it[longitude] = location.longitude
            it[mod_date] = location.modDate
        }
    }

    fun updateLocation(location: Location) {
        Locations.update({ Locations.id eq location.id }) {
            it[id] = location.id
            it[name] = location.name
            it[city] = location.city
            it[address] = location.address
            it[latitude] = location.latitude
            it[longitude] = location.longitude
            it[mod_date] = location.modDate
        }
    }

    fun deleteLocation(id: Int) {
        Locations.deleteWhere { Locations.id eq id }
    }

}