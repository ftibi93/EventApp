package database

import com.tiborfarago.resource_module.classes.NewEvent
import com.tiborfarago.resource_module.classes.NewLocation
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.UserToEvents
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import utils.execAndMap
import utils.mapToObject
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert

object ApiDao {

    fun getUserToEventsByUserId(userId: Int): List<UserToEvents> =
            "SELECT * FROM ${UserToEvents.TABLE_NAME} WHERE ${UserToEvents.USER_ID} = \"$userId\"".execAndMap {
                it.mapToObject<UserToEvents>()
            }

    fun getAllUserToEvents() = """
        SELECT * FROM ${UserToEvents.TABLE_NAME}
    """.trimIndent().execAndMap { it.mapToObject<UserToEvents>() }

    fun getAllNewEvents(latestId: Int) = """
        SELECT e.id, e.location_id, e.category_id, e.name, e.date, e.mod_date FROM ${Event.TABLE_NAME} e WHERE e.id > $latestId
    """.trimIndent().execAndMap { it.mapToObject<NewEvent>() }

    fun getNewLocations(locationIdList: String) = """
        SELECT l.id, l.name, l.city, l.mod_date FROM ${Location.TABLE_NAME} l WHERE l.id IN ($locationIdList)
    """.trimIndent().execAndMap {
        it.mapToObject<NewLocation>()
    }

    fun getBaseEventsWithIds(eventIdList: String?) = """
        SELECT e.id, e.location_id, e.category_id, e.name, e.date, e.mod_date FROM ${Event.TABLE_NAME} e
        WHERE e.id IN ($eventIdList)
    """.trimIndent().execAndMap { it.mapToObject<NewEvent>() }

    fun getEventsWithIds(eventIdList: String?) = """
        SELECT * FROM ${Event.TABLE_NAME} e
        WHERE e.id IN ($eventIdList)
    """.trimIndent().execAndMap { it.mapToObject<Event>() }

    fun getExistingUserIds(userIdList: String) = """
        SELECT id FROM ${User.TABLE_NAME} WHERE id IN ($userIdList)
    """.trimIndent().execAndMap { it.getInt("id") }

    fun getLocationDetails(locationId: Int) = """
        SELECT * FROM ${Location.TABLE_NAME} WHERE ${Location.ID} = $locationId
    """.trimIndent().execAndMap { it.mapToObject<Location>() }

    fun getBaseLocationsWithIds(locationIdList: String?) = """
        SELECT l.id, l.name, l.city, l.mod_date FROM ${Location.TABLE_NAME} l WHERE l.id IN ($locationIdList)
    """.trimIndent().execAndMap { it.mapToObject<NewLocation>() }

    fun getLocationsWithIds(locationIdList: String?) = """
        SELECT * FROM ${Location.TABLE_NAME} WHERE id IN ($locationIdList)
    """.trimIndent().execAndMap { it.mapToObject<Location>() }

    fun insertUserToEvents(userToEvents: List<UserToEvents>) {
        userToEvents.forEach { userToEvents ->
            database.UserToEvents.insert {
                it[userId] = userToEvents.userId
                it[eventId] = userToEvents.eventId
                it[mod_date] = userToEvents.modDate
            }
        }
    }

    fun insertUser(user: User) {
        Users.insert {
            it[Users.id] = user.id
            it[Users.name] = user.name
            it[Users.password] = user.password
            it[Users.mod_date] = user.modDate
        }
    }

    fun deleteUser(id: Int) {
        Users.deleteWhere { Users.id eq id }
    }

}