package database

import repositories.EventRepository
import com.tiborfarago.resource_module.room.Event
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update

object ApiEventDao {

    fun insertEvent(event: Event) {
        Events.insert {
            it[id] = event.id
            it[locationId] = event.locationId
            it[categoryId] = event.categoryId
            it[name] = event.name
            it[date] = event.date
            it[description] = event.description
            it[schedule] = event.schedule
            it[mod_date] = event.modDate

        }
    }

    fun updateEvent(event: Event, userToEvents: List<com.tiborfarago.resource_module.room.UserToEvents>) {
        if (Events.select { Events.id eq event.id }.count() != 0)
            Events.update({ Events.id eq event.id }) {
                it[id] = event.id
                it[locationId] = event.locationId
                it[categoryId] = event.categoryId
                it[name] = event.name
                it[date] = event.date
                it[description] = event.description
                it[schedule] = event.schedule
                it[mod_date] = event.modDate
            }
        else
            insertEvent(event)

        UserToEvents.deleteWhere { UserToEvents.eventId eq event.id }
        EventRepository.insertUserToEvents(userToEvents)
    }

    fun deleteEvent(id: Int) {
        UserToEvents.deleteWhere {
            UserToEvents.eventId eq id
        }

        Events.deleteWhere { Events.id eq id }
    }

}