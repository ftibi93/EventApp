package database

import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.room.UserToEvents
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object Users : Table(User.TABLE_NAME) {
    val id = integer(User.ID).primaryKey().autoIncrement()
    val name = varchar(User.NAME, 50)
    val password = varchar(User.PASSWORD, 100)
    val permission = integer(User.PERMISSION).default(0)
    val mod_date = datetime(User.MOD_DATE)
}

object Events : Table(Event.TABLE_NAME) {
    val id = integer(Event.ID).primaryKey().autoIncrement()
    val locationId = integer(Event.LOCATION_ID).references(Locations.id, ReferenceOption.CASCADE)
    val categoryId = integer(Event.CATEGORY_ID).references(Categories.id, ReferenceOption.CASCADE)
    val name = varchar(Event.NAME, 50)
    val date = datetime(Event.DATE)
    val description = varchar(Event.DESCRIPTION, 1000).nullable()
    val schedule = varchar(Event.SCHEDULE, 1000).nullable()
    val mod_date = datetime(Event.MOD_DATE)
}

object Locations : Table(Location.TABLE_NAME) {
    val id = integer(Location.ID).primaryKey().autoIncrement()
    val name = varchar(Location.NAME, 50)
    val city = varchar(Location.CITY, 50)
    val address = varchar(Location.ADDRESS, 50).nullable()
    val longitude = decimal(Location.LONGITUDE, 13, 10).nullable()
    val latitude = decimal(Location.LATITUDE, 13, 10).nullable()
    val mod_date = datetime(Location.MOD_DATE)
}

object UserToEvents : Table(UserToEvents.TABLE_NAME) {
    val userId = integer(UserToEvents.USER_ID).primaryKey(0).references(Users.id)
    val eventId = integer(UserToEvents.EVENT_ID).primaryKey(1).references(Events.id)
    val mod_date = datetime(UserToEvents.MOD_DATE)
}

object Categories : Table(Category.TABLE_NAME) {
    val id = integer(Category.ID).primaryKey().autoIncrement()
    val name = varchar(Category.NAME, 50)
    val mod_date = datetime(Category.MOD_DATE)
}