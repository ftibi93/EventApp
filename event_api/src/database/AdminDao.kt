package database

import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.room.UserToEvents
import utils.execAndMap
import utils.mapToObject

object AdminDao {
    fun getAllUserToEvents() = """
        SELECT * FROM ${UserToEvents.TABLE_NAME}
    """.trimIndent().execAndMap { it.mapToObject<UserToEvents>() }

    fun getNeededLocations(latestId: Int, neededIds: String) = """
        SELECT * FROM ${Location.TABLE_NAME} WHERE ${Location.ID} IN ($neededIds) OR ${Location.ID} > $latestId
    """.trimIndent().execAndMap { it.mapToObject<Location>() }

    fun getNeededEvents(latestId: Int, neededIds: String) = """
        SELECT * FROM ${Event.TABLE_NAME} WHERE ${Event.ID} IN ($neededIds) OR ${Event.ID} > $latestId
    """.trimIndent().execAndMap { it.mapToObject<Event>() }

    fun getNeededUsers() = """
        SELECT * FROM ${User.TABLE_NAME}
    """.trimIndent().execAndMap { it.mapToObject<User>() }

    fun getUserByUsername(userName: String): List<User> ="""
        SELECT * FROM ${User.TABLE_NAME} WHERE ${User.NAME} = "$userName"
    """.trimIndent().execAndMap { it.mapToObject<User>() }
}