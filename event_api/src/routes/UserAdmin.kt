package routes

import businesslogic.UserService
import com.tiborfarago.resource_module.classes.LoginRequest
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receiveStream
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject


class UserAdmin constructor(private val route: Route) {
    fun init() = route.route("useradmin") {
        post("login") {
            val request = call.receiveStream().convertToString().toObject<LoginRequest>() ?: return@post

            call.respondText(UserService.userAdminLogin(request).toJsonString(), ContentType.parse("text/json"))
        }
    }

}