package routes

import businesslogic.CategoryService
import com.tiborfarago.resource_module.room.Category
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receive
import io.ktor.request.receiveStream
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject
import com.tiborfarago.resource_module.utils.Result

class CategoryRoute constructor(private val route: Route) {

    fun init() = route.route("category") {
        get("getAllCategories") {
            call.respondText(CategoryService.getAllCategories().toJsonString(), ContentType.parse("text/json"))
        }

        post("insertCategory") {
            val request = call.receiveStream().convertToString().toObject<Category>() ?: return@post

            when(val result = CategoryService.insert(request)) {
                is Result.Success -> call.respondText(result.data.toJsonString(), ContentType.Application.Json)
                is Result.Error -> call.respond(HttpStatusCode.NotFound, result.networkError.errorMessage)
            }
        }

        post("modifyCategory") {
            val request = call.receiveStream().convertToString().toObject<Category>() ?: return@post

            when(val result = CategoryService.update(request)) {
                is Result.Success -> call.respondText(result.data.toJsonString(), ContentType.Application.Json)
                is Result.Error -> call.respond(HttpStatusCode.NotFound, result.networkError.errorMessage)
            }
        }

        post("deleteCategory") {
            val request = call.receive<Parameters>()["id"]?.toInt() ?: return@post

            call.respondText(CategoryService.delete(request).toJsonString(), ContentType.Application.Json)
        }
    }
}