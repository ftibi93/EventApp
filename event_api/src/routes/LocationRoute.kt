package routes

import com.tiborfarago.resource_module.room.Location
import businesslogic.LocationService
import com.tiborfarago.resource_module.utils.Result
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receive
import io.ktor.request.receiveStream
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject

class LocationRoute constructor(private val route: Route) {

    fun init() = route.route("location") {
        post("insertLocation") {
            val request = call.receiveStream().convertToString().toObject<Location>() ?: return@post

            when (val response = LocationService.insertLocation(request)) {
                is Result.Error -> call.respond(HttpStatusCode.NotFound, response.networkError.errorMessage)
                is Result.Success -> call.respondText(response.data.toJsonString(), ContentType.Application.Json)
            }
        }

        post("modifyLocation") {
            val request = call.receiveStream().convertToString().toObject<Location>() ?: return@post

            when (val response = LocationService.modifyLocation(request)) {
                is Result.Success -> call.respondText(response.data.toJsonString(), ContentType.Application.Json)
                is Result.Error -> call.respond(HttpStatusCode.NotFound, response.networkError.errorMessage)
            }
        }

        post("deleteLocation") {
            val request = call.receive<Parameters>()["id"]?.toInt() ?: return@post

            call.respondText(LocationService.deleteLocation(request).toJsonString(), ContentType.Application.Json)
        }
    }
}