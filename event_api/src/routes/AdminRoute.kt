package routes

import businesslogic.UserService
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.classes.LatestIdAndBasicObjectIds
import com.tiborfarago.resource_module.utils.Result
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receiveStream
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

@Suppress("NewApi")
class AdminRoute constructor(private val route: Route) {
    fun init() = route.route("admin") {
        post("login") {
            val request = call.receiveStream().convertToString().toObject<LoginRequest>() ?: return@post

            when(val result = UserService.login(request)) {
                is Result.Success -> call.respondText(result.data.toJsonString(), ContentType.parse("text/json"))
                is Result.Error -> call.respond(HttpStatusCode.NotFound, result.networkError.errorMessage)
            }

        }

        post("getAllLocations") {
            val request = call.receiveStream().convertToString().toObject<LatestIdAndBasicObjectIds>() ?: return@post

            call.respondText(UserService.allLocations(request).toJsonString(), ContentType.parse("text/json"))
        }

        post("getAllEvents") {
            val request = call.receiveStream().convertToString().toObject<LatestIdAndBasicObjectIds>() ?: return@post

            call.respondText(UserService.allEvents(request).toJsonString(), ContentType.parse("text/json"))
        }

        get("getAllUsersToEvents") {
            call.respondText(UserService.allUserToEvents().toJsonString(), ContentType.parse("text/json"))
        }

        get("getAllUsers") {
            call.respondText(UserService.allUsers().toJsonString(), ContentType.parse("text/json"))
        }
    }
}