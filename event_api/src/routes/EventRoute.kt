package routes

import com.tiborfarago.resource_module.classes.*
import businesslogic.EventService
import businesslogic.LocationService
import com.tiborfarago.resource_module.utils.Result
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receive
import io.ktor.request.receiveStream
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject
import utils.handleUserHeader

class EventRoute constructor(private val route: Route) {

    fun init() = route.route("event") {
        post("getEventsBaseUpdate") {
            handleUserHeader()

            val request: IdWithModDateRequest = call.receiveStream().convertToString().toObject<IdWithModDateRequest>()
                    ?: return@post

            call.respondText(EventService.getEventsBaseUpdate(request).toJsonString(), ContentType.parse("text/json"))
        }

        post("getEventsDetailsUpdate") {
            handleUserHeader()

            val request = call.receiveStream().convertToString().toObject<IdWithModDateRequest>() ?: return@post

            call.respondText(EventService.getEventsDetailsUpdate(request).toJsonString(), ContentType.parse("text/json"))
        }

        get("getNewEvents") {
            val latestId: Int = call.parameters["latestId"]?.toInt() ?: -1

            call.respondText(EventService.getNewEvents(latestId).toJsonString(), ContentType.parse("text/json"))
        }

        get("getEventDetails") {
            handleUserHeader()

            val eventId: Int = call.parameters["eventId"]?.toInt() ?: -1


            when (val result = EventService.getEventDetails(eventId)) {
                is Result.Error -> call.respond(HttpStatusCode.NotFound, result.networkError.errorMessage)
                is Result.Success -> call.respondText(result.data.toJsonString(), ContentType.parse("text/json"))
            }
        }

        post("getLocationsBaseUpdate") {
            handleUserHeader()

            val request = call.receiveStream().convertToString().toObject<IdWithModDateRequest>() ?: return@post

            call.respondText(LocationService.getLocationsBaseUpdate(request).toJsonString(), ContentType.parse("text/json"))
        }

        post("getLocationsDetailUpdate") {
            handleUserHeader()

            val request = call.receiveStream().convertToString().toObject<IdWithModDateRequest>() ?: return@post

            call.respondText(LocationService.getLocationsDetailsUpdate(request).toJsonString(), ContentType.parse("text/json"))
        }

        // admin part

        post("insertEvent") {
            val request = call.receiveStream().convertToString().toObject<InsertEventRequest>() ?: return@post

            when (val response = EventService.insert(request)) {
                is Result.Success -> call.respondText(response.data.toJsonString(), ContentType.Application.Json)
                is Result.Error -> call.respond(HttpStatusCode.NotFound, response.networkError.errorMessage)
            }
        }

        post("modifyEvent") {
            val request = call.receiveStream().convertToString().toObject<InsertEventRequest>() ?: return@post

            when (val response = EventService.modify(request)) {
                is Result.Success -> call.respondText(response.data.toJsonString(), ContentType.Application.Json)
                is Result.Error -> call.respond(HttpStatusCode.NotFound, response.networkError.errorMessage)
            }
        }

        post("deleteEvent") {
            val request = call.receive<Parameters>()["id"] ?: return@post

            call.respondText(EventService.delete(request.toInt()).toJsonString(), ContentType.Application.Json)
        }
    }
}