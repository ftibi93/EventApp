package routes

import com.tiborfarago.resource_module.room.User
import businesslogic.UserService
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.Parameters
import io.ktor.request.receive
import io.ktor.request.receiveStream
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route
import utils.convertToString
import utils.toJsonString
import utils.toObject

class UserRoute constructor(private val route: Route) {

    fun init() = route.route("user") {
        post("insertUser") {
            val request = call.receiveStream().convertToString().toObject<User>() ?: return@post

            call.respondText(UserService.insert(request).toJsonString(), ContentType.Application.Json)
        }

        post("deleteUser") {
            val request = call.receive<Parameters>()["id"]?.toInt() ?: return@post

            call.respondText(UserService.delete(request).toJsonString(), ContentType.Application.Json)
        }
    }
}