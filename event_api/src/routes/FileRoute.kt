package routes

import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.ApiConstants
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondFile
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import utils.toJsonString
import java.io.File
import java.io.InputStream
import java.io.OutputStream

class FileRoute constructor(private val route: Route) {

    fun init() = route.route("files") {
        post("uploadCover") {
            val folder = File("${System.getProperty("user.home")}/event_covers")
            folder.mkdirs()

            val parts = call.receiveMultipart().readAllParts()

            val id = (parts
                    .asSequence()
                    .filter { it is PartData.FormItem }
                    .first { it.name == "id" } as PartData.FormItem).value

            val fileItem = (parts
                    .asSequence()
                    .filter { it is PartData.FileItem }
                    .first { it.name == "file" } as PartData.FileItem)

            val file = File("${System.getProperty("user.home")}/event_covers", "$id.jpg")

            fileItem.streamProvider()
                    .use { provider ->
                        file.outputStream()
                                .buffered()
                                .use { provider.convertToString(it) }
                    }

            fileItem.dispose()

            call.respondText(SimpleResponse(ApiConstants.SUCCESS).toJsonString(), ContentType.Application.Json)
        }

        get("downloadCover") {
            val id = call.parameters["id"] ?: return@get

            val folder = File("${System.getProperty("user.home")}/event_covers")
            val file = folder.listFiles().firstOrNull { f -> f.nameWithoutExtension == id }

            if (file != null) {
                call.response.header("Content-Disposition", "attachment; filename=${file.name}")
                call.respondFile(file)
            } else
                call.respond(HttpStatusCode.NotFound)
        }
    }

    private fun InputStream.convertToString(
            outputStream: OutputStream
    ) {
        val buffer = ByteArray(1024)

        while (true) {
            val byteCount = this.read(buffer)
            if (byteCount < 0) break
            outputStream.write(buffer, 0, byteCount)
        }
    }
}