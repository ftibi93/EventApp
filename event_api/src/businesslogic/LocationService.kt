package businesslogic

import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.IdWithModDateRequest
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.classes.UpdateBaseLocationsResponse
import com.tiborfarago.resource_module.classes.UpdateLocationDetailsResponse
import com.tiborfarago.resource_module.classes.util.NetworkError
import repositories.LocationRepository
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.utils.Result

object LocationService {

    fun getLocationsBaseUpdate(request: IdWithModDateRequest): UpdateBaseLocationsResponse {
        val locations = LocationRepository.getBaseLocationsWithIds(request.idsString)

        val notExistingLocations = request.idWithModDateList.map { it.id }.minus(locations.map { it.id })

        val neededLocations = locations.filter { newLocation ->
            newLocation.modDate.millis > request.idWithModDateList.first { locationInRequest -> locationInRequest.id == newLocation.id }.modDate
        }

        return UpdateBaseLocationsResponse(neededLocations, notExistingLocations)
    }

    fun getLocationsDetailsUpdate(request: IdWithModDateRequest): UpdateLocationDetailsResponse {
        val locations = LocationRepository.getLocationsWithIds(request.idsString)

        val notExistingLocations = request.idWithModDateList.map { it.id }.minus(locations.map { it.id })

        val neededLocations = locations.filter { location ->
            location.modDate.millis > request.idWithModDateList.first { locationInRequest -> locationInRequest.id == location.id }.modDate
        }

        return UpdateLocationDetailsResponse(neededLocations, notExistingLocations)
    }

    fun insertLocation(location: Location): Result<SimpleResponse> {
        if (LocationRepository.getLocationById(location.id) != null)
            return Result.Error(NetworkError(ApiConstants.EXISTING_ID))

        LocationRepository.insertLocation(location)

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun modifyLocation(location: Location): Result<SimpleResponse> {
        if (LocationRepository.getLocationById(location.id) == null)
            return Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID))

        LocationRepository.updateLocation(location)

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun deleteLocation(id: Int): SimpleResponse {
        LocationRepository.deleteLocation(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }

}