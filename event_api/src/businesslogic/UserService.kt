package businesslogic

import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.classes.util.NetworkError
import repositories.AdminRepository
import repositories.UserRepository
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.utils.Result
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import kotlin.streams.toList

@Suppress("NewApi")
object UserService {

    fun login(loginRequest: LoginRequest): Result<LoginResponse> {
        val user = AdminRepository.loginUser(loginRequest)
                .also {
                    if (it.isEmpty()) return Result.Error(NetworkError(ApiConstants.WRONG_USERNAME))
                }
                .parallelStream()
                .filter {
                    BCrypt.checkpw(loginRequest.password, it.password)
                }.toList().firstOrNull() ?: return Result.Error(NetworkError(ApiConstants.WRONG_PASSWORD))

        return Result.Success(LoginResponse(user.permission, 0, emptyList()))
    }

    fun allLocations(objects: LatestIdAndBasicObjectIds): UpdateLocationDetailsResponse =
            UpdateLocationDetailsResponse(AdminRepository.getNeededLocations(objects.latestId, objects.basicObjectIds.joinToString()))

    fun allEvents(objects: LatestIdAndBasicObjectIds): UpdateDetailsEventsResponse =
            UpdateDetailsEventsResponse(AdminRepository.getNeededEvents(objects.latestId, objects.basicObjectIds.joinToString()))

    fun allUserToEvents(): UserToEventsResponse = UserToEventsResponse(AdminRepository.getAllUserToEvents())

    fun allUsers(): UsersResponse = UsersResponse(AdminRepository.getAllUsers())

    fun userAdminLogin(loginRequest: LoginRequest): LoginResponse {
        val check = AdminRepository.loginUser(loginRequest).parallelStream().filter { user ->
            loginRequest.name == user.name && BCrypt.checkpw(loginRequest.password, user.password)
        }.toList()

        val modDate = check.firstOrNull()?.modDate?.millis ?: DateTime(1970, 1, 1, 0, 0, 0).millis
        val id = check.firstOrNull()?.id ?: -1

        val userToEvents = UserRepository.getUserToEventsByUserId(id)

        return LoginResponse(id, modDate, userToEvents)
    }

    fun insert(user: User): SimpleResponse {
        UserRepository.insertUser(user)

        return SimpleResponse(ApiConstants.SUCCESS)
    }

    fun delete(id: Int): SimpleResponse {
        UserRepository.deleteUser(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }
}