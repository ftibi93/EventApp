package businesslogic

import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.classes.util.NetworkError
import repositories.CategoryRepository
import repositories.EventRepository
import repositories.LocationRepository
import com.tiborfarago.resource_module.utils.Result
import java.io.File

object EventService {

    fun getEventsBaseUpdate(request: IdWithModDateRequest): UpdateBaseEventsResponse {
        val events = EventRepository.getBaseEventsByIds(request.idsString)

        val notExistingEvents = request.idWithModDateList.map { it.id }.minus(events.map { it.id })

        val neededEvents = events.filter { newEvent ->
            newEvent.modDate.millis > request.idWithModDateList.first { eventInRequest -> eventInRequest.id == newEvent.id }.modDate
        }

        return UpdateBaseEventsResponse(neededEvents, notExistingEvents)
    }

    fun getEventsDetailsUpdate(request: IdWithModDateRequest): UpdateDetailsEventsResponse {
        val events = EventRepository.getEventByIds(request.idsString)

        val notExistingEvents = request.idWithModDateList.map { it.id }.minus(events.map { it.id })

        val neededEvents = events.filter { newEvent ->
            newEvent.modDate.millis > request.idWithModDateList.first { eventInRequest -> eventInRequest.id == newEvent.id }.modDate
        }

        return UpdateDetailsEventsResponse(neededEvents, notExistingEvents)
    }

    fun getNewEvents(latestId: Int): NewEventsResponse {
        val newEvents = EventRepository.getAllNewEvents(latestId)

        if (newEvents.isEmpty())
            return NewEventsResponse()

        val newLocations = LocationRepository.getNewLocations(newEvents.map { it.locationId }.distinct().joinToString())

        return NewEventsResponse(
                newEvents,
                newLocations,
                EventRepository.getAllUserToEvents()
        )
    }

    fun getEventDetails(eventId: Int): Result<EventAndLocationDetailsResponse> {
        val event = EventRepository.getEventByIds(eventId.toString()).firstOrNull()
                ?: return Result.Error(NetworkError("event"))
        val location = LocationRepository.getLocationDetails(event.locationId)
                ?: return Result.Error(NetworkError("location"))

        return Result.Success(EventAndLocationDetailsResponse(event, location))
    }

    fun insert(request: InsertEventRequest): Result<SimpleResponse> = when {
        EventRepository.getEventByIds(request.event.id.toString()).isNotEmpty() -> Result.Error(NetworkError(ApiConstants.EXISTING_ID))
        !CategoryRepository.isCategoryExists(request.event.categoryId) -> Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID_CATEGORY))
        LocationRepository.getLocationDetails(request.event.locationId) == null -> Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID_LOCATION))
        else -> {
            EventRepository.insertEvent(request.event)
            EventRepository.insertUserToEvents(request.userToEvents)

            Result.Success(SimpleResponse(ApiConstants.SUCCESS))
        }
    }

    fun modify(request: InsertEventRequest): Result<SimpleResponse> = when {
        !CategoryRepository.isCategoryExists(request.event.categoryId) -> Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID_CATEGORY))
        LocationRepository.getLocationDetails(request.event.locationId) == null -> Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID_LOCATION))
        else -> {
            EventRepository.updateEvent(request.event, request.userToEvents)
            Result.Success(SimpleResponse(ApiConstants.SUCCESS))
        }
    }

    fun delete(id: Int): SimpleResponse {
        EventRepository.deleteEvent(id)

        val file = File("${System.getProperty("user.home")}/event_covers", "$id.jpg")
        file.delete()

        return SimpleResponse(ApiConstants.SUCCESS)
    }

}