package businesslogic

import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.CategoriesResponse
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.classes.util.NetworkError
import repositories.CategoryRepository
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.utils.Result

object CategoryService {
    fun getAllCategories(): CategoriesResponse = CategoriesResponse(CategoryRepository.getAllCategories())

    fun insert(category: Category): Result<SimpleResponse> {
        if (CategoryRepository.getCategoryById(category.id) != null)
            return Result.Error(NetworkError(ApiConstants.EXISTING_ID))

        CategoryRepository.insertCategory(category)

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun update(category: Category): Result<SimpleResponse> {
        if (CategoryRepository.getCategoryById(category.id) == null)
            return Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID))

        CategoryRepository.updateCategory(category)

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun delete(id: Int): SimpleResponse {
        CategoryRepository.deleteCategory(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }
}