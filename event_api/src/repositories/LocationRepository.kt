package repositories

import com.tiborfarago.resource_module.classes.NewLocation
import database.ApiDao
import database.ApiLocationDao
import com.tiborfarago.resource_module.room.Location
import org.jetbrains.exposed.sql.transactions.transaction

object LocationRepository {

    fun getLocationById(id: Int): Location? = transaction {
        ApiDao.getLocationDetails(id)
    }.firstOrNull()

    fun insertLocation(location: Location) {
        transaction {
            ApiLocationDao.insertLocation(location)
        }
    }

    fun updateLocation(location: Location)  {
        transaction {
            ApiLocationDao.updateLocation(location)
        }
    }

    fun deleteLocation(id: Int) {
        transaction {
            ApiLocationDao.deleteLocation(id)
        }
    }

    fun getNewLocations(ids: String): List<NewLocation> = transaction {
        ApiDao.getNewLocations(ids)
    }

    fun getLocationDetails(id: Int): Location? = transaction {
        ApiDao.getLocationDetails(id)
    }.firstOrNull()

    fun getBaseLocationsWithIds(ids: String): List<NewLocation> = transaction {
        ApiDao.getBaseLocationsWithIds(ids)
    }

    fun getLocationsWithIds(ids: String): List<Location> = transaction {
        ApiDao.getLocationsWithIds(ids)
    }

}