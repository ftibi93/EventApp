package repositories

import database.ApiDao
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.room.UserToEvents
import org.jetbrains.exposed.sql.transactions.transaction

object UserRepository {

    fun getUserToEventsByUserId(userId: Int): List<UserToEvents> = transaction {
        ApiDao.getUserToEventsByUserId(userId)
    }

    fun insertUser(user: User) {
        transaction {
            ApiDao.insertUser(user)
        }
    }

    fun deleteUser(id: Int) {
        transaction {
            ApiDao.deleteUser(id)
        }
    }

    fun getExistingUserIds(headersUserIds: String): List<Int> = transaction {
        ApiDao.getExistingUserIds(headersUserIds)
    }

}