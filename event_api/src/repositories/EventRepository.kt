package repositories

import com.tiborfarago.resource_module.classes.NewEvent
import database.ApiDao
import database.ApiEventDao
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.UserToEvents
import org.jetbrains.exposed.sql.transactions.transaction

object EventRepository {
    fun getBaseEventsByIds(ids: String): List<NewEvent> = transaction {
        ApiDao.getBaseEventsWithIds(ids)
    }

    fun getEventByIds(ids: String): List<Event> = transaction {
        ApiDao.getEventsWithIds(ids)
    }

    fun getAllNewEvents(latestId: Int): List<NewEvent> = transaction {
        ApiDao.getAllNewEvents(latestId)
    }

    fun getAllUserToEvents(): List<UserToEvents> = transaction {
        ApiDao.getAllUserToEvents()
    }

    fun insertUserToEvents(userToEvents: List<UserToEvents>) {
        transaction {
            ApiDao.insertUserToEvents(userToEvents)
        }
    }

    fun insertEvent(event: Event) {
        transaction {
            ApiEventDao.insertEvent(event)
        }
    }

    fun updateEvent(event: Event, userToEvents: List<UserToEvents>) {
        transaction {
            ApiEventDao.updateEvent(event, userToEvents)
        }
    }

    fun deleteEvent(id: Int) {
        transaction {
            ApiEventDao.deleteEvent(id)
        }
    }
}