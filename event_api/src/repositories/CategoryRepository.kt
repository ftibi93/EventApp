package repositories

import database.ApiCategoryDao
import com.tiborfarago.resource_module.room.Category
import org.jetbrains.exposed.sql.transactions.transaction

object CategoryRepository {
    fun getAllCategories(): List<Category> = transaction {
        ApiCategoryDao.getAllCategories()
    }

    fun getCategoryById(id: Int): Category? = transaction {
        ApiCategoryDao.getCategoryById(id).firstOrNull()
    }

    fun insertCategory(category: Category) {
        transaction {
            ApiCategoryDao.insertCategory(category)
        }
    }

    fun updateCategory(category: Category) {
        transaction {
            ApiCategoryDao.updateCategory(category)
        }
    }

    fun deleteCategory(id: Int) {
        transaction {
            ApiCategoryDao.deleteCategory(id)
        }
    }

    fun isCategoryExists(id: Int): Boolean = transaction { ApiCategoryDao.isCategoryExists(id) }
}