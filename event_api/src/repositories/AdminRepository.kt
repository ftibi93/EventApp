package repositories

import com.tiborfarago.resource_module.classes.LoginRequest
import database.AdminDao
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import org.jetbrains.exposed.sql.transactions.transaction

object AdminRepository {
    fun loginUser(loginRequest: LoginRequest): List<User> = transaction {
        AdminDao.getUserByUsername(loginRequest.name)
    }

    fun getNeededLocations(latestId: Int, basicIds: String): List<Location> = transaction {
        AdminDao.getNeededLocations(latestId, basicIds)
    }

    fun getNeededEvents(latestId: Int, basicIds: String): List<Event> = transaction {
        AdminDao.getNeededEvents(latestId, basicIds)
    }

    fun getAllUserToEvents() = transaction {
        AdminDao.getAllUserToEvents()
    }

    fun getAllUsers() = transaction {
        AdminDao.getNeededUsers()
    }
}