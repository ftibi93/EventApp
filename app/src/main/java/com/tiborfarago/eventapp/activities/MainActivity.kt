package com.tiborfarago.eventapp.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.WorkManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.database.AppDatabase
import com.tiborfarago.eventapp.fragments.dialogs.AddSubscriptionDialog
import com.tiborfarago.eventapp.fragments.dialogs.BottomNavigationDrawerFragment
import com.tiborfarago.eventapp.network.ChangeUploader
import com.tiborfarago.resource_module.utils.Result
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.simplestack.FragmentStateChanger
import com.tiborfarago.eventapp.simplestack.MainKey
import com.tiborfarago.eventapp.util.App
import com.tiborfarago.eventapp.util.SharedPrefKeys
import com.tiborfarago.eventapp.util.showSnackbar
import com.tiborfarago.eventapp.work_manager.LoginWorker
import com.zhuinden.simplestack.BackstackDelegate
import com.zhuinden.simplestack.History
import com.zhuinden.simplestack.StateChange
import com.zhuinden.simplestack.StateChanger
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), StateChanger, CoroutineScope {
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    @Inject
    lateinit var appDatabase: AppDatabase
    @Inject
    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var bottomNavigationDrawerFragment: BottomNavigationDrawerFragment
    @Inject
    lateinit var changeUploader: ChangeUploader
    @Inject
    lateinit var synchronizer: Synchronizer

    private lateinit var backstackDelegate: BackstackDelegate
    private lateinit var fragmentStateChanger: FragmentStateChanger

    override fun onCreate(savedInstanceState: Bundle?) {
        backstackDelegate = BackstackDelegate().apply {
            onCreate(savedInstanceState, lastCustomNonConfigurationInstance, History.single(MainKey()))
            registerForLifecycleCallbacks(this@MainActivity)
        }

        (application as App).applicationComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentStateChanger = FragmentStateChanger(supportFragmentManager, R.id.frame_layout)
        backstackDelegate.setStateChanger(this)

        setSupportActionBar(bar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
        }

        launch(Dispatchers.Default) {
            val result = listOf(
                    async { changeUploader.commitCategoryChanges() },
                    async { changeUploader.commitLocationChanges() },
                    async { changeUploader.commitEventChanges() },
                    async { changeUploader.commitUserChanges() },
                    async { changeUploader.uploadImages() }
            ).awaitAll().flatten()

            result.forEach { Timber.e(it.networkError) }

            withContext(Dispatchers.Main) {
                result.firstOrNull()?.let { showSnackbar("${it.networkError.errorCode} - ${it.networkError.errorMessage}") }
            }

            val result2 = listOf(
                    async { synchronizer.getCategories() },
                    async { synchronizer.getEventsBaseUpdate() },
                    async { synchronizer.getEventsDetailsUpdate() },
                    async { synchronizer.getLocationBaseUpdate() },
                    async { synchronizer.getLocationDetailsUpdate() },
                    async { synchronizer.getNewEventsAndLocations() }
            ).awaitAll().filter { it is Result.Error }

            withContext(Dispatchers.Main) {
                result2.firstOrNull()?.let { showSnackbar("${(it as Result.Error).networkError.errorCode} - ${it.networkError.errorMessage}") }
            }

            result2.forEach { Timber.e((it as Result.Error).networkError) }


        }

        WorkManager.getInstance().getWorkInfosByTagLiveData(LoginWorker.TAG).observe(this, Observer { workStatusList ->
            val result = workStatusList.firstOrNull { it.id == LoginWorker.workerId && it.outputData.getString(LoginWorker.RESULT_TEXT) != null }
            val resultText = result?.outputData?.getString(LoginWorker.RESULT_TEXT)
                    ?: return@Observer
            if (result.outputData.getBoolean(LoginWorker.RESULT_SUCCESS, false))
                Snackbar.make(findViewById(android.R.id.content), resultText, Snackbar.LENGTH_SHORT).show()
            else
                Snackbar.make(findViewById(android.R.id.content), resultText, Snackbar.LENGTH_LONG).setAction(getString(R.string.retry)) {
                    AddSubscriptionDialog().show(supportFragmentManager, "dialog_retry")
                }.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        if (event == BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_SWIPE || event == BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_TIMEOUT) {
                            sharedPreferences.edit().putString(SharedPrefKeys.LOGIN_DIALOG_USER, "").apply()
                        }
                    }
                }).show()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                bottomNavigationDrawerFragment.show(supportFragmentManager, bottomNavigationDrawerFragment.tag)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun handleStateChange(stateChange: StateChange, completionCallback: StateChanger.Callback) {
        if (stateChange.topNewState<Any>() == stateChange.topPreviousState()) {
            completionCallback.stateChangeComplete()
            return
        }

        fragmentStateChanger(stateChange)
        completionCallback.stateChangeComplete()
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return backstackDelegate.onRetainCustomNonConfigurationInstance()
    }

    override fun onBackPressed() {
        if (!backstackDelegate.onBackPressed())
            super.onBackPressed()
    }

    fun navigateTo(key: Any) {
        backstackDelegate.backstack.goTo(key)
    }

    fun replaceHistory(key: Any) {
        backstackDelegate.backstack.setHistory(History.single(key), StateChange.REPLACE)
    }

    fun goBack() {
        backstackDelegate.backstack.goBack()
    }
}
