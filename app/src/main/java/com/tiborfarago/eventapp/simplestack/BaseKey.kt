package com.tiborfarago.eventapp.simplestack

import android.os.Bundle
import android.os.Parcelable

abstract class BaseKey : Parcelable {
    fun getFragmentTag() = toString()

    fun newFragment(): BaseFragment = createFragment().apply {
        arguments = (arguments ?: Bundle()).apply {
            putParcelable("KEY", this@BaseKey)
        }
    }

    abstract fun createFragment(): BaseFragment
}