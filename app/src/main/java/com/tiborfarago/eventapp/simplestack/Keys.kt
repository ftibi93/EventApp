package com.tiborfarago.eventapp.simplestack

import com.tiborfarago.eventapp.fragments.*
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainKey(val tag: String) : BaseKey() {
    constructor() : this("MainKey")

    override fun createFragment(): BaseFragment = MainFragment()
}

@Parcelize
data class SubscriptionKey(val tag: String) : BaseKey() {
    constructor() : this("SubscriptionKey")

    override fun createFragment(): BaseFragment = SubscriptionFragment()
}

@Parcelize
data class EventDetailsKey(val tag: String, val eventId: Int) : BaseKey() {
    constructor(eventId: Int) : this("EventDetailsKey", eventId)

    override fun createFragment(): BaseFragment = EventDetailsFragment()
}

@Parcelize
data class AdminKey(val tag: String = "AdminKey") : BaseKey() {
    override fun createFragment(): BaseFragment = AdminFragment()
}

@Parcelize
data class CategoriesKey(val tag: String = "CategoriesKey") : BaseKey() {
    override fun createFragment(): BaseFragment = AdminCategoriesFragment()
}

@Parcelize
data class AddLocationKey(val tag: String = "AddLocationKey", val locationId: Int? = null) : BaseKey() {
    override fun createFragment(): BaseFragment = AddLocationFragment()

}

@Parcelize
data class LocationsKey(val tag: String = "LocationsKey") : BaseKey() {
    override fun createFragment(): BaseFragment = AdminLocationsFragment()
}

@Parcelize
data class AddEventKey(val tag: String = "AddEventKey", val eventId: Int? = null) : BaseKey() {
    override fun createFragment(): BaseFragment = AddEventFragment()
}

@Parcelize
data class EventsKey(val tag: String = "EventsKey") : BaseKey() {
    override fun createFragment(): BaseFragment = AdminEventsFragment()
}

@Parcelize
data class UsersKey(val tag: String = "UsersKey") : BaseKey() {
    override fun createFragment(): BaseFragment = AdminUsersFragment()
}