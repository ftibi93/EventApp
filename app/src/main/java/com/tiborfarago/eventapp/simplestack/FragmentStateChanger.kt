package com.tiborfarago.eventapp.simplestack

import androidx.fragment.app.FragmentManager
import com.tiborfarago.eventapp.R
import com.zhuinden.simplestack.StateChange

/**
 * Modified version of the following
 * @link https://github.com/Zhuinden/simple-stack/blob/504b2c44295c77a960ca34add68fdc685c3dbc19/simple-stack-example-basic-fragment/src/main/java/com/zhuinden/navigationexamplefrag/FragmentStateChanger.java
 * @link https://medium.com/@Zhuinden/simplified-fragment-navigation-using-a-custom-backstack-552e06961257
 */
class FragmentStateChanger constructor(private val fragmentManager: FragmentManager, private val containerId: Int) {

    operator fun invoke(stateChange: StateChange) {
        val fragmentTransaction = fragmentManager.beginTransaction().disallowAddToBackStack()
        if(stateChange.direction == StateChange.FORWARD) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_right, R.anim.slide_out_to_left)
        } else if(stateChange.direction == StateChange.BACKWARD) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right, R.anim.slide_in_from_left, R.anim.slide_out_to_right)
        }

        val previousState = stateChange.getPreviousState<BaseKey>()
        val newState = stateChange.getNewState<BaseKey>()
        for (oldKey in previousState) {
            val fragment = fragmentManager.findFragmentByTag(oldKey.getFragmentTag())
            if (fragment != null) {
                if (!newState.contains(oldKey)) {
                    fragmentTransaction.remove(fragment)
                } else if (!fragment.isDetached) {
                    fragmentTransaction.detach(fragment)
                }
            }
        }
        for (newKey in newState) {
            var fragment = fragmentManager.findFragmentByTag(newKey.getFragmentTag())
            if (newKey.equals(stateChange.topNewState<Any>())) {
                if (fragment != null) {
                    if (fragment.isDetached) {
                        fragmentTransaction.attach(fragment)
                    }
                } else {
                    fragment = newKey.newFragment()
                    fragmentTransaction.add(containerId, fragment, newKey.getFragmentTag())
                }
            } else {
                if (fragment != null && !fragment.isDetached) {
                    fragmentTransaction.detach(fragment)
                }
            }
        }
        fragmentTransaction.commitNow()
    }
}