package com.tiborfarago.eventapp.network.services

import com.tiborfarago.eventapp.util.Constants
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface AdminService {
    @POST("${Constants.ADMIN_URL}/getAllLocations")
    fun getAllLocations(@Body latestIdAndBasicObjectIds: LatestIdAndBasicObjectIds): Call<UpdateLocationDetailsResponse>

    @POST("${Constants.ADMIN_URL}/getAllEvents")
    fun getAllEvents(@Body latestIdAndBasicObjectIds: LatestIdAndBasicObjectIds): Call<UpdateDetailsEventsResponse>

    @GET("${Constants.ADMIN_URL}/getAllUsersToEvents")
    fun getAllUserToEvents(): Call<UserToEventsResponse>

    @GET("${Constants.ADMIN_URL}/getAllUsers")
    fun getAllUsers(): Call<UsersResponse>


    // categories

    @POST("${Constants.CATEGORY_URL}/insertCategory")
    fun uploadCategory(@Body category: Category): Call<SimpleResponse>

    @POST("${Constants.CATEGORY_URL}/modifyCategory")
    fun modifyCategory(@Body category: Category): Call<SimpleResponse>

    @FormUrlEncoded
    @POST("${Constants.CATEGORY_URL}/deleteCategory")
    fun deleteCategory(@Field("id") id: Int): Call<SimpleResponse>


    // locations

    @POST("${Constants.LOCATION_URL}/insertLocation")
    fun uploadLocation(@Body location: Location): Call<SimpleResponse>

    @POST("${Constants.LOCATION_URL}/modifyLocation")
    fun modifyLocation(@Body location: Location): Call<SimpleResponse>

    @FormUrlEncoded
    @POST("${Constants.LOCATION_URL}/deleteLocation")
    fun deleteLocation(@Field("id") id: Int): Call<SimpleResponse>


    // events

    @POST("${Constants.EVENT_URL}/insertEvent")
    fun uploadEvent(@Body insertEventRequest: InsertEventRequest): Call<SimpleResponse>

    @POST("${Constants.EVENT_URL}/modifyEvent")
    fun modifyEvent(@Body insertEventRequest: InsertEventRequest): Call<SimpleResponse>

    @FormUrlEncoded
    @POST("${Constants.EVENT_URL}/deleteEvent")
    fun deleteEvent(@Field("id") id: Int): Call<SimpleResponse>


    // users

    @POST("${Constants.USER_URL}/insertUser")
    fun uploadUser(@Body user: User): Call<SimpleResponse>

    @FormUrlEncoded
    @POST("${Constants.USER_URL}/deleteUser")
    fun deleteUser(@Field("id") id: Int): Call<SimpleResponse>


    // covers

    @Multipart
    @POST("${Constants.FILE_URL}/uploadCover")
    fun uploadCover(
            @Part image: MultipartBody.Part,
            @Part("id") id: RequestBody
    ): Call<SimpleResponse>
}