package com.tiborfarago.eventapp.network.services

import com.tiborfarago.eventapp.util.Constants
import com.tiborfarago.resource_module.classes.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface EventService {
    @GET("${Constants.EVENT_URL}/getNewEvents")
    fun getNewEvents(
            @Query("latestId") latestEventId: Int = -1
    ): Call<NewEventsResponse>

    @GET("${Constants.EVENT_URL}/getEventDetails")
    fun getEventLocationDetails(@Query("eventId") eventId: Int): Call<EventAndLocationDetailsResponse>

    @GET("${Constants.CATEGORY_URL}/getAllCategories")
    fun getAllCategories(): Call<CategoriesResponse>



    @POST("${Constants.EVENT_URL}/getEventsBaseUpdate")
    fun getEventsBaseUpdate(@Body idWithModDateRequest: IdWithModDateRequest): Call<UpdateBaseEventsResponse>

    @POST("${Constants.EVENT_URL}/getEventsDetailsUpdate")
    fun getEventsDetailsUpdate(@Body idWithModDateRequest: IdWithModDateRequest): Call<UpdateDetailsEventsResponse>

    @POST("${Constants.EVENT_URL}/getLocationsBaseUpdate")
    fun getLocationsBaseUpdate(@Body idWithModDateRequest: IdWithModDateRequest): Call<UpdateBaseLocationsResponse>

    @POST("${Constants.EVENT_URL}/getLocationsDetailUpdate")
    fun getLocationsDetailUpdate(@Body idWithModDateRequest: IdWithModDateRequest): Call<UpdateLocationDetailsResponse>
}