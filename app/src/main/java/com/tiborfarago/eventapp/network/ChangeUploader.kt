package com.tiborfarago.eventapp.network

import android.content.Context
import android.net.Uri
import com.tiborfarago.eventapp.database.daos.*
import com.tiborfarago.eventapp.network.services.AdminService
import com.tiborfarago.eventapp.util.await
import com.tiborfarago.resource_module.classes.InsertEventRequest
import com.tiborfarago.resource_module.room.*
import com.tiborfarago.resource_module.utils.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class ChangeUploader @Inject constructor(
        private val context: Context,
        private val adminService: AdminService,
        private val changeDao: ChangeDao,
        private val categoryDao: CategoryDao,
        private val locationDao: LocationDao,
        private val userToEventDao: UserToEventDao,
        private val eventDao: EventDao,
        private val userDao: UserDao,
        private val coverUriDao: CoverUriDao
) {
    suspend fun commitCategoryChanges(): List<Result.Error> = withContext(Dispatchers.Default) {
        val responses = mutableListOf<Result.Error>()

        val changes = changeDao.getChangesForTable(Category.TABLE_NAME)

        // insert
        changes
                .filter { it.type == Change.TYPE.ADD }
                .forEach { change ->
                    categoryDao.getCategoryById(change.changedRowId)?.let { it1 ->
                        val response = adminService.uploadCategory(it1).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    }
                }

        // modify
        changes
                .filter { it.type == Change.TYPE.MODIFY }
                .forEach { change ->
                    categoryDao.getCategoryById(change.changedRowId)?.let { category ->
                        val response = adminService.modifyCategory(category).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)

                    }
                }

        // delete
        changes
                .filter { it.type == Change.TYPE.DELETE }
                .forEach {
                    val response = adminService.deleteCategory(it.changedRowId).await()

                    if (response is Result.Error)
                        responses.add(response)
                    else {
                        categoryDao.getCategoryById(it.changedRowId)?.let { it1 -> categoryDao.delete(it1) }
                        changeDao.delete(it)
                    }
                }



        responses
    }

    suspend fun commitLocationChanges(): List<Result.Error> = withContext(Dispatchers.Default) {
        val responses = mutableListOf<Result.Error>()

        val changes = changeDao.getChangesForTable(Location.TABLE_NAME)

        // insert
        changes
                .filter { it.type == Change.TYPE.ADD }
                .forEach { change ->
                    locationDao.location(change.changedRowId)?.let {
                        val response = adminService.uploadLocation(it).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    }
                }

        // modify
        changes
                .filter { it.type == Change.TYPE.MODIFY }
                .forEach { change ->
                    locationDao.location(change.changedRowId)?.let {
                        val response = adminService.modifyLocation(it).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    }
                }

        // delete
        changes
                .filter { it.type == Change.TYPE.DELETE }
                .forEach { change ->
                    val response = adminService.deleteLocation(change.changedRowId).await()

                    if (response is Result.Error)
                        responses.add(response)
                    else {
                        locationDao.location(change.changedRowId)?.let { locationDao.delete(it) }
                        changeDao.delete(change)
                    }
                }

        responses
    }

    suspend fun commitEventChanges(): List<Result.Error> = withContext(Dispatchers.Default) {
        val eventChanges = changeDao.getChangesForTable(Event.TABLE_NAME)
        val responses = mutableListOf<Result.Error>()

        // insert
        eventChanges
                .filter { it.type == Change.TYPE.ADD }
                .forEach { change ->
                    eventDao.getEventById(change.changedRowId)?.let {
                        val userToEventsList = userToEventDao.getUserToEventsForEvents(eventChanges
                                .asSequence()
                                .filter { it.type != Change.TYPE.DELETE }
                                .map { it.changedRowId }
                                .toList())

                        val response = adminService.uploadEvent(InsertEventRequest(it, userToEventsList)).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    } ?: changeDao.delete(change)
                }

        eventChanges
                .filter { it.type == Change.TYPE.MODIFY }
                .forEach { change ->
                    eventDao.getEventById(change.changedRowId)?.let {
                        val userToEventList = userToEventDao.getUserToEventsForEvents(eventChanges
                                .asSequence()
                                .filter { it.type != Change.TYPE.DELETE }
                                .map { it.changedRowId }
                                .toList())

                        val response = adminService.modifyEvent(InsertEventRequest(it, userToEventList)).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    } ?: changeDao.delete(change)
                }

        eventChanges
                .filter { it.type == Change.TYPE.DELETE }
                .forEach { change ->
                    val response = adminService.deleteEvent(change.changedRowId).await()

                    if (response is Result.Error)
                        responses.add(response)
                    else {
                        eventDao.getEventById(change.changedRowId)?.let { eventDao.delete(it) }
                        changeDao.delete(change)
                        userToEventDao.deleteForEvents(change.changedRowId)
                    }
                }

        responses
    }

    suspend fun commitUserChanges(): List<Result.Error> = withContext(Dispatchers.Default) {
        val changes = changeDao.getChangesForTable(User.TABLE_NAME)
        val responses = mutableListOf<Result.Error>()

        changes
                .filter { it.type == Change.TYPE.ADD }
                .forEach { change ->
                    userDao.getUserById(change.changedRowId)?.let {
                        val response = adminService.uploadUser(it).await()

                        if (response is Result.Error)
                            responses.add(response)
                        else changeDao.delete(change)
                    } ?: changeDao.delete(change)
                }

        changes
                .filter { it.type == Change.TYPE.DELETE }
                .forEach {
                    val response = adminService.deleteUser(it.changedRowId).await()

                    if (response is Result.Error)
                        responses.add(response)
                    else {
                        userDao.deleteUser(it.changedRowId)
                        changeDao.delete(it)
                    }
                }

        responses
    }

    suspend fun uploadImages(): List<Result.Error> = withContext(Dispatchers.Default) {
        val uris = coverUriDao.getAll()
        val responses = mutableListOf<Result.Error>()

        uris.forEach {
            val file = uriToFile(it.uri)
            if (file != null) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val fileBody = MultipartBody.Part.createFormData("file", file.name, requestFile)

                val idBody = RequestBody.create(MediaType.parse("multipart/form-data"), it.eventId.toString())

                val response = adminService.uploadCover(fileBody, idBody).await()

                if (response is Result.Error)
                    responses.add(response)
                else coverUriDao.delete(it)
            }
        }

        responses
    }

    private fun uriToFile(uri: String): File? {
        val uri = Uri.parse(uri)

        val stream = context.contentResolver.openInputStream(uri) ?: return null

        val file = File.createTempFile("valami", "jpg")
        val buffer = ByteArray(stream.available())

        stream.read(buffer)

        val outputStream = FileOutputStream(file)
        outputStream.write(buffer)

        return file
    }
}