package com.tiborfarago.eventapp.network.services

import com.tiborfarago.eventapp.util.Constants
import com.tiborfarago.resource_module.classes.LoginRequest
import com.tiborfarago.resource_module.classes.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserAdminService {
    @POST("${Constants.USERADMIN_URL}/login")
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST("${Constants.ADMIN_URL}/login")
    fun loginAdmin(@Body loginRequest: LoginRequest): Call<LoginResponse>
}