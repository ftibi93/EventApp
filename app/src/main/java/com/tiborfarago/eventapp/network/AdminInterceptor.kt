package com.tiborfarago.eventapp.network

import com.tiborfarago.eventapp.network.services.UserAdminService
import com.tiborfarago.eventapp.util.SharedPreferencesManager
import com.tiborfarago.eventapp.util.await
import com.tiborfarago.resource_module.classes.LoginRequest
import com.tiborfarago.resource_module.utils.Result
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AdminInterceptor @Inject constructor(
        private val userAdminService: UserAdminService,
        private val sharedPreferencesManager: SharedPreferencesManager
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer ${sharedPreferencesManager.token}")
                .build()

        val response = chain.proceed(request)

        if (response.code() == 401)
            return updateToken(request, chain)

        return response
    }

    private fun updateToken(request: Request, chain: Interceptor.Chain): Response = runBlocking {
        var tries = 0
        var res: Response

        do {
            tries += 1
            val response = userAdminService.loginAdmin(
                    LoginRequest(sharedPreferencesManager.adminUser, sharedPreferencesManager.adminPass)).await()

            if (response is Result.Success)
                sharedPreferencesManager.token = response.data.token

            val newRequest = request.newBuilder().header("Authorization", "Bearer ${sharedPreferencesManager.token}").build()
            res = chain.proceed(newRequest)
        } while (tries <= 3 && res.code() == 401)

        return@runBlocking res
    }

}