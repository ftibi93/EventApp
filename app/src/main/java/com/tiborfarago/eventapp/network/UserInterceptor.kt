package com.tiborfarago.eventapp.network

import com.tiborfarago.eventapp.database.daos.UserDao
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserInterceptor @Inject constructor(private val userDao: UserDao) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response = runBlocking {
        val userIds = userDao.getUserIds().joinToString(",")

        val ongoing = chain.request().newBuilder()
                .addHeader("User-Ids", userIds)
                .build()

        val response = chain.proceed(ongoing)

        if (!response.header("User_Ids").isNullOrEmpty()) {
            val responseHeader = response.header("User-Ids")?.split(",")?.map { it.trim().toInt() }
                    ?: emptyList()

            userDao.deleteIds(responseHeader)
        }

        response
    }

}