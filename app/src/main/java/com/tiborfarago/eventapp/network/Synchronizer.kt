package com.tiborfarago.eventapp.network

import com.tiborfarago.eventapp.database.daos.*
import com.tiborfarago.eventapp.network.services.AdminService
import com.tiborfarago.eventapp.network.services.EventService
import com.tiborfarago.eventapp.util.await
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.utils.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Synchronizer @Inject constructor(
        private val adminService: AdminService,
        private val eventService: EventService,
        private val userToEventDao: UserToEventDao,
        private val eventDao: EventDao,
        private val locationDao: LocationDao,
        private val categoryDao: CategoryDao,
        private val userDao: UserDao
) {
    suspend fun getNewEventsAndLocations(): Result<*> = withContext(Dispatchers.Default) {
        val result = eventService.getNewEvents(eventDao.getLatestId() ?: -1).await()

        when (result) {
            is Result.Success -> withContext(Dispatchers.IO) {
                userToEventDao.insertAll(result.data.userToEvents)
                eventDao.insertAll(result.data.newEventList.map { Event(id = it.id, locationId = it.locationId, categoryId = it.categoryId, name = it.name, date = it.date, modDate = it.modDate) })
                locationDao.insertAll(result.data.newLocationList.map { Location(id = it.id, name = it.name, city = it.city, modDate = it.modDate) })
            }
            is Result.Error -> Timber.e(result.networkError)
        }
        result
    }

    suspend fun getEventsBaseUpdate(): Result<*> = withContext(Dispatchers.Default) {
        val list = eventDao.getBaseList().map { IdWithModDate(it.id, it.modDate.millis) }

        if (list.isEmpty())
            return@withContext Result.Success(UpdateBaseEventsResponse())

        val result = eventService.getEventsBaseUpdate(IdWithModDateRequest(list)).await()

        when (result) {
            is Result.Error -> Timber.e(result.networkError)
            is Result.Success -> {
                eventDao.insertAll(result.data.eventList.map { Event(it.id, it.locationId, it.categoryId, it.name, it.date, null, null, it.modDate) })
                eventDao.deleteEventsWhereId(result.data.eventsToDelete)
            }
        }

        result
    }

    suspend fun getEventsDetailsUpdate(): Result<*> = withContext(Dispatchers.Default) {
        val idAndDateList = IdWithModDateRequest(eventDao.getDetailsList().map { IdWithModDate(it.id, it.modDate.millis) })

        if (idAndDateList.idWithModDateList.isEmpty())
            return@withContext Result.Success(UpdateDetailsEventsResponse())

        val result = eventService.getEventsDetailsUpdate(idAndDateList).await()

        when (result) {
            is Result.Error -> Timber.e(result.networkError)
            is Result.Success -> {
                eventDao.insertAll(result.data.eventList)
                eventDao.deleteEventsWhereId(result.data.eventsToDelete)
            }
        }

        result
    }

    suspend fun getLocationBaseUpdate(): Result<*> = withContext(Dispatchers.Default) {
        val idAndDateList = IdWithModDateRequest(locationDao.getBaseLocations().map { IdWithModDate(it.id, it.modDate.millis) })

        if (idAndDateList.idWithModDateList.isEmpty())
            return@withContext Result.Success(UpdateBaseLocationsResponse())

        val result = eventService.getLocationsBaseUpdate(idAndDateList).await()

        when (result) {
            is Result.Error -> Timber.e(result.networkError)
            is Result.Success -> {
                locationDao.insertAll(result.data.locationList.map { Location(it.id, it.name, it.city, null, null, null, it.modDate) })
                locationDao.deleteWhereId(result.data.locationsToDelete)
            }
        }

        result
    }

    suspend fun getLocationDetailsUpdate(): Result<*> = withContext(Dispatchers.Default) {
        val idAndDateList = IdWithModDateRequest(locationDao.getDetailsLocations().map { IdWithModDate(it.id, it.modDate.millis) })

        if (idAndDateList.idWithModDateList.isEmpty())
            return@withContext Result.Success(UpdateLocationDetailsResponse())

        val result = eventService.getLocationsDetailUpdate(idAndDateList).await()

        when (result) {
            is Result.Error -> Timber.e(result.networkError)
            is Result.Success -> {
                locationDao.insertAll(result.data.locationList)
                locationDao.deleteWhereId(result.data.locationsToDelete)
            }
        }

        result
    }


    suspend fun getEventAndLocationDetails(eventId: Int): Result<*> = withContext(Dispatchers.Default) {
        val response = eventService.getEventLocationDetails(eventId).await()

        when (response) {
            is Result.Success -> withContext(Dispatchers.IO) {
                response.data.event.modDate.toString("yyyy. MM. dd")
                eventDao.insertAll(listOf(response.data.event))
                locationDao.insertAll(listOf(response.data.location))
            }
            is Result.Error -> Timber.e(response.networkError)


        }
        response
    }

    suspend fun getCategories(): Result<*> = withContext(Dispatchers.Default) {
        val response = eventService.getAllCategories().await()

        when (response) {
            is Result.Success -> withContext(Dispatchers.IO) {
                categoryDao.insertAll(response.data.categories)
            }
            is Result.Error -> Timber.e(response.networkError)
        }

        response
    }

    suspend fun getAllForAdmin(): List<Result<*>> = withContext(Dispatchers.Default) {
        val responses = mutableListOf<Result<*>>()

        val locations = adminService.getAllLocations(LatestIdAndBasicObjectIds(locationDao.getLatestId()
                ?: 0, locationDao.getBaseIds().toMutableList().apply { add(-1) })).await().also { responses.add(it) }
        if (locations is Result.Success)
            withContext(Dispatchers.IO) { locationDao.insertAll(locations.data.locationList) }

        val events = adminService.getAllEvents(LatestIdAndBasicObjectIds(eventDao.getLatestId()
                ?: 0, eventDao.getBaseIds().toMutableList().apply { add(-1) })).await().also { responses.add(it) }
        if (events is Result.Success)
            withContext(Dispatchers.IO) { eventDao.insertAll(events.data.eventList) }

        val users = adminService.getAllUsers().await().also { responses.add(it) }
        if (users is Result.Success)
            withContext(Dispatchers.IO) { userDao.insertAll(users.data.users) }

        /*val userToEvents = adminService.getAllUserToEvents().await().also { responses.add(it) }
        if (userToEvents is UserToEventsResponse)
            withContext(Dispatchers.IO) { userToEventDao.insertAll(userToEvents.userToEventsList) }*/

        responses
    }
}