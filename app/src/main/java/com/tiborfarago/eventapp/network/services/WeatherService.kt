package com.tiborfarago.eventapp.network.services

import com.tiborfarago.eventapp.network.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast")
    fun getFiveDayForecast(
            @Query("lat") latitude: String,
            @Query("lon") longitude: String,
            @Query("APPID") appId: String = "f812135c130d40daa3a60c9ae6cf356f",
            @Query("units") units: String = "metric",
            @Query("lang") lang: String = "hu"
    ): Call<WeatherResponse>
}