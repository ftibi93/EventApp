package com.tiborfarago.eventapp.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

@JsonClass(generateAdapter = true)
data class MainObject(
        @Json(name = "temp") var temp: Double = 0.0
)

@JsonClass(generateAdapter = true)
data class WeatherObjectItem(
        @Json(name = "description") var description: String = ""
)

@JsonClass(generateAdapter = true)
data class WeatherObject(
        @Json(name = "weather") var weatherObjectItems: List<WeatherObjectItem> = emptyList(),
        @Json(name = "main") var mainObject: MainObject,
        @Json(name = "dt_txt") var dtText: String = ""
) {
    fun getDate(): DateTime = DateTime.parse(dtText, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"))
}

@JsonClass(generateAdapter = true)
data class WeatherResponse(
        @Json(name = "list") var weatherObjects: List<WeatherObject> = emptyList()
)