package com.tiborfarago.eventapp.work_manager.sync

import android.content.Context
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.tiborfarago.eventapp.database.daos.CategoryDao
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.util.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import javax.inject.Inject

class CategorySyncWorker(private val context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    @Inject
    lateinit var categoryDao: CategoryDao
    @Inject
    lateinit var synchronizer: Synchronizer

    override fun doWork(): Result = runBlocking(Dispatchers.Default) {
        (context.applicationContext as App).applicationComponent.inject(this@CategorySyncWorker)

        val result = synchronizer.getCategories()

        if (result is com.tiborfarago.resource_module.utils.Result.Error) {
            Timber.d(result.toString())
            return@runBlocking Result.retry()
        }

        return@runBlocking Result.success()
    }

    companion object {
        fun runNow() {
            WorkManager.getInstance().enqueue(OneTimeWorkRequest.from(CategorySyncWorker::class.java))
        }
    }
}