package com.tiborfarago.eventapp.work_manager

import android.content.Context
import android.util.Log
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.util.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class NewEventsWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    @Inject
    lateinit var synchronizer: Synchronizer

    override fun doWork() = runBlocking(Dispatchers.Default) {
        (applicationContext as App).applicationComponent.inject(this@NewEventsWorker)

        val result = synchronizer.getNewEventsAndLocations()
        when (result) {
            is com.tiborfarago.resource_module.utils.Result.Error -> {
                Log.e("TESZT", "${result.networkError.errorCode} - ${result.networkError.errorMessage}")
            }
        }

        val categoryResult = synchronizer.getCategories()
        when (categoryResult) {
            is com.tiborfarago.resource_module.utils.Result.Error -> Log.e("TESZT", "${categoryResult.networkError.errorCode} - ${categoryResult.networkError.errorMessage}")
        }

        return@runBlocking Result.success()
    }

    companion object {
        fun startNow() {
            val worker = OneTimeWorkRequestBuilder<NewEventsWorker>().build()
            WorkManager.getInstance().enqueue(worker)
        }
    }
}