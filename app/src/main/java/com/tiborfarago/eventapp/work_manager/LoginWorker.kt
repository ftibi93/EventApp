package com.tiborfarago.eventapp.work_manager

import android.content.Context
import android.content.SharedPreferences
import androidx.work.*
import com.tiborfarago.eventapp.database.daos.UserDao
import com.tiborfarago.eventapp.database.daos.UserToEventDao
import com.tiborfarago.eventapp.network.services.UserAdminService
import com.tiborfarago.eventapp.util.App
import com.tiborfarago.eventapp.util.SharedPrefKeys
import com.tiborfarago.eventapp.util.await
import com.tiborfarago.resource_module.classes.LoginRequest
import com.tiborfarago.resource_module.room.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class LoginWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    @Inject
    lateinit var userAdminService: UserAdminService
    @Inject
    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var userToEventDao: UserToEventDao
    @Inject
    lateinit var userDao: UserDao

    override fun doWork(): Result = runBlocking(Dispatchers.Default) {
        (applicationContext as App).applicationComponent.inject(this@LoginWorker)

        val userName = inputData.getString(USER_NAME_EXTRA) ?: ""
        val password = inputData.getString(PASSWORD_EXTRA) ?: ""

        val result = userAdminService.login(LoginRequest(userName, password)).await()
        when (result) {
            is com.tiborfarago.resource_module.utils.Result.Success -> {
                Timber.d(result.toString())
                if (result.data.id == -1) {
                    saveUser(userName)

                    return@runBlocking Result.success(workDataOf(
                            RESULT_SUCCESS to false,
                            RESULT_TEXT to "Helytelen bejelentkezési adatok"))
                } else {
                    saveUser("")

                    withContext(Dispatchers.IO) {
                        userDao.insert(User(result.data.id, userName, password, 0, DateTime(result.data.modDate)))
                        userToEventDao.insertAll(result.data.userToEvents)
                    }

                    return@runBlocking Result.success(workDataOf(
                            RESULT_SUCCESS to true,
                            RESULT_TEXT to "Sikeres bejelentkezés"))
                }
            }
            is com.tiborfarago.resource_module.utils.Result.Error -> {
                saveUser(userName)
                return@runBlocking Result.success(workDataOf(
                        RESULT_SUCCESS to false,
                        RESULT_TEXT to result.networkError.errorMessage))
            }
        }
    }

    private fun saveUser(userName: String) {
        sharedPreferences.edit().putString(SharedPrefKeys.LOGIN_DIALOG_USER, userName).apply()
    }

    companion object {
        const val TAG = "loginworker_tag"
        const val RESULT_SUCCESS = "login_result_success"
        const val RESULT_TEXT = "login_result_key"
        private const val USER_NAME_EXTRA = "login_user_name_extra"
        private const val PASSWORD_EXTRA = "login_password_extra"

        var workerId: UUID = UUID.randomUUID()

        fun schedule(userName: String, password: String) {
            val data: Data = workDataOf(USER_NAME_EXTRA to userName, PASSWORD_EXTRA to password)
            val worker = OneTimeWorkRequestBuilder<LoginWorker>().setInputData(data).addTag(TAG).build()
            workerId = worker.id

            WorkManager.getInstance().enqueue(worker)
        }
    }
}