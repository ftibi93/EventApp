package com.tiborfarago.eventapp.work_manager.sync

import android.content.Context
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.database.daos.LocationDao
import com.tiborfarago.eventapp.database.daos.UserToEventDao
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.util.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import javax.inject.Inject

class EventSyncWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    @Inject
    lateinit var synchronizer: Synchronizer
    @Inject
    lateinit var eventDao: EventDao
    @Inject
    lateinit var locationDao: LocationDao
    @Inject
    lateinit var userToEventDao: UserToEventDao

    override fun doWork(): Result = runBlocking(Dispatchers.Default) {
        (applicationContext as App).applicationComponent.inject(this@EventSyncWorker)

        val result = synchronizer.getNewEventsAndLocations()
        if (result is com.tiborfarago.resource_module.utils.Result.Error)
            Timber.e(result.networkError)

        val result2 = synchronizer.getEventsBaseUpdate()
        if (result2 is com.tiborfarago.resource_module.utils.Result.Error)
            Timber.e(result2.networkError)

        val result3 = synchronizer.getEventsDetailsUpdate()
        if (result3 is com.tiborfarago.resource_module.utils.Result.Error)
            Timber.e(result3.networkError)

        return@runBlocking Result.success()
    }

    companion object {
        fun runNow() {
            WorkManager.getInstance().enqueue(OneTimeWorkRequest.from(EventSyncWorker::class.java))
        }
    }
}