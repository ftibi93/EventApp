package com.tiborfarago.eventapp.di

import com.tiborfarago.eventapp.activities.MainActivity
import com.tiborfarago.eventapp.di.viewmodel.ViewModelModule
import com.tiborfarago.eventapp.fragments.*
import com.tiborfarago.eventapp.fragments.dialogs.AddSubscriptionDialog
import com.tiborfarago.eventapp.fragments.dialogs.FilterDialogFragment
import com.tiborfarago.eventapp.work_manager.LoginWorker
import com.tiborfarago.eventapp.work_manager.NewEventsWorker
import com.tiborfarago.eventapp.work_manager.sync.CategorySyncWorker
import com.tiborfarago.eventapp.work_manager.sync.EventSyncWorker
import com.tiborfarago.eventapp.work_manager.sync.LocationSyncWorker
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    RetrofitModule::class,
    AppModule::class,
    DatabaseModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(mainFragment: MainFragment)
    fun inject(subscriptionFragment: SubscriptionFragment)
    fun inject(loginWorker: LoginWorker)
    fun inject(addSubscriptionDialog: AddSubscriptionDialog)
    fun inject(newEventsWorker: NewEventsWorker)
    fun inject(eventDetailsFragment: EventDetailsFragment)
    fun inject(adminFragment: AdminFragment)
    fun inject(adminCategoriesFragment: AdminCategoriesFragment)

    fun inject(addLocationFragment: AddLocationFragment)
    fun inject(adminLocationsFragment: AdminLocationsFragment)

    fun inject(addEventFragment: AddEventFragment)
    fun inject(adminEventsFragment: AdminEventsFragment)

    fun inject(adminUsersFragment: AdminUsersFragment)
    fun inject(categorySyncWorker: CategorySyncWorker)
    fun inject(eventSyncWorker: EventSyncWorker)
    fun inject(locationSyncWorker: LocationSyncWorker)
    fun inject(filterDialogFragment: FilterDialogFragment)
}