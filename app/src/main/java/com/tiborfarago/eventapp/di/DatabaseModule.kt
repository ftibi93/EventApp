package com.tiborfarago.eventapp.di

import android.content.Context
import androidx.room.Room
import com.tiborfarago.eventapp.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private val context: Context) {
    @Singleton
    @Provides
    fun provideAppDatabase() = Room.databaseBuilder(context, AppDatabase::class.java, "event_database").build()

    @Singleton
    @Provides
    fun provideEventDao(appDatabase: AppDatabase) = appDatabase.eventDao()

    @Singleton
    @Provides
    fun provideUserDao(appDatabase: AppDatabase) = appDatabase.userDao()

    @Singleton
    @Provides
    fun providesUserToEventsDao(appDatabase: AppDatabase) = appDatabase.userToEventDao()

    @Singleton
    @Provides
    fun providesLocationDao(appDatabase: AppDatabase) = appDatabase.locationDao()

    @Singleton
    @Provides
    fun providesCategoryDao(appDatabase: AppDatabase) = appDatabase.categoryDao()

    @Singleton
    @Provides
    fun providesChangeDao(appDatabase: AppDatabase) = appDatabase.changeDao()

    @Singleton
    @Provides
    fun providesScheduleDao(appDatabase: AppDatabase) = appDatabase.scheduleDao()

    @Singleton
    @Provides
    fun providesCoverUriDao(appDatabase: AppDatabase) = appDatabase.coverUriDao()

    @Singleton
    @Provides
    fun providesFavoriteDao(appDatabase: AppDatabase) = appDatabase.favoriteDao()
}