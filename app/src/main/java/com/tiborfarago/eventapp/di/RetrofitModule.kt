package com.tiborfarago.eventapp.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.tiborfarago.eventapp.network.AdminInterceptor
import com.tiborfarago.eventapp.network.services.AdminService
import com.tiborfarago.eventapp.network.services.EventService
import com.tiborfarago.eventapp.network.services.UserAdminService
import com.tiborfarago.eventapp.network.UserInterceptor
import com.tiborfarago.eventapp.network.services.WeatherService
import com.tiborfarago.eventapp.util.Constants
import com.tiborfarago.resource_module.moshi.MoshiObject
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {
    @Provides
    @Singleton
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    @Singleton
    @Named("client")
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, userInterceptor: UserInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .addInterceptor(userInterceptor)
                    .addNetworkInterceptor(StethoInterceptor())
                    .build()

    @Provides
    @Singleton
    @Named("admin_client")
    fun providesAdminOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, adminInterceptor: AdminInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .addInterceptor(adminInterceptor)
                    .addNetworkInterceptor(StethoInterceptor())
                    .build()

    @Provides
    @Singleton
    @Named("event_api")
    fun providesRetrofit(@Named("client") okHttpClient: OkHttpClient) : Retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(MoshiObject.moshiObject))
            .client(okHttpClient)
            .baseUrl("${Constants.BASE_URL}/${Constants.API_URL}/")
            .build()

    @Provides
    @Singleton
    @Named("event_api_admin")
    fun providesAdminRetrofit(@Named("admin_client") okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(MoshiObject.moshiObject))
            .client(okHttpClient)
            .baseUrl("${Constants.BASE_URL}/${Constants.API_URL}/")
            .build()

    @Provides
    @Singleton
    fun providesUserAdminService(@Named("event_api") retrofit: Retrofit): UserAdminService = retrofit.create(UserAdminService::class.java)

    @Provides
    @Singleton
    fun providesEventService(@Named("event_api") retrofit: Retrofit): EventService = retrofit.create(EventService::class.java)

    @Provides
    @Singleton
    fun providesAdminService(@Named("event_api_admin") retrofit: Retrofit): AdminService = retrofit.create(AdminService::class.java)

    @Provides
    @Singleton
    @Named("weather_api")
    fun providesWeatherRetrofit(@Named("client") okHttpClient: OkHttpClient) : Retrofit = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .client(okHttpClient)
            .baseUrl("http://api.openweathermap.org/data/2.5/")
            .build()

    @Provides
    @Singleton
    fun providesWeatherService(@Named("weather_api") weatherApi: Retrofit): WeatherService = weatherApi.create(WeatherService::class.java)
}