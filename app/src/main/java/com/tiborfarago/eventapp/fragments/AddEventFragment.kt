package com.tiborfarago.eventapp.fragments


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.button.MaterialButton
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.AddEventScheduleAdapter
import com.tiborfarago.eventapp.adapters.AddEventUserAdapter
import com.tiborfarago.eventapp.database.CoverUriRow
import com.tiborfarago.eventapp.database.ScheduleRow
import com.tiborfarago.eventapp.database.daos.*

import com.tiborfarago.eventapp.database.views.LocationNameWithId
import com.tiborfarago.eventapp.databinding.FragmentAddEventBinding
import com.tiborfarago.eventapp.fragments.dialogs.DatePickerDialogFragment
import com.tiborfarago.eventapp.fragments.dialogs.TimePickerDialogFragment
import com.tiborfarago.eventapp.simplestack.AddEventKey
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.*
import com.tiborfarago.eventapp.viewmodels.AddEventViewModel
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.UserToEvents
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_add_event.*
import kotlinx.coroutines.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.util.*
import javax.inject.Inject

class AddEventFragment : BaseFragment() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var eventDao: EventDao
    @Inject
    lateinit var categoryDao: CategoryDao
    @Inject
    lateinit var locationDao: LocationDao
    @Inject
    lateinit var scheduleDao: ScheduleDao
    @Inject
    lateinit var userToEventDao: UserToEventDao
    @Inject
    lateinit var changeDao: ChangeDao
    @Inject
    lateinit var coverUriDao: CoverUriDao
    @Inject
    lateinit var scheduleStringConverter: ScheduleStringConverter

    private lateinit var binding: FragmentAddEventBinding
    private lateinit var vm: AddEventViewModel

    private lateinit var userAdapter: AddEventUserAdapter
    private lateinit var scheduleAdapter: AddEventScheduleAdapter

    private var checkedUsers: HashSet<Int> = HashSet()
    private lateinit var categories: List<Category>
    private lateinit var locationNamesWithIds: List<LocationNameWithId>

    private var eventName: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAddEventBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        app().applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this, viewModelProvider)[AddEventViewModel::class.java]
        binding.setLifecycleOwner(this)
        binding.vm = vm

        launch(Dispatchers.Default) {
            scheduleDao.deleteAll()
            categories = categoryDao.getCategories()
            locationNamesWithIds = locationDao.getLocationNamesAndIds()

            withContext(Dispatchers.Main) {
                getKey<AddEventKey>().eventId?.let {
                    val event = withContext(Dispatchers.IO) { eventDao.getEventById(it) }
                            ?: return@let
                    eventName = event.name

                    binding.event = event
                    vm.date.postValue(event.date)
                    vm.time.postValue(event.date)

                    binding.autoCompleteCategoryTextView.setText(withContext(Dispatchers.IO) {
                        categoryDao.getCategoryNameById(event.categoryId) ?: ""
                    })
                    binding.autoCompleteLocationTextView.setText(withContext(Dispatchers.IO) {
                        locationDao.getLocationNameById(event.locationId) ?: ""
                    })

                    withContext(Dispatchers.Default) {
                        scheduleDao.insertAll(scheduleStringConverter.stringToSchedule(it))
                    }

                    checkedUsers = HashSet(withContext(Dispatchers.IO) { userToEventDao.getUsersForEvent(it) })

                    vm.coverPath.postValue(withContext(Dispatchers.IO) { coverUriDao.getUriForEvent(it) })
                }

                auto_complete_category_text_view.apply {
                    setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, android.R.id.text1, categories.map { it.name }))
                    setOnFocusChangeListener { view, b ->
                        if (b) showDropDown()
                        else launch(Dispatchers.Default) {
                            if (categoryDao.getCategoryByName(text.toString()) == null && text.toString().isNotEmpty())
                                withContext(Dispatchers.Main) { error = "Csak a listában szereplő kategóriák hsaználhatók" }
                        }
                    }
                }

                auto_complete_location_text_view.apply {
                    setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, locationNamesWithIds.map { it.name }))
                    setOnFocusChangeListener { view, b ->
                        if (b) showDropDown()
                        else launch(Dispatchers.Default) {
                            if (locationDao.getLocationByName(text.toString()) == null && text.toString().isNotEmpty())
                                withContext(Dispatchers.Main) { error = "Csak a listában szereplő helyszínek használhatók" }
                        }
                    }
                }

                userAdapter = AddEventUserAdapter(checkedUsers) { user, isChecked ->
                    user?.let {
                        if (isChecked) checkedUsers.add(it.id)
                        else checkedUsers.remove(it.id)
                    }
                }

                users_recycler_view.adapter = userAdapter

                vm.userList.observe(this@AddEventFragment, Observer {
                    userAdapter.submitList(it)
                })

            }
        }

        date_button.setOnClickListener {
            val dialog = DatePickerDialogFragment().apply {
                arguments = Bundle().apply { putString(DatePickerDialogFragment.DATE_EXTRA, (it as MaterialButton).text.toString()) }
                setTargetFragment(this@AddEventFragment, REQUEST_CODE_DATE)
            }
            dialog.show(requireFragmentManager(), "date_dialog")
        }

        time_button.setOnClickListener {
            val dialog = TimePickerDialogFragment().apply {
                arguments = Bundle().apply { putString(TimePickerDialogFragment.TIME_EXTRA, (it as MaterialButton).text.toString()) }
                setTargetFragment(this@AddEventFragment, REQUEST_CODE_TIME)
            }
            dialog.show(requireFragmentManager(), "time_dialog")
        }

        button_close.setOnClickListener {
            mainActivity().goBack()
        }

        add_cover_button.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).addCategory(Intent.CATEGORY_OPENABLE).setType("image/*")
            startActivityForResult(intent, REQUEST_COVER_PICKER)
        }

        mainActivity().main_fab.apply {
            setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_save_white_24dp))
            show()
            setOnClickListener {
                saveEvent()
            }
        }

        /**
         * Azért kell késleltetés, mert beállítódik az adapter, bekerülnek az adatok
         * és amikor bekerülnek, meghívódnak az EditText-ek TextChangedListener-jei,
         * emiatt pedig újra beszúródnak (vagy valami ilyesmi)
         */
        launch {
            delay(500)
            scheduleAdapter = AddEventScheduleAdapter(
                    onDelete = { this@AddEventFragment.launch(Dispatchers.IO) { this@AddEventScheduleAdapter?.let { it1 -> scheduleDao.delete(it1) } } },
                    onTimeChange = { isStart, scheduleRow ->
                        val dialog = TimePickerDialogFragment().apply {
                            setTargetFragment(this@AddEventFragment, REQUEST_CODE_TIME)
                            arguments = Bundle().apply {
                                putString(TimePickerDialogFragment.TIME_EXTRA, if (isStart) scheduleRow?.start?.toString("HH:mm") else scheduleRow?.end?.toString("HH:mm"))
                                putBoolean(TimePickerDialogFragment.IS_START_EXTRA, isStart)
                                putInt(TimePickerDialogFragment.ID_EXTRA, scheduleRow?.id ?: 0)
                            }
                        }

                        dialog.show(requireFragmentManager(), "time_dialog")
                    },
                    onTextChange = { text, scheduleRow ->
                        this@AddEventFragment.launch(Dispatchers.IO) {
                            scheduleDao.insert(scheduleRow?.apply { title = text })
                        }
                    }
            )

            schedule_recycler_view.adapter = scheduleAdapter

            vm.scheduleList.observe(this@AddEventFragment, Observer {
                scheduleAdapter.submitList(it)
            })
        }

        add_schedule_item_button.setOnClickListener {
            launch(Dispatchers.IO) {
                scheduleDao.insert(ScheduleRow())
            }
        }

        vm.coverPath.observe(this, Observer {
            if (it != null) cover_image_view.setImageURI(Uri.parse(it))
        })
    }

    private fun saveEvent() {
        launch {
            if (withContext(Dispatchers.IO) { eventDao.getEventCountByName(event_name.text.toString()) } > 0 && event_name.text.toString() != eventName) {
                showSnackbar("Létező esemény név")
                return@launch
            }

            if (binding.autoCompleteCategoryTextView.error.isNullOrEmpty().not() || binding.autoCompleteLocationTextView.error.isNullOrEmpty().not()) {
                return@launch
            }

            if (date_button.text.toString().contains(".").not()) {
                showSnackbar("Dátum megadása kötelező")
                return@launch
            }

            if (description_tv.text.toString().isEmpty()) {
                showSnackbar("Leírás megadása kötelező")
                return@launch
            }

            val dateInts = date_button.text.toString().split(" ").map { it.dropLast(1).toInt() }

            withContext(Dispatchers.Default) {
                delay(1000)
                var id = 0
                val event = Event(
                        locationId = locationDao.getLocationByName(auto_complete_location_text_view.text.toString())?.id
                                ?: -1,
                        categoryId = categoryDao.getCategoryByName(auto_complete_category_text_view.text.toString())?.id
                                ?: -1,
                        name = event_name.text.toString(),
                        date = DateTime(dateInts[0], dateInts[1], dateInts[2],
                                if (time_button.text.toString().contains(":")) time_button.text.toString().split(":")[0].toInt().plus(0) else 0,
                                if (time_button.text.toString().contains(":")) time_button.text.toString().split(":")[1].toInt().plus(0) else 0
                        ).withZoneRetainFields(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Europe/Budapest"))),
                        description = edit_text_description.text.toString(),
                        modDate = DateTime.now(),
                        schedule = scheduleStringConverter.scheduleToString()
                )

                if (eventName == null) {
                    id = eventDao.insert(event).toInt()
                    changeDao.insert(Change(Event.TABLE_NAME, id, Change.TYPE.ADD))
                } else {
                    getKey<AddEventKey>().eventId?.let {
                        id = it
                        userToEventDao.deleteForEvents(it)
                        eventDao.update(event.apply {
                            this.id = it
                        })
                        changeDao.insert(Change(Event.TABLE_NAME, it, Change.TYPE.MODIFY))
                    }
                }

                userToEventDao.insertAll(checkedUsers.map { UserToEvents(it, id, DateTime.now()) })

                vm.coverPath.value?.let {
                    coverUriDao.insert(CoverUriRow(id, it))
                }

            }

            showSnackbar("Esemény mentve")
            mainActivity().goBack()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) return

        when (requestCode) {
            REQUEST_CODE_DATE -> {
                val dateString = data.getStringExtra(REQUEST_CODE_DATE.toString())
                val date = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(dateString)
                vm.date.postValue(date)
            }
            REQUEST_CODE_TIME -> {
                val timeString = data.getStringExtra(REQUEST_CODE_TIME.toString())
                val time = DateTimeFormat.forPattern("HH:mm").parseDateTime(timeString).minusHours(1)

                if (data.getIntExtra(TimePickerDialogFragment.ID_EXTRA, -1) == -1) {
                    vm.time.postValue(time)
                } else {
                    val id = data.getIntExtra(TimePickerDialogFragment.ID_EXTRA, 0)
                    val isStart = data.getBooleanExtra(TimePickerDialogFragment.IS_START_EXTRA, false)

                    launch(Dispatchers.IO) {
                        scheduleDao.insert(scheduleDao.getSchedule(id)?.apply {
                            if (isStart) start = time
                            else end = time
                        })
                    }
                }
            }
            REQUEST_COVER_PICKER -> vm.coverPath.postValue(data.data.toString())
        }
    }

    companion object {
        const val REQUEST_CODE_DATE = 111
        const val REQUEST_CODE_TIME = 112

        const val REQUEST_COVER_PICKER = 113
    }
}
