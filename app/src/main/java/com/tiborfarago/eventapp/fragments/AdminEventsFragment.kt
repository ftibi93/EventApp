package com.tiborfarago.eventapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.SimpleItemManagerAdapter
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.simplestack.AddEventKey
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.app
import com.tiborfarago.eventapp.util.mainActivity
import com.tiborfarago.eventapp.viewmodels.EventsViewModel
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.Event
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class AdminEventsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var eventDao: EventDao
    @Inject
    lateinit var changeDao: ChangeDao

    private lateinit var eventsViewModel: EventsViewModel

    private lateinit var adapter: SimpleItemManagerAdapter<Event>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        app().applicationComponent.inject(this)
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        eventsViewModel = ViewModelProviders.of(this, viewModelFactory)[EventsViewModel::class.java]

        adapter = SimpleItemManagerAdapter(
                modifyClick = {
                    mainActivity().navigateTo(AddEventKey(eventId = it?.id))
                },
                deleteClick = {
                    val builder = AlertDialog.Builder(requireContext())
                            .setMessage("Biztos, hogy törli az eseményt?")
                            .setPositiveButton("Igen") { _, _ ->
                                launch(Dispatchers.Default) {
                                    if (it != null) deleteEvent(it)
                                }
                            }
                            .setNegativeButton("Nem") { _, _ -> }

                    builder.create().show()
                }
        )

        categories_recycler_view.adapter = adapter

        eventsViewModel.events.observe(this, Observer {
            adapter.submitList(it)
        })

        mainActivity().main_fab.hide()
    }

    private fun deleteEvent(event: Event) {
        val changeRow = changeDao.getChangeByTableAndRow(Event.TABLE_NAME, event.id)

        if (changeRow == null || changeRow.type != Change.TYPE.ADD)
            changeDao.insert(Change(Event.TABLE_NAME, event.id, Change.TYPE.DELETE))

        eventDao.delete(event)
    }
}
