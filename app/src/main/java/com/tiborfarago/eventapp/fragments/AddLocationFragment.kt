package com.tiborfarago.eventapp.fragments


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.places.ui.PlacePicker

import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.LocationDao
import com.tiborfarago.eventapp.databinding.FragmentAddLocationBinding
import com.tiborfarago.eventapp.simplestack.AddLocationKey
import com.tiborfarago.eventapp.simplestack.AdminKey
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.app
import com.tiborfarago.eventapp.util.mainActivity
import com.tiborfarago.eventapp.util.showSnackbar
import com.tiborfarago.eventapp.viewmodels.LocationsViewModel
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.Location
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_add_location.*
import kotlinx.coroutines.*
import org.joda.time.DateTime
import java.math.BigDecimal
import javax.inject.Inject

class AddLocationFragment : BaseFragment() {

    private val geoCoder by lazy { Geocoder(requireContext()) }

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var locationDao: LocationDao
    @Inject
    lateinit var changeDao: ChangeDao

    private var locationId: Int = -1

    private lateinit var binding: FragmentAddLocationBinding

    private lateinit var viewModel: LocationsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAddLocationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        app().applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        binding.setLifecycleOwner(this)

        viewModel = ViewModelProviders.of(this, viewModelProvider)[LocationsViewModel::class.java]

        locationId = getKey<AddLocationKey>().locationId ?: -1

        if (locationId != -1) {
            launch {
                binding.location = withContext(Dispatchers.IO) { locationDao.location(locationId) }
            }
            binding.fragmentAddLocationTitle.text = "Helyszín módosítása"
        }

        mainActivity().main_fab.apply {
            setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_save_white_24dp))
            show()
            setOnClickListener {
                saveLocation()
            }
        }

        button_place_picker.setOnClickListener {
            val picker = PlacePicker.IntentBuilder().build(requireActivity())
            startActivityForResult(picker, PICKER_REQUEST_CODE)
        }

        button_close.setOnClickListener {
            mainActivity().goBack()
        }

    }

    private fun saveLocation() {
        if (
                location_address.text.toString().isEmpty()
                || location_city.text.toString().isEmpty()
                || location_latitude.text.toString().isEmpty()
                || location_longitude.text.toString().isEmpty()
                || location_name.text.toString().isEmpty()) {
            showSnackbar("Minden mező kitöltése kötelező")
            return
        }

        if (locationId == -1) {
            insertLocation()
        } else {
            updateLocation()
        }
    }

    private fun insertLocation() {
        launch(Dispatchers.IO) {
            val id = locationDao.insert(Location(0,
                    location_name.text.toString(),
                    location_city.text.toString(),
                    location_address.text.toString(),
                    location_longitude.text.toString().toBigDecimal(),
                    location_latitude.text.toString().toBigDecimal(),
                    DateTime.now()
            ))

            changeDao.insert(Change(Location.TABLE_NAME, id.toInt(), Change.TYPE.ADD))

            withContext(Dispatchers.Main) {
                showSnackbar("Helyszín mentve")
                mainActivity().navigateTo(AdminKey())
            }
        }
    }

    private fun updateLocation() {
        launch(Dispatchers.IO) {
            locationDao.update(Location(
                    locationId,
                    binding.locationName.text.toString(),
                    binding.locationCity.text.toString(),
                    binding.locationAddress.text.toString(),
                    BigDecimal(binding.locationLongitude.text.toString()),
                    BigDecimal(binding.locationLatitude.text.toString()),
                    DateTime.now()
            ))

            val changeRow = changeDao.getChangeByTableAndRow(Location.TABLE_NAME, locationId)
            if (changeRow == null)
                changeDao.insert(Change(Location.TABLE_NAME, locationId, Change.TYPE.MODIFY))

            withContext(Dispatchers.Main) {
                showSnackbar("Helyszín mentve")
                mainActivity().navigateTo(AdminKey())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            val place = PlacePicker.getPlace(requireContext(), data)

            val asd = geoCoder.getFromLocation(place.latLng.latitude, place.latLng.longitude, 1)[0]

            location_longitude.setText(asd.longitude.toString())
            location_latitude.setText(asd.latitude.toString())

            location_city.setText(asd.locality)
            location_address.setText("${asd.thoroughfare} ${asd.featureName}.")

            location_name.setText(place.name)
        }
    }

    companion object {
        private const val PICKER_REQUEST_CODE = 193
    }
}
