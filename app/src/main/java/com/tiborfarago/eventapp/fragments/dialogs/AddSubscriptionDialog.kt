package com.tiborfarago.eventapp.fragments.dialogs

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.util.App
import com.tiborfarago.eventapp.util.SharedPrefKeys
import com.tiborfarago.eventapp.work_manager.LoginWorker
import kotlinx.android.synthetic.main.dialog_add_subscription.*
import javax.inject.Inject

class AddSubscriptionDialog : BottomSheetDialogFragment() {
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_add_subscription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (context?.applicationContext as App?)?.applicationComponent?.inject(this)
        super.onViewCreated(view, savedInstanceState)

        dialog_add_subscription_user_name.setText(sharedPreferences.getString(SharedPrefKeys.LOGIN_DIALOG_USER, ""))

        dialog_add_subscription_button_login.setOnClickListener {
            if (dialog_add_subscription_user_name.text.toString().isNotEmpty() && dialog_add_subscription_password.toString().isNotEmpty()) {
                LoginWorker.schedule(dialog_add_subscription_user_name.text.toString(), dialog_add_subscription_password.text.toString())
                this.dismiss()
            } else {
                dialog_add_subscription_user_name.text?.isEmpty()?.let {
                    dialog_add_subscription_user_name.error = "Nem lehet üres"
                }
                dialog_add_subscription_password.text?.isEmpty()?.let {
                    dialog_add_subscription_password.error = "Nem lehet üres"
                }
            }
        }
    }
}