package com.tiborfarago.eventapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputEditText
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.SimpleItemManagerAdapter
import com.tiborfarago.eventapp.database.daos.CategoryDao
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.App
import com.tiborfarago.eventapp.viewmodels.CategoriesViewModel
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Change
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import javax.inject.Inject

class AdminCategoriesFragment : BaseFragment() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var categoryDao: CategoryDao
    @Inject
    lateinit var changeDao: ChangeDao
    @Inject
    lateinit var eventDao: EventDao

    private lateinit var categoryAdminAdapter: SimpleItemManagerAdapter<Category>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (context?.applicationContext as App).applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        val vm = ViewModelProviders.of(this, viewModelProvider)[CategoriesViewModel::class.java]

        categoryAdminAdapter = SimpleItemManagerAdapter(
                {
                    if (it != null) modifyCategoryDialog(it)
                },
                {
                    val builder = AlertDialog.Builder(requireContext())
                            .setMessage("Biztos, hogy törli a kategóriát?")
                            .setPositiveButton("Igen") { _, _ ->
                                launch(Dispatchers.IO) {
                                    if (it != null) deleteCategoryIfPossible(it)
                                }
                            }
                            .setNegativeButton("Nem") { _, _ -> }

                    builder.create().show()
                })

        categories_recycler_view.adapter = categoryAdminAdapter

        vm.categoriesPaged.observe(this, Observer {
            categoryAdminAdapter.submitList(it)
        })
    }

    private suspend fun deleteCategoryIfPossible(category: Category) {
        val eventCount = eventDao.getEventCountByCategoryId(category.id)

        if (eventCount > 0) {
            withContext(Dispatchers.Main) {
                AlertDialog.Builder(requireContext())
                        .setPositiveButton("Ok") { _, _ -> }
                        .setTitle("Figyelem!")
                        .setMessage("A kategóriát $eventCount esemény használja, ezért nem törölhető")
                        .create()
                        .show()
            }
        } else deleteCategory(category)
    }

    private fun deleteCategory(category: Category) {
        val changeRow = changeDao.getChangeByTableAndRow(Category.TABLE_NAME, category.id)

        if (changeRow == null || changeRow.type != Change.TYPE.ADD)
            changeDao.insert(Change(Category.TABLE_NAME, category.id, Change.TYPE.DELETE))

        categoryDao.delete(category)
    }

    private fun modifyCategoryDialog(category: Category) {
        val view = layoutInflater.inflate(R.layout.dialog_add_category, null)
        val editText = view.findViewById<TextInputEditText>(R.id.dialog_category_edit_text)
        editText.setText(category.name)

        val builder = AlertDialog.Builder(requireContext())
                .setTitle("Kategória módosítása")
                .setView(view)
                .setPositiveButton("Mentés") { _, _ -> }
                .setNegativeButton("Mégse") { _, _ -> }

        builder.create()
                .apply {
                    show()
                    getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                        launch(Dispatchers.IO) {
                            if (categoryDao.getCategoryByName(editText.text.toString()) != null) {
                                withContext(Dispatchers.Main) { editText.error = "Ilyen kategória már létezik" }
                            } else {
                                categoryDao.update(category.apply {
                                    name = editText.text.toString()
                                    modDate = DateTime.now()
                                })

                                if (changeDao.getChangeByTableAndRow(Category.TABLE_NAME, category.id) == null)
                                    changeDao.insert(Change(Category.TABLE_NAME, category.id, Change.TYPE.MODIFY))

                                dismiss()
                            }
                        }
                    }
                }
    }
}
