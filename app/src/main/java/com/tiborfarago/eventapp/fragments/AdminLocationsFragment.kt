package com.tiborfarago.eventapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.SimpleItemManagerAdapter
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.database.daos.LocationDao
import com.tiborfarago.eventapp.simplestack.AddLocationKey
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.app
import com.tiborfarago.eventapp.util.mainActivity
import com.tiborfarago.eventapp.viewmodels.LocationsViewModel
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.Location
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AdminLocationsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var locationDao: LocationDao
    @Inject
    lateinit var changeDao: ChangeDao
    @Inject
    lateinit var eventDao: EventDao

    private lateinit var locationsAdapter: SimpleItemManagerAdapter<Location>

    private lateinit var viewModel: LocationsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        app().applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        mainActivity().main_fab.hide()

        viewModel = ViewModelProviders.of(this, viewModelProvider)[LocationsViewModel::class.java]

        locationsAdapter = SimpleItemManagerAdapter(
                {
                    mainActivity().navigateTo(AddLocationKey(locationId = it?.id))
                },
                {
                    val builder = AlertDialog.Builder(requireContext())
                            .setMessage("Biztos, hogy törli a helyszínt?")
                            .setPositiveButton("Igen") { _, _ ->
                                launch(Dispatchers.IO) {
                                    if (it != null) deleteLocationIfPossible(it)
                                }
                            }
                            .setNegativeButton("Nem") { _, _ -> }

                    builder.create().show()
                }
        )

        categories_recycler_view.adapter = locationsAdapter

        viewModel.locationsPagedList.observe(this, Observer {
            locationsAdapter.submitList(it)
        })

    }

    private suspend fun deleteLocationIfPossible(location: Location) {
        val eventCount = eventDao.getEventCountByLocationId(location.id)

        if (eventCount > 0) {
            withContext(Dispatchers.Main) {
                AlertDialog.Builder(requireContext())
                        .setPositiveButton("Ok") { _, _ -> }
                        .setTitle("Figyelem!")
                        .setMessage("A helyszínt $eventCount esemény használja, ezért nem törölhető")
                        .create()
                        .show()
            }
        } else deleteLocation(location)
    }

    private fun deleteLocation(location: Location) {
        val changeRow = changeDao.getChangeByTableAndRow(Location.TABLE_NAME, location.id)

        if (changeRow == null || changeRow.type != Change.TYPE.ADD)
            changeDao.insert(Change(Location.TABLE_NAME, location.id, Change.TYPE.DELETE))

        locationDao.delete(location)
    }
}
