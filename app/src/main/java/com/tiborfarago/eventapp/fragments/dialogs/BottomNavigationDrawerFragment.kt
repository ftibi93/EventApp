package com.tiborfarago.eventapp.fragments.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.simplestack.AdminKey
import com.tiborfarago.eventapp.simplestack.MainKey
import com.tiborfarago.eventapp.simplestack.SubscriptionKey
import com.tiborfarago.eventapp.util.mainActivity
import kotlinx.android.synthetic.main.layout_bottom_drawer.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BottomNavigationDrawerFragment @Inject constructor(): BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_bottom_drawer, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        bottom_drawer_navigation_view.setNavigationItemSelectedListener {
            val destination = when (it.itemId) {
                R.id.mainFragment -> MainKey()
                R.id.subscriptionFragment -> SubscriptionKey()
                R.id.menu_nav_admin -> AdminKey()
                else -> null
            }

            destination?.let { key ->
                mainActivity().replaceHistory(key)
                this.dismiss()
                true
            } ?: false
        }

        disableNavigationViewScrollbars(bottom_drawer_navigation_view, false)

        bottom_drawer_navigation_view.setCheckedItem(R.id.mainFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setOnShowListener { dialogInterface ->
            val d = dialog as BottomSheetDialog

            val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
            BottomSheetBehavior.from(bottomSheet).setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    if (slideOffset > 0.5) {
                        disableNavigationViewScrollbars(bottom_drawer_navigation_view, true)
                    } else {
                        disableNavigationViewScrollbars(bottom_drawer_navigation_view, false)
                    }
                }

                override fun onStateChanged(p0: View, state: Int) {
                    when (state) {
                        BottomSheetBehavior.STATE_HIDDEN -> dismiss()
                    }
                }

            })


        }

        return dialog
    }

    private fun disableNavigationViewScrollbars(navigationView: NavigationView?, isEnabled: Boolean) {
        val navigationMenuView = navigationView?.getChildAt(0) as NavigationMenuView
        navigationMenuView.isVerticalScrollBarEnabled = isEnabled
    }
}