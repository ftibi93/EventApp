package com.tiborfarago.eventapp.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.util.Constants
import kotlinx.android.synthetic.main.dialog_flyer_full_screen.*

class FlyerDialogFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_flyer_full_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getInt(ID_EXTRA, -1)

        Glide.with(requireContext())
                .load("${Constants.BASE_URL}/api/${Constants.FILE_URL}/downloadCover?id=$id")
                .into(full_screen_photo_view)

        full_screen_photo_view.setOnClickListener {
            this.dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
    }

    companion object {
        const val ID_EXTRA = "id_extra"
    }
}