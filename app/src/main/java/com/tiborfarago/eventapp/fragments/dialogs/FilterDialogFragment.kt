package com.tiborfarago.eventapp.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.FilterCategoryAdapter
import com.tiborfarago.eventapp.adapters.FilterLocationAdapter
import com.tiborfarago.eventapp.databinding.DialogFilterBinding
import com.tiborfarago.eventapp.util.SharedPreferencesManager
import com.tiborfarago.eventapp.util.app
import com.tiborfarago.eventapp.util.customviews.BottomSheetBehaviorGoogle
import com.tiborfarago.eventapp.viewmodels.MainFragmentViewModel
import com.tiborfarago.resource_module.room.Event
import javax.inject.Inject

class FilterDialogFragment : Fragment() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    private lateinit var behavior: BottomSheetBehaviorGoogle<*>

    private lateinit var binding: DialogFilterBinding

    private lateinit var vm: MainFragmentViewModel

    private lateinit var filterAdapter: FilterCategoryAdapter
    private lateinit var locationAdapter: FilterLocationAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DialogFilterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        app().applicationComponent.inject(this)
        super.onActivityCreated(savedInstanceState)

        filterAdapter = FilterCategoryAdapter(
                { categoryId: Int?, b: Boolean ->
                    if (categoryId != null)
                        vm.addRemoveCategoryId(categoryId, b)
                },
                {
                    vm.mainObject.value?.categoryIds?.contains(it) == true
                }
        )

        locationAdapter = FilterLocationAdapter(
                { locationId, shouldAdd ->
                    if (locationId != null)
                        vm.addRemoveLocationId(locationId, shouldAdd)
                },
                {
                    vm.mainObject.value?.locationIds?.contains(it) == true
                }
        )

        binding.categoryFilterRecyclerView.adapter = this.filterAdapter
        binding.locationFilterRecyclerView.adapter = this.locationAdapter

        vm = ViewModelProviders.of(parentFragment!!, viewModelProvider)[MainFragmentViewModel::class.java]

        vm.categories.observe(this, Observer {
            filterAdapter.submitList(it)
        })

        vm.locations.observe(this, Observer {
            locationAdapter.submitList(it)
        })

        initOrderSettings()

        binding.filterChipGroupOrderDirection.setOnCheckedChangeListener { _, i ->
            when (i) {
                R.id.filter_chip_group_order_direction_ascend -> {
                    sharedPreferencesManager.mainOrderDirection = "ASC"
                    vm.setOrderByDirection("ASC")
                }
                R.id.filter_chip_group_order_direction_descend -> {
                    sharedPreferencesManager.mainOrderDirection = "DESC"
                    vm.setOrderByDirection("DESC")
                }
            }
        }

        binding.filterChipGroupOrder.setOnCheckedChangeListener { _, i ->
            when (i) {
                R.id.filter_chip_group_order_name -> {
                    sharedPreferencesManager.mainOrderField = Event.NAME
                    vm.setOrderByField("name")
                }
                R.id.filter_chip_group_order_date -> {
                    sharedPreferencesManager.mainOrderField = Event.DATE
                    vm.setOrderByField("date")
                }
                R.id.filter_chip_group_order_location -> {
                    sharedPreferencesManager.mainOrderField = "location_name"
                    vm.setOrderByField("location_name")
                }
            }
        }

        behavior = BottomSheetBehaviorGoogle.from(binding.rootConstraint)
    }

    private fun initOrderSettings() {
        when (sharedPreferencesManager.mainOrderDirection) {
            "ASC" -> binding.filterChipGroupOrderDirectionAscend.isChecked = true
            "DESC" -> binding.filterChipGroupOrderDirectionDescend.isChecked = true
        }

        when (sharedPreferencesManager.mainOrderField) {
            "name" -> binding.filterChipGroupOrderName.isChecked = true
            "date" -> binding.filterChipGroupOrderDate.isChecked = true
            "location_name" -> binding.filterChipGroupOrderLocation.isChecked = true
        }
    }
}