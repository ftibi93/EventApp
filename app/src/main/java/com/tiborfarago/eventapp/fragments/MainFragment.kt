package com.tiborfarago.eventapp.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.TextWatcher
import android.view.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.MainAdapter
import com.tiborfarago.eventapp.network.ChangeUploader
import com.tiborfarago.resource_module.utils.Result
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.simplestack.EventDetailsKey
import com.tiborfarago.eventapp.util.*
import com.tiborfarago.eventapp.util.customviews.BottomSheetBehaviorGoogle
import com.tiborfarago.eventapp.viewmodels.MainFragmentViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject


class MainFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var synchronizer: Synchronizer
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager
    @Inject
    lateinit var changeUploader: ChangeUploader

    private lateinit var vm: MainFragmentViewModel

    private var favoriteMenuItem: MenuItem? = null

    private lateinit var textChangeListener: TextWatcher

    private lateinit var bottomSheetBehavior: BottomSheetBehaviorGoogle<*>

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        (context?.applicationContext as App).applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_main, container, false)

    @SuppressLint("EnqueueWork")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        bottomSheetBehavior = BottomSheetBehaviorGoogle.from(view.findViewById(R.id.filter_sheet) as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        mainActivity().main_fab.apply {
            show()
            setOnClickListener {
                if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED || bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                else
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_filter_list_white_24dp))
        }

        vm = ViewModelProviders.of(this, viewModelFactory)[MainFragmentViewModel::class.java]

        val adapter = MainAdapter {
            mainActivity().navigateTo(EventDetailsKey(this))
        }
        vm.eventList.observe(this, Observer(adapter::submitList))
        recycler_view_main.adapter = adapter

        // without this, the RV stops working after a couple refreshes
        (recycler_view_main.layoutManager as LinearLayoutManager).isSmoothScrollbarEnabled = false

        main_swipe_refresh_layout.setOnRefreshListener {
            launch {
                val result = listOf(
                        async { changeUploader.commitCategoryChanges() },
                        async { changeUploader.commitLocationChanges() },
                        async { changeUploader.commitEventChanges() },
                        async { changeUploader.commitUserChanges() },
                        async { changeUploader.uploadImages() }
                ).awaitAll().flatten()

                result.forEach { Timber.e(it.networkError) }

                withContext(Dispatchers.Main) {
                    result.firstOrNull()?.let { showSnackbar("${it.networkError.errorCode} - ${it.networkError.errorMessage}") }
                }

                val syncResult = listOf(
                        async { synchronizer.getCategories() },
                        async { synchronizer.getNewEventsAndLocations() },
                        async { synchronizer.getEventsBaseUpdate() },
                        async { synchronizer.getEventsDetailsUpdate() },
                        async { synchronizer.getLocationBaseUpdate() },
                        async { synchronizer.getLocationDetailsUpdate() }
                ).awaitAll()

                syncResult.firstOrNull { it is Result.Error }?.let {
                    showSnackbar(it.toString())
                }

                withContext(Dispatchers.Main) {
                    main_swipe_refresh_layout.isRefreshing = false
                }
            }
        }

        vm.mainObject.observe(this, Observer {
            sharedPreferencesManager.isFavoriteEvents = it.isFavorites

            favoriteMenuItem?.setIcon(if (it.isFavorites) R.drawable.ic_star_black_24dp else R.drawable.ic_star_border_black_24dp)
        })

        textChangeListener = shortTextChangeListener(onTextChanged = { vm.setSearchText(this) })

        vm.setFavorites(sharedPreferencesManager.isFavoriteEvents)
        vm.setOrderByDirection(sharedPreferencesManager.mainOrderDirection)
        vm.setOrderByField(sharedPreferencesManager.mainOrderField)

        mainActivity().search_bar_layout.editText.addTextChangedListener(textChangeListener)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mainActivity().search_bar_layout.hide()
        mainActivity().search_bar_layout.editText.removeTextChangedListener(textChangeListener)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)

        favoriteMenuItem = menu?.findItem(R.id.main_favorites)

        favoriteMenuItem?.setOnMenuItemClickListener {
            vm.negateIsFavorites()
            true
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.main_search -> {
                mainActivity().search_bar_layout.showHide()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
