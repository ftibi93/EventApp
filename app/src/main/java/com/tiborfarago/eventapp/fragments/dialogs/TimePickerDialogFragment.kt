package com.tiborfarago.eventapp.fragments.dialogs

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import com.tiborfarago.eventapp.fragments.AddEventFragment
import java.util.*

class TimePickerDialogFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private var scheduleId: Int = 0
    private var isStart = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        var hour = c.get(Calendar.HOUR_OF_DAY)
        var minute = c.get(Calendar.MINUTE)

        arguments?.let {
            if (it.containsKey(TIME_EXTRA) && it.getString(TIME_EXTRA, "").contains(":")) {
                hour = it.getString(TIME_EXTRA, "").split(":")[0].toInt()
                minute = it.getString(TIME_EXTRA, "").split(":")[1].toInt()
            }

            scheduleId = it.getInt(ID_EXTRA, 0)
            isStart = it.getBoolean(IS_START_EXTRA, false)
        }

        return TimePickerDialog(activity, this, hour, minute, true)
    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        if (scheduleId == 0) {
            targetFragment?.onActivityResult(AddEventFragment.REQUEST_CODE_TIME, 1,
                    Intent()
                            .putExtra(AddEventFragment.REQUEST_CODE_TIME.toString(), "${p1 + 1}:$p2"))
        } else {
            targetFragment?.onActivityResult(AddEventFragment.REQUEST_CODE_TIME, 1,
                    Intent()
                            .putExtra(AddEventFragment.REQUEST_CODE_TIME.toString(), "${p1 + 1}:$p2")
                            .putExtra(ID_EXTRA, scheduleId)
                            .putExtra(IS_START_EXTRA, isStart))
        }
    }

    companion object {
        const val TIME_EXTRA = "time_extra"

        const val ID_EXTRA = "id_extra"
        const val IS_START_EXTRA = "is_start_extra"
    }
}