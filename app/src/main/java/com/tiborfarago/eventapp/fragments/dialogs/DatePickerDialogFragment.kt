package com.tiborfarago.eventapp.fragments.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.tiborfarago.eventapp.fragments.AddEventFragment
import java.util.*

class DatePickerDialogFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)

        arguments?.let {
            if (it.containsKey(DATE_EXTRA) && it.getString(DATE_EXTRA, "").contains(".")) {
                year = it.getString(DATE_EXTRA, "").split(" ")[0].dropLast(1).toInt()
                month = it.getString(DATE_EXTRA, "").split(" ")[1].dropLast(1).toInt() -1
                day = it.getString(DATE_EXTRA, "").split(" ")[2].dropLast(1).toInt()
            }
        }

        return DatePickerDialog(requireActivity(), this, year, month, day)
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        targetFragment?.onActivityResult(AddEventFragment.REQUEST_CODE_DATE, 1,
                Intent()
                        .putExtra(AddEventFragment.REQUEST_CODE_DATE.toString(), "$year-${month + 1}-$day")
        )
    }

    companion object {
        const val DATE_EXTRA = "date_extra"
    }
}