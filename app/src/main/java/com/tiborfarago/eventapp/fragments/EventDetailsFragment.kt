package com.tiborfarago.eventapp.fragments


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.chip.Chip
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.EventDetailsScheduleAdapter
import com.tiborfarago.eventapp.database.daos.*
import com.tiborfarago.eventapp.databinding.FragmentEventDetailsBinding
import com.tiborfarago.eventapp.fragments.dialogs.FlyerDialogFragment
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.simplestack.EventDetailsKey
import com.tiborfarago.eventapp.util.*
import com.tiborfarago.eventapp.viewmodels.EventDetailsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_event_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus
import org.osmdroid.views.overlay.OverlayItem
import javax.inject.Inject

class EventDetailsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var eventDao: EventDao
    @Inject
    lateinit var locationDao: LocationDao
    @Inject
    lateinit var userToEventDao: UserToEventDao
    @Inject
    lateinit var categoryDao: CategoryDao
    @Inject
    lateinit var synchronizer: Synchronizer
    @Inject
    lateinit var scheduleStringConverter: ScheduleStringConverter
    @Inject
    lateinit var scheduleDao: ScheduleDao

    private lateinit var binding: FragmentEventDetailsBinding

    private lateinit var vm: EventDetailsViewModel

    private lateinit var adapter: EventDetailsScheduleAdapter

    private var eventId: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentEventDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (requireActivity().application as App).applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        adapter = EventDetailsScheduleAdapter()
        eventId = getKey<EventDetailsKey>().eventId

        vm = ViewModelProviders.of(this, viewModelFactory)[EventDetailsViewModel::class.java].apply {
            setEventId(getKey<EventDetailsKey>().eventId)
        }

        binding.setLifecycleOwner(this)
        binding.vm = vm

        binding.swipeLayout.setOnRefreshListener {
            launch(Dispatchers.Default) {
                vm.downloadEventAndLocation(eventId)
            }
        }

        binding.imageButtonJumpToDirections.setOnClickListener {
            binding.nestedScrollView.smoothScrollTo(0, binding.cardviewMap.bottom)
        }

        binding.buttonClose.setOnClickListener {
            mainActivity().goBack()
        }

        binding.recyclerViewSchedule.adapter = adapter

        vm.schedule.observe(this, Observer {
            adapter.submitList(it)
        })

        vm.isFavorite.observe(this, Observer {
            mainActivity().main_fab.setImageResource(if (it) R.drawable.ic_star_black_24dp else R.drawable.ic_star_border_black_24dp)
        })

        vm.messageText.observe(this, Observer { wrapper ->
            wrapper.getContentIfNotHandled()?.let {
                showSnackbar(it)
            }
        })

        vm.isRefreshing.observe(this, Observer { wrapper ->
            wrapper.getContentIfNotHandled()?.let {
                binding.swipeLayout.isRefreshing = it
            }
        })

        vm.location.observe(this, Observer {
            if (it.latitude != null && it.longitude != null)
                setupMap(it.latitude?.toDouble(), it.longitude?.toDouble())
        })

        Glide.with(this)
                .load("${Constants.BASE_URL}/api/${Constants.FILE_URL}/downloadCover?id=$eventId")
                .apply(RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(binding.flyerImageView)

        binding.flyerImageView.setOnClickListener {
            val dialog = FlyerDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(FlyerDialogFragment.ID_EXTRA, eventId)
                }
            }

            dialog.show(fragmentManager, "flyer_dialog")
        }

        launch(Dispatchers.IO) {
            val event = eventDao.getEventById(eventId) ?: return@launch
            val location = locationDao.location(event.locationId) ?: return@launch

            scheduleDao.deleteAll()
            scheduleDao.insertAll(scheduleStringConverter.stringToSchedule(eventId))

            if (event.schedule.isNullOrEmpty() || event.description.isNullOrEmpty() || location.address.isNullOrEmpty()) {
                vm.downloadEventAndLocation(eventId)
            }

            val users = userToEventDao.getUserNamesForEvent(eventId)

            withContext(Dispatchers.Main) {
                if (users.isEmpty()) binding.chipGroupUsers.addView(Chip(requireContext()).apply { text = "Publikus" })
                else {
                    users.forEach {
                        binding.chipGroupUsers.addView(Chip(requireContext()).apply { text = it })
                    }
                }
            }

            vm.downloadWeather(location)
        }

        mainActivity().main_fab.apply {
            show()
            setOnClickListener {
                vm.addRemoveFavorite()
            }
        }
    }

    private fun setupMap(latitude: Double?, longitude: Double?) {
        if (latitude == null || longitude == null) return

        val loc = GeoPoint(latitude, longitude)

        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context))
        map_view.apply {
            setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)
            controller.setZoom(18.0)
            controller.setCenter(loc)
        }

        val items = arrayListOf(OverlayItem("", "", loc))
        map_view.overlays.add(ItemizedOverlayWithFocus<OverlayItem>(items,
                object : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
                    override fun onItemSingleTapUp(index: Int, item: OverlayItem): Boolean = false
                    override fun onItemLongPress(index: Int, item: OverlayItem): Boolean = false
                }, context))

        above_map_view.setOnClickListener {
            val uri = Uri.parse("http://maps.google.com/maps?q=loc:$latitude,$longitude()")
            val intent = Intent(Intent.ACTION_VIEW, uri)

            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()

        map_view.onPause()
    }
}
