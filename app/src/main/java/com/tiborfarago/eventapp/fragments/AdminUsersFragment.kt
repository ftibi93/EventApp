package com.tiborfarago.eventapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.SimpleItemManagerAdapter
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.UserDao
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.app
import com.tiborfarago.eventapp.viewmodels.UsersViewModel
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.User
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class AdminUsersFragment : BaseFragment() {

    @Inject
    lateinit var viewmodelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var userDao: UserDao
    @Inject
    lateinit var changeDao: ChangeDao

    private lateinit var viewmodel: UsersViewModel
    private lateinit var adapter: SimpleItemManagerAdapter<User>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        app().applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        viewmodel = ViewModelProviders.of(this, viewmodelProvider)[UsersViewModel::class.java]

        adapter = SimpleItemManagerAdapter(
                modifyClick = {},
                deleteClick = {
                    val builder = AlertDialog.Builder(requireContext())
                            .setMessage("Biztos, hogy törli a felhasználót?")
                            .setPositiveButton("Igen") { _, _ ->
                                launch(Dispatchers.IO) {
                                    if (it != null) deleteUser(it)
                                }
                            }
                            .setNegativeButton("Nem") { _, _ -> }

                    builder.create().show()
                }
        )

        categories_recycler_view.adapter = adapter

        viewmodel.users.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    private fun deleteUser(user: User) {
        val changeRow = changeDao.getChangeByTableAndRow(User.TABLE_NAME, user.id)

        if (changeRow == null || changeRow.type != Change.TYPE.ADD)
            changeDao.insert(Change(User.TABLE_NAME, user.id, Change.TYPE.DELETE))

        userDao.delete(user)
    }
}