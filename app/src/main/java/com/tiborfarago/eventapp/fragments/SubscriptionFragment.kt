package com.tiborfarago.eventapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.adapters.SubscriptionAdapter
import com.tiborfarago.eventapp.database.daos.UserDao
import com.tiborfarago.eventapp.fragments.dialogs.AddSubscriptionDialog
import com.tiborfarago.eventapp.simplestack.BaseFragment
import com.tiborfarago.eventapp.util.App
import com.tiborfarago.eventapp.util.mainActivity
import com.tiborfarago.eventapp.viewmodels.SubscriptionViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_subscription.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SubscriptionFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var userDao: UserDao

    private lateinit var subscriptionAdapter: SubscriptionAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_subscription, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (context?.applicationContext as App).applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        val vm = ViewModelProviders.of(this, viewModelFactory)[SubscriptionViewModel::class.java]

        mainActivity().main_fab.apply {
            show()
            setOnClickListener {
                AddSubscriptionDialog().show(mainActivity().supportFragmentManager, "subscription_dialog")
            }
            setImageResource(R.drawable.ic_add_white_24dp)
        }

        subscriptionAdapter = SubscriptionAdapter({
            launch(Dispatchers.IO) {
                userDao.deleteUser(it?.id)
            }
        }, {
            launch(Dispatchers.IO) {
                userDao.negateIsShown(it?.id)
            }
        })
        vm.userList.observe(this, Observer { subscriptionAdapter.submitList(it) })
        fragment_subscription_recycler_view.adapter = subscriptionAdapter

    }
}
