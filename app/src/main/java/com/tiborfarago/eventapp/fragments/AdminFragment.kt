package com.tiborfarago.eventapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputEditText

import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.database.daos.CategoryDao
import com.tiborfarago.eventapp.database.daos.ChangeDao
import com.tiborfarago.eventapp.database.daos.UserDao
import com.tiborfarago.eventapp.databinding.FragmentAdminBinding
import com.tiborfarago.eventapp.network.ChangeUploader
import com.tiborfarago.resource_module.utils.Result
import com.tiborfarago.eventapp.network.services.AdminService
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.network.services.UserAdminService
import com.tiborfarago.eventapp.simplestack.*
import com.tiborfarago.eventapp.util.*
import com.tiborfarago.eventapp.viewmodels.AdminViewModel
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.LoginRequest
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Change
import com.tiborfarago.resource_module.room.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_add_user.view.*
import kotlinx.coroutines.*
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import timber.log.Timber
import javax.inject.Inject

class AdminFragment : BaseFragment() {

    @Inject
    lateinit var viewModelProvider: ViewModelProvider.Factory
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager
    @Inject
    lateinit var userAdminService: UserAdminService
    @Inject
    lateinit var synchronizer: Synchronizer
    @Inject
    lateinit var categoryDao: CategoryDao
    @Inject
    lateinit var changeDao: ChangeDao
    @Inject
    lateinit var changeUploader: ChangeUploader
    @Inject
    lateinit var userDao: UserDao

    private lateinit var binding: FragmentAdminBinding
    private lateinit var vm: AdminViewModel

    private var userName = ""
    private var password = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAdminBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (context?.applicationContext as App).applicationComponent.inject(this)
        super.onViewCreated(view, savedInstanceState)

        mainActivity().main_fab.hide()

        binding.setLifecycleOwner(this)

        vm = ViewModelProviders.of(this, viewModelProvider)[AdminViewModel::class.java]
        binding.vm = vm

        launch(Dispatchers.Default) {
            delay(300)
            if (vm.isLoginLayoutVisible.value == false)
                synchronizer.getAllForAdmin()
        }

        binding.includedLogin.adminLoginButton.setOnClickListener {
            loginAdmin()
        }

        binding.categoriesAdd.setOnClickListener {
            addCategory()
        }

        binding.categoriesModifyDelete.setOnClickListener {
            mainActivity().navigateTo(CategoriesKey())
        }

        binding.locationsAdd.setOnClickListener {
            mainActivity().navigateTo(AddLocationKey())
        }

        binding.locationsModifyDelete.setOnClickListener {
            mainActivity().navigateTo(LocationsKey())
        }

        binding.uploadChanges.setOnClickListener {
            launch(Dispatchers.Default) {
                listOf(
                        async { changeUploader.commitCategoryChanges() },
                        async { changeUploader.commitLocationChanges() },
                        async { changeUploader.commitEventChanges() },
                        async { changeUploader.commitUserChanges() },
                        async { changeUploader.uploadImages() }
                ).awaitAll().flatten()
                        .forEach { error ->
                            Timber.e(error.networkError)
                            showSnackbar(error.networkError.toString())
                        }
            }
        }

        binding.eventsAdd.setOnClickListener {
            mainActivity().navigateTo(AddEventKey())
        }

        binding.eventsModifyDelete.setOnClickListener {
            mainActivity().navigateTo(EventsKey())
        }

        binding.usersAdd.setOnClickListener {
            addUser()
        }

        binding.usersModifyDelete.setOnClickListener {
            mainActivity().navigateTo(UsersKey())
        }

    }

    private fun addCategory() {
        val view = layoutInflater.inflate(R.layout.dialog_add_category, null)
        val editText = view.findViewById<TextInputEditText>(R.id.dialog_category_edit_text)

        val dialog = AlertDialog.Builder(requireContext())
                .setTitle("Kategória hozzáadása")
                .setView(view)
                .setPositiveButton("Mentés") { _, _ -> }
                .create()

        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            launch(Dispatchers.IO) {
                if (categoryDao.getCategoryByName(editText.text.toString()) != null) {
                    withContext(Dispatchers.Main) { editText.error = "Ez a kategória már létezik" }
                } else {
                    val id = categoryDao.insert(Category(name = editText.text.toString(), modDate = DateTime.now()))
                    changeDao.insert(Change(Category.TABLE_NAME, id.toInt(), Change.TYPE.ADD))
                    withContext(Dispatchers.Main) {
                        dialog.dismiss()
                        showSnackbar("Kategória hozzáadva")
                    }
                }
            }
        }
    }

    private fun addUser() {
        val view = layoutInflater.inflate(R.layout.dialog_add_user, null)

        val dialog = AlertDialog.Builder(requireContext())
                .setTitle("Felhasználó regisztrálása")
                .setView(view)
                .setNegativeButton("Mégse") { _, _ -> }
                .setPositiveButton("Mentés") { _, _ -> }
                .create()

        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            launch(Dispatchers.IO) {
                if (userDao.getUserCountByName(view.add_user_user_name.text.toString()) != 0) {
                    withContext(Dispatchers.Main) { view.add_user_user_name.error = "Létező felhasználónév" }
                    return@launch
                }

                if (userDao.getPasswords().any { password ->
                            BCrypt.checkpw(view.add_user_password.text.toString(), password)
                        }) {
                    withContext(Dispatchers.Main) { view.add_user_password.error = "Létező jelszó" }
                    return@launch
                }

                val id = userDao.insert(User(
                        name = view.add_user_user_name.text.toString(),
                        password = BCrypt.hashpw(view.add_user_password.text.toString(), BCrypt.gensalt(10)),
                        modDate = DateTime.now()
                ))
                changeDao.insert(Change(User.TABLE_NAME, id.toInt(), Change.TYPE.ADD))

                withContext(Dispatchers.Main) {
                    dialog.dismiss()
                    showSnackbar("Felhasználó regisztrálva")
                }
            }
        }
    }

    private fun loginAdmin() {
        userName = binding.includedLogin.adminUser.text.toString()
        password = binding.includedLogin.adminPassword.text.toString()

        launch(Dispatchers.Default) {
            val response = userAdminService.loginAdmin(LoginRequest(userName, password)).await()
            when (response) {
                is Result.Error -> {
                    if (response.networkError.errorCode == 500) showSnackbar("Szerveroldali hiba")
                    else if (response.networkError.errorCode == 401) showSnackbar("Hibás bejelentkezési adatok")

                    Timber.d(response.networkError.toString())
                }
                is Result.Success -> {
                    sharedPreferencesManager.adminUser = userName
                    sharedPreferencesManager.adminPass = password
                    sharedPreferencesManager.token = response.data.token

                    vm.isLoginLayoutVisible.postValue(false)
                    showSnackbar("Admin bejelentkezve")

                    synchronizer.getAllForAdmin().firstOrNull { retrofitResponse -> retrofitResponse is Result.Error }?.let {
                        val error = it as Result.Error
                        showSnackbar("${error.networkError.errorCode} - ${error.networkError.errorMessage}")
                    }
                }
            }
        }
    }
}
