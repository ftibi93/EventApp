package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.databinding.ItemUserWoCardViewBinding
import com.tiborfarago.resource_module.room.User

class AddEventUserAdapter(
        private val checkedUsers: HashSet<Int>,
        private val onClickListener: (User?, Boolean) -> Unit) : PagedListAdapter<User, AddEventUserAdapter.ViewHolder>(diff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(ItemUserWoCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemUserWoCardViewBinding) : RecyclerView.ViewHolder(binding.root) {
        private var model: User? = null

        init {
            binding.itemUserEnabled.setOnCheckedChangeListener { _, isChecked ->
                onClickListener.invoke(model, isChecked)
            }
        }

        fun bind(model: User?) {
            this.model = model
            binding.itemUserName.text = model?.name

            binding.itemUserEnabled.isChecked = checkedUsers.contains(model?.id)
        }
    }

    companion object {
        val diff = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean = oldItem == newItem
        }
    }
}