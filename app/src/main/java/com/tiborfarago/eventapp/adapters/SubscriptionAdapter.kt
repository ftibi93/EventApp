package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.R
import com.tiborfarago.eventapp.database.views.SubscriptionFragmentModel
import com.tiborfarago.eventapp.databinding.ItemUserBinding

class SubscriptionAdapter(
        private val deleteClickListener: ((SubscriptionFragmentModel?) -> Unit),
        private val showClickListener: ((SubscriptionFragmentModel?) -> Unit)
) : PagedListAdapter<SubscriptionFragmentModel, SubscriptionAdapter.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_user, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {
        private var user: SubscriptionFragmentModel? = null
        init {
            binding.itemUserBaseLayout.setOnLongClickListener {
                binding.itemUserDeleteLayout.visibility = View.VISIBLE

                true
            }

            binding.itemUserDeleteLayout.setOnClickListener {
                it.visibility = View.GONE
            }

            binding.itemUserDeleteButton.setOnClickListener {
                deleteClickListener(user)
            }

            binding.itemUserEnabled.setOnClickListener {
                showClickListener(user)
            }
        }

        fun bindItem(user: SubscriptionFragmentModel?) {
            this.user = user
            binding.user = user
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<SubscriptionFragmentModel>() {
            override fun areItemsTheSame(oldItem: SubscriptionFragmentModel, newItem: SubscriptionFragmentModel): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: SubscriptionFragmentModel, newItem: SubscriptionFragmentModel): Boolean = oldItem == newItem

        }
    }
}