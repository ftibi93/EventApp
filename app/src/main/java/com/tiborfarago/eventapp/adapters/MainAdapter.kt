package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.database.views.MainFragmentModel
import com.tiborfarago.eventapp.databinding.ItemEventBinding

class MainAdapter constructor(val onClickListener: Int.() -> Unit) : PagedListAdapter<MainFragmentModel, MainAdapter.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemEventBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val itemEventBinding: ItemEventBinding) : RecyclerView.ViewHolder(itemEventBinding.root), View.OnClickListener {
        private var eventId: Int = -1

        init {
            itemEventBinding.root.setOnClickListener(this)
        }

        fun bind(event: MainFragmentModel?) {
            eventId = event?.eventId ?: -1

            itemEventBinding.event = event
            itemEventBinding.executePendingBindings()
        }

        override fun onClick(p0: View?) {
            eventId.onClickListener()
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MainFragmentModel>() {
            override fun areItemsTheSame(oldItem: MainFragmentModel, newItem: MainFragmentModel): Boolean =
                oldItem.eventName == newItem.eventName

            override fun areContentsTheSame(oldItem: MainFragmentModel, newItem: MainFragmentModel): Boolean =
                oldItem == newItem
        }
    }
}