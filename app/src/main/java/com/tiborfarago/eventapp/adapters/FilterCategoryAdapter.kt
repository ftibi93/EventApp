package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.databinding.ItemCategoryFilterBinding
import com.tiborfarago.resource_module.room.Category

class FilterCategoryAdapter(
        private val onClickListener: (categoryId: Int?, shouldAdd: Boolean) -> Unit,
        private val isAlreadyChecked: (categoryId: Int?) -> Boolean
) : PagedListAdapter<Category, FilterCategoryAdapter.ViewHolder>(diff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemCategoryFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ViewHolder(private val binding: ItemCategoryFilterBinding) : RecyclerView.ViewHolder(binding.root) {
        private var category: Category? = null

        fun bind(category: Category?) {
            this.category = category
            binding.name = category?.name

            binding.filterChip.isCloseIconVisible = isAlreadyChecked(category?.id)

            binding.filterChip.setOnClickListener {
                binding.filterChip.isCloseIconVisible = binding.filterChip.isCloseIconVisible.not()
                onClickListener(category?.id, binding.filterChip.isCloseIconVisible)
            }

            binding.filterChip.setOnCloseIconClickListener {
                binding.filterChip.isCloseIconVisible = binding.filterChip.isCloseIconVisible.not()
                onClickListener(category?.id, binding.filterChip.isCloseIconVisible)
            }

            binding.executePendingBindings()
        }
    }

    companion object {
        private val diff = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean = oldItem == newItem
        }
    }
}