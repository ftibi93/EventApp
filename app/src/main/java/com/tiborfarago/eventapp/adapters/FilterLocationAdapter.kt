package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.database.views.LocationNameWithId
import com.tiborfarago.eventapp.databinding.ItemCategoryFilterBinding

class FilterLocationAdapter(
        private val onClickListener: (locationId: Int?, shouldAdd: Boolean) -> Unit,
        private val isAlreadyChecked: (locationId: Int?) -> Boolean
) : PagedListAdapter<LocationNameWithId, FilterLocationAdapter.ViewHolder>(diff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(ItemCategoryFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ViewHolder(private val binding: ItemCategoryFilterBinding) : RecyclerView.ViewHolder(binding.root) {
        private var locationNameWithId: LocationNameWithId? = null

        fun bind(locationNameWithId: LocationNameWithId?) {
            this.locationNameWithId = locationNameWithId
            binding.name = locationNameWithId?.name

            binding.filterChip.isCloseIconVisible = isAlreadyChecked(locationNameWithId?.id)

            binding.filterChip.setOnClickListener {
                binding.filterChip.isCloseIconVisible = binding.filterChip.isCloseIconVisible.not()
                onClickListener(locationNameWithId?.id, binding.filterChip.isCloseIconVisible)
            }

            binding.filterChip.setOnCloseIconClickListener {
                binding.filterChip.isCloseIconVisible = binding.filterChip.isCloseIconVisible.not()
                onClickListener(locationNameWithId?.id, binding.filterChip.isCloseIconVisible)
            }

            binding.executePendingBindings()
        }
    }

    companion object {
        private val diff = object : DiffUtil.ItemCallback<LocationNameWithId>() {
            override fun areItemsTheSame(oldItem: LocationNameWithId, newItem: LocationNameWithId): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: LocationNameWithId, newItem: LocationNameWithId): Boolean = oldItem == newItem
        }
    }
}