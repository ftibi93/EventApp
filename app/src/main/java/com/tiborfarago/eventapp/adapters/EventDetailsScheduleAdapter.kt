package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.database.ScheduleRow
import com.tiborfarago.eventapp.databinding.ItemScheduleBinding

class EventDetailsScheduleAdapter : PagedListAdapter<ScheduleRow, EventDetailsScheduleAdapter.ViewHolder>(diff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(ItemScheduleBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemScheduleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(scheduleRow: ScheduleRow?) {
            binding.row = scheduleRow
        }
    }

    companion object {
        private val diff = object : DiffUtil.ItemCallback<ScheduleRow>() {
            override fun areItemsTheSame(oldItem: ScheduleRow, newItem: ScheduleRow): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: ScheduleRow, newItem: ScheduleRow): Boolean = oldItem == newItem
        }
    }
}