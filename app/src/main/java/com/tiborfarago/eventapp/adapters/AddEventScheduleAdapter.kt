package com.tiborfarago.eventapp.adapters

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.database.ScheduleRow
import com.tiborfarago.eventapp.databinding.ItemScheduleEditBinding
import com.tiborfarago.eventapp.util.shortTextChangeListener

class AddEventScheduleAdapter(
        private val onDelete: (ScheduleRow?.() -> Unit),
        private val onTimeChange: ((isStart: Boolean, ScheduleRow?) -> Unit),
        private val onTextChange: ((text: String, ScheduleRow?) -> Unit)
) : PagedListAdapter<ScheduleRow, AddEventScheduleAdapter.ViewHolder>(diff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(ItemScheduleEditBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemScheduleEditBinding) : RecyclerView.ViewHolder(binding.root) {
        private var scheduleRow: ScheduleRow? = null
        private var handler = Handler()

        init {
            binding.scheduleErase.setOnClickListener { onDelete.invoke(scheduleRow) }
            binding.scheduleStart.setOnClickListener { onTimeChange.invoke(true, scheduleRow) }
            binding.scheduleEnd.setOnClickListener { onTimeChange.invoke(false, scheduleRow) }

            binding.scheduleTitleEditText.addTextChangedListener(shortTextChangeListener(
                    onTextChanged = { handler.removeCallbacksAndMessages(null) },
                    afterTextChanged = { handler.postDelayed({ onTextChange.invoke(this, scheduleRow) }, 1000) }
            ))
        }

        fun bindItem(scheduleRow: ScheduleRow?) {
            this.scheduleRow = scheduleRow
            binding.row = scheduleRow
        }
    }

    companion object {
        private val diff = object : DiffUtil.ItemCallback<ScheduleRow>() {
            override fun areItemsTheSame(oldItem: ScheduleRow, newItem: ScheduleRow): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: ScheduleRow, newItem: ScheduleRow): Boolean = oldItem == newItem
        }
    }
}