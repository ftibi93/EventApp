package com.tiborfarago.eventapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tiborfarago.eventapp.databinding.ItemRowsAdminBinding
import com.tiborfarago.resource_module.room.BaseEntity
import com.tiborfarago.resource_module.room.User

class SimpleItemManagerAdapter<T : BaseEntity>(
        private val modifyClick: (T?) -> Unit,
        private val deleteClick: (T?) -> Unit
) : PagedListAdapter<T, SimpleItemManagerAdapter<T>.BaseViewHolder>(asd<T>()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return BaseViewHolder(ItemRowsAdminBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class BaseViewHolder(private val binding: ItemRowsAdminBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: T?) {
            binding.nameText = obj?.baseName

            binding.delete.setOnClickListener { deleteClick.invoke(obj) }
            binding.modify.setOnClickListener { modifyClick.invoke(obj) }

            if (obj is User)
                binding.modify.visibility = View.GONE
        }
    }

    companion object {
        fun <T : BaseEntity> asd() = object : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem.baseId == newItem.baseId
            override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem.baseName == newItem.baseName
        }
    }
}