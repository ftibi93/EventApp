package com.tiborfarago.eventapp.database.daos

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.room.UserToEvents

@Dao
abstract class UserToEventDao : BaseDao<UserToEvents>() {
    @Query("SELECT * FROM user")
    abstract fun getUserList(): DataSource.Factory<Int, User>

    @Query("SELECT user_id FROM user_to_events GROUP BY user_id")
    abstract fun getUserIdList(): List<Int>

    @Query("SELECT * FROM user_to_events WHERE event_id IN (:ids)")
    abstract fun getUserToEventsForEvents(ids: List<Int>): List<UserToEvents>

    @Query("SELECT user_id FROM user_to_events WHERE event_id = :eventId")
    abstract fun getUsersForEvent(eventId: Int): List<Int>

    @Query("DELETE FROM user_to_events WHERE event_id = :eventId")
    abstract fun deleteForEvents(eventId: Int?)

    @Query("SELECT u.name FROM user_to_events ue LEFT JOIN user u ON u.id = ue.user_id WHERE ue.event_id = :eventId")
    abstract fun getUserNamesForEvent(eventId: Int): List<String>
}