package com.tiborfarago.eventapp.database.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.eventapp.database.views.LocationNameWithId
import com.tiborfarago.resource_module.room.Location

@Dao
abstract class LocationDao : BaseDao<Location>() {
    @Query("SELECT * FROM location WHERE id = :locationId")
    abstract fun locationLiveData(locationId: Int): LiveData<Location>

    @Query("SELECT * FROM location WHERE id = :locationId")
    abstract fun location(locationId: Int?): Location?

    @Query("SELECT * FROM location")
    abstract fun getLocationsPagedList(): DataSource.Factory<Int, Location>

    @Query("SELECT id, name FROM location")
    abstract fun getLocationsPagedListSimple(): DataSource.Factory<Int, LocationNameWithId>

    @Query("SELECT id, name FROM location")
    abstract fun getLocationNamesAndIds(): List<LocationNameWithId>

    @Query("SELECT * FROM location WHERE name = :name")
    abstract fun getLocationByName(name: String): Location?

    @Query("SELECT name FROM location WHERE id = :id")
    abstract fun getLocationNameById(id: Int): String?


    @Query("SELECT id FROM location ORDER BY id DESC LIMIT 1")
    abstract fun getLatestId(): Int?

    @Query("SELECT id FROM location WHERE address IS NULL")
    abstract fun getBaseIds(): List<Int>

    @Query("SELECT * FROM location WHERE address IS NULL")
    abstract fun getBaseLocations(): List<Location>

    @Query("SELECT * FROM location WHERE address IS NOT NULL")
    abstract fun getDetailsLocations(): List<Location>

    @Query("DELETE FROM location WHERE id IN (:locationIds)")
    abstract fun deleteWhereId(locationIds: List<Int>)
}