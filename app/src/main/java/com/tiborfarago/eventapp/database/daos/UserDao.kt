package com.tiborfarago.eventapp.database.daos

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.eventapp.database.views.SubscriptionFragmentModel
import com.tiborfarago.resource_module.room.User

@Dao
abstract class UserDao : BaseDao<User>() {
    @Query("DELETE FROM user WHERE id IN (:ids)")
    abstract fun deleteIds(ids: List<Int>)

    @Query("DELETE FROM user WHERE id = :id")
    abstract fun deleteUser(id: Int?)

    @Query("SELECT u.id, u.name, u.is_shown, (SELECT COUNT(*) FROM user_to_events WHERE user_id = u.id) AS event_count FROM user u")
    abstract fun getUsersPaged(): DataSource.Factory<Int, SubscriptionFragmentModel>

    @Query("UPDATE user SET is_shown = NOT is_shown WHERE id = :id")
    abstract fun negateIsShown(id: Int?)

    @Query("SELECT id FROM user")
    abstract fun getUserIds(): List<Int>

    @Query("SELECT id FROM user ORDER BY id DESC LIMIT 1")
    abstract fun getLatestId(): Int?

    @Query("SELECT COUNT(*) FROM user WHERE name = :name")
    abstract fun getUserCountByName(name: String): Int

    @Query("SELECT password FROM user")
    abstract fun getPasswords(): List<String>

    @Query("SELECT * FROM user WHERE id = :id")
    abstract fun getUserById(id: Int): User?

    @Query("SELECT * FROM user")
    abstract fun getAllUsersPaged(): DataSource.Factory<Int, User>
}