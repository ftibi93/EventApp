package com.tiborfarago.eventapp.database.daos

import androidx.room.*
import com.tiborfarago.resource_module.room.Change

@Dao
abstract class ChangeDao : BaseDao<Change>() {
    @Query("SELECT * FROM change WHERE changed_table = :tableName AND changed_row_id = :rowId")
    abstract fun getChangeByTableAndRow(tableName: String, rowId: Int?): Change?

    @Query("SELECT * FROM change WHERE changed_table = :tableName")
    abstract fun getChangesForTable(tableName: String): List<Change>
}