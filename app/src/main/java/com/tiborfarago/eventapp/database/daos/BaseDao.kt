package com.tiborfarago.eventapp.database.daos

import androidx.room.*

@Dao
abstract class BaseDao <in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(type: T?): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(types: List<T>?)

    @Update
    abstract fun update(type: T)

    @Delete
    abstract fun delete(type: T)
}