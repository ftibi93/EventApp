package com.tiborfarago.eventapp.database.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.tiborfarago.resource_module.room.Category

@Dao
abstract class CategoryDao : BaseDao<Category>() {
    @Query("SELECT * FROM category ORDER BY name")
    abstract fun getCategories(): List<Category>

    @Query("SELECT * FROM category WHERE name = :name")
    abstract fun getCategoryByName(name: String): Category?

    @Query("SELECT * FROM category")
    abstract fun getCategoriesPagedList(): DataSource.Factory<Int, Category>

    @Query("SELECT * FROM category WHERE id = :id")
    abstract fun getCategoryById(id: Int): Category?

    @Query("SELECT name FROM category WHERE id = :id")
    abstract fun getCategoryNameById(id: Int): String?

    @Query("SELECT name FROM category WHERE id = :id")
    abstract fun getCategoryNameByIdLiveData(id: Int): LiveData<String?>

    @Query("DELETE FROM category")
    abstract fun deleteAll()
}