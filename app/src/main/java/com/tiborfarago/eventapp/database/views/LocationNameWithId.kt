package com.tiborfarago.eventapp.database.views

import androidx.room.ColumnInfo
import com.tiborfarago.resource_module.room.Location

data class LocationNameWithId(
        @ColumnInfo(name = Location.ID) var id: Int,
        @ColumnInfo(name = Location.NAME) var name: String
)