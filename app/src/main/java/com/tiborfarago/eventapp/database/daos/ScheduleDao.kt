package com.tiborfarago.eventapp.database.daos

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.eventapp.database.ScheduleRow

@Dao
abstract class ScheduleDao : BaseDao<ScheduleRow>() {
    @Query("SELECT * FROM schedule")
    abstract fun getAllPaged(): DataSource.Factory<Int, ScheduleRow>

    @Query("SELECT * FROM schedule")
    abstract fun getAll(): List<ScheduleRow>

    @Query("DELETE FROM schedule")
    abstract fun deleteAll()

    @Query("SELECT * FROM schedule WHERE id = :id")
    abstract fun getSchedule(id: Int): ScheduleRow?
}