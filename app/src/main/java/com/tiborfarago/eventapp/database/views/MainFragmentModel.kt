package com.tiborfarago.eventapp.database.views

import androidx.room.ColumnInfo
import org.joda.time.DateTime

data class MainFragmentModel(
        @ColumnInfo(name = "id") var eventId: Int = -1,
        @ColumnInfo(name = "name") var eventName: String = "",
        @ColumnInfo(name = "date") var eventDate: DateTime = DateTime(1970, 1, 1, 0, 0),
        @ColumnInfo(name = "location_name") var locationName: String? = ""
)