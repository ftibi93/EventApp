package com.tiborfarago.eventapp.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.eventapp.database.FavoriteRow

@Dao
abstract class FavoriteDao : BaseDao<FavoriteRow>() {
    @Query("SELECT * FROM favorite WHERE table_name = :tableName AND id = :id")
    abstract fun getFavoriteRow(tableName: String, id: Int): FavoriteRow?

    @Query("SELECT * FROM favorite WHERE table_name = :tableName AND id = :id")
    abstract fun getFavoriteRowLiveData(tableName: String, id: Int): LiveData<FavoriteRow?>
}