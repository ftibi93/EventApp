package com.tiborfarago.eventapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime

@Entity(tableName = "schedule")
data class ScheduleRow(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "start") var start: DateTime = DateTime().withTime(8, 0, 0, 0),
        @ColumnInfo(name = "end") var end: DateTime = DateTime().withTime(9, 0, 0, 0),
        @ColumnInfo(name = "title") var title: String = ""
)