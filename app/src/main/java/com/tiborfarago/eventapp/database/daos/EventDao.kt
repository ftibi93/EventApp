package com.tiborfarago.eventapp.database.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import com.tiborfarago.eventapp.database.FavoriteRow
import com.tiborfarago.eventapp.database.views.MainFragmentModel
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.UserToEvents

@Dao
abstract class EventDao : BaseDao<Event>() {

    @Query("SELECT * FROM event")
    abstract fun getAll(): List<Event>

    @Query("DELETE FROM event WHERE id IN (:idList)")
    abstract fun deleteEventsWhereId(idList: List<Int>)

    //@Query("SELECT e.name as name, e.date as date, l.name as location_name FROM event e " +
    //        "LEFT JOIN location l ON e.location_id = l.id WHERE e.id NOT IN (SELECT ue.event_id FROM user_to_events ue WHERE ue.user_id NOT IN (:userIds))")

    @RawQuery(observedEntities = [Event::class, Location::class, UserToEvents::class, FavoriteRow::class])
    abstract fun getMainEventRaw(query: SupportSQLiteQuery): DataSource.Factory<Int, MainFragmentModel>


    fun getMainRawPaged(
            list: List<Int>,
            searchText: String,
            orderByField: String = Event.DATE,
            orderByDirection: String = "ASC",
            categoryIds: HashSet<Int>,
            locationIds: HashSet<Int>): DataSource.Factory<Int, MainFragmentModel> {
        //language=RoomSql
        return getMainEventRaw(SimpleSQLiteQuery("""
            SELECT e.id as id,e.name AS name,
       e.date AS date,
       l.name AS location_name,
       CASE WHEN f.id IS NULL THEN 0 ELSE 1 END asd
FROM   event e
       LEFT JOIN location l ON e.location_id = l.id
       LEFT JOIN favorite f ON e.id = f.id AND f.table_name = "${Event.TABLE_NAME}"
       LEFT JOIN user_to_events ue ON  ue.event_id = e.id
       LEFT JOIN user u ON ue.user_id = u.id

       WHERE (u.is_shown = 1 OR ue.event_id IS NULL)
       AND asd IN (${list.joinToString()})
       AND e.name LIKE "$searchText"
       ${if (categoryIds.isEmpty()) "" else "AND e.category_id IN (${categoryIds.joinToString()})"}
       ${if (locationIds.isEmpty()) "" else "AND e.location_id IN (${locationIds.joinToString()})"}

       GROUP BY e.id
       ORDER BY $orderByField $orderByDirection
        """.trimIndent()))
    }


    @Query("SELECT * FROM event")
    abstract fun getAllEvents(): DataSource.Factory<Int, Event>

    @Query("SELECT * FROM event WHERE id = :eventId")
    abstract fun getEventLiveDataById(eventId: Int): LiveData<Event>

    @Query("SELECT * FROM event WHERE id = :eventId")
    abstract fun getEventById(eventId: Int): Event?

    @Query("SELECT schedule FROM event WHERE id = :id")
    abstract fun getEventSchedule(id: Int): String?

    @Query("SELECT COUNT(*) FROM event WHERE name = :name")
    abstract fun getEventCountByName(name: String): Int

    @Query("SELECT id FROM event ORDER BY id DESC LIMIT 1")
    abstract fun getLatestId(): Int?

    @Query("SELECT id FROM event WHERE description IS NULL")
    abstract fun getBaseIds(): List<Int>

    @Query("SELECT * FROM event WHERE description IS NULL")
    abstract fun getBaseList(): List<Event>

    @Query("SELECT * FROM event WHERE description IS NOT NULL")
    abstract fun getDetailsList(): List<Event>

    @Query("SELECT COUNT(*) FROM event WHERE location_id = :locationId")
    abstract fun getEventCountByLocationId(locationId: Int): Int

    @Query("SELECT COUNT(*) FROM event WHERE category_id = :categoryId")
    abstract fun getEventCountByCategoryId(categoryId: Int): Int
}