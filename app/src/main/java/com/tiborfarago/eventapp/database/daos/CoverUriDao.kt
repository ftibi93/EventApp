package com.tiborfarago.eventapp.database.daos

import androidx.room.Dao
import androidx.room.Query
import com.tiborfarago.eventapp.database.CoverUriRow

@Dao
abstract class CoverUriDao : BaseDao<CoverUriRow>() {
    @Query("SELECT * FROM cover_uri_row")
    abstract fun getAll(): List<CoverUriRow>

    @Query("SELECT uri FROM cover_uri_row WHERE event_id = :id")
    abstract fun getUriForEvent(id: Int): String?
}