package com.tiborfarago.eventapp.database.views

import androidx.room.ColumnInfo
import com.tiborfarago.resource_module.room.User

data class SubscriptionFragmentModel(
        @ColumnInfo(name = User.ID) var id: Int = 0,
        @ColumnInfo(name = User.NAME) var name: String = "",
        @ColumnInfo(name = User.IS_SHOWN) var isShown: Boolean = true,
        @ColumnInfo(name = "event_count") var eventCount: Int = 0
)