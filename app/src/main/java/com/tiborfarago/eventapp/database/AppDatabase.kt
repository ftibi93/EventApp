package com.tiborfarago.eventapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tiborfarago.eventapp.database.daos.*
import com.tiborfarago.resource_module.room.*


@Database(entities = [
    User::class,
    Event::class,
    Location::class,
    UserToEvents::class,
    Category::class,
    Change::class,
    ScheduleRow::class,
    CoverUriRow::class,
    FavoriteRow::class
], version = 1)
@TypeConverters(DateConverter::class, BigDecimalConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDao(): EventDao
    abstract fun userDao(): UserDao
    abstract fun userToEventDao(): UserToEventDao
    abstract fun locationDao(): LocationDao
    abstract fun categoryDao(): CategoryDao
    abstract fun changeDao(): ChangeDao
    abstract fun scheduleDao(): ScheduleDao
    abstract fun coverUriDao(): CoverUriDao
    abstract fun favoriteDao(): FavoriteDao
}