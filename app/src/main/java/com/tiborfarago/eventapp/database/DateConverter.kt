package com.tiborfarago.eventapp.database

import androidx.room.TypeConverter
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.math.BigDecimal
import java.util.*

class DateConverter {
    @TypeConverter
    fun toDateTime(long: Long) = DateTime(long).withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Europe/Budapest")))

    @TypeConverter
    fun toLong(dateTime: DateTime) = dateTime.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Europe/Budapest"))).millis
}

class BigDecimalConverter {
    @TypeConverter
    fun toBigDecimal(double: Double?): BigDecimal? = double?.let { BigDecimal(it) }

    @TypeConverter
    fun toDouble(bigDecimal: BigDecimal?): Double? = bigDecimal?.toDouble()
}