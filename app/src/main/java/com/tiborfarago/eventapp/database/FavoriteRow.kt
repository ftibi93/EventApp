package com.tiborfarago.eventapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "favorite", primaryKeys = ["table_name", "id"])
data class FavoriteRow(
        @ColumnInfo(name = "table_name") var tableName: String = "",
        @ColumnInfo(name = "id") var id: Int = 0
)