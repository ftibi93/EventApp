package com.tiborfarago.eventapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cover_uri_row")
data class CoverUriRow(
        @PrimaryKey @ColumnInfo(name = "event_id") var eventId: Int,
        @ColumnInfo(name = "uri") var uri: String
)