package com.tiborfarago.eventapp.util

import android.content.SharedPreferences
import com.tiborfarago.resource_module.room.Event
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesManager @Inject constructor(private val sharedPreferences: SharedPreferences) {
    var adminUser: String
        get() = sharedPreferences.getString(ADMIN_USER, "") ?: ""
        set(value) = sharedPreferences.edit().putString(ADMIN_USER, value).apply()

    var adminPass: String
        get() = sharedPreferences.getString(ADMIN_PASS, "") ?: ""
        set(value) = sharedPreferences.edit().putString(ADMIN_PASS, value).apply()

    var token: String
        get() = sharedPreferences.getString(TOKEN, "") ?: ""
        set(value) = sharedPreferences.edit().putString(TOKEN, value).apply()

    var isFavoriteEvents: Boolean
        get() = sharedPreferences.getBoolean(FAVORITE_EVENTS, false)
        set(value) = sharedPreferences.edit().putBoolean(FAVORITE_EVENTS, value).apply()

    var mainOrderField: String
        get() = sharedPreferences.getString(ORDER_MAIN_FIELD, Event.DATE) ?: Event.DATE
        set(value) = sharedPreferences.edit().putString(ORDER_MAIN_FIELD, value).apply()

    var mainOrderDirection: String
        get() = sharedPreferences.getString(ORDER_MAIN_DIRECTION, "ASC") ?: "ASC"
        set(value) = sharedPreferences.edit().putString(ORDER_MAIN_DIRECTION, value).apply()

    companion object {
        private const val ADMIN_USER = "admin_user"
        private const val ADMIN_PASS = "admin_pass"
        private const val TOKEN = "token"

        private const val FAVORITE_EVENTS = "favorite_events"

        private const val ORDER_MAIN_FIELD = "order_main_field"
        private const val ORDER_MAIN_DIRECTION = "order_main_direction"
    }
}