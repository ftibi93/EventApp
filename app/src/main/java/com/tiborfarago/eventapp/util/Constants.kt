package com.tiborfarago.eventapp.util

object Constants {
    const val BASE_URL = "http://192.168.1.2:8081"

    const val API_URL = "api"

    const val USERADMIN_URL = "useradmin"
    const val EVENT_URL = "event"
    const val ADMIN_URL = "admin"
    const val CATEGORY_URL = "category"
    const val LOCATION_URL = "location"
    const val USER_URL = "user"
    const val FILE_URL = "files"
}