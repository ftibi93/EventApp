package com.tiborfarago.eventapp.util

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.tiborfarago.eventapp.activities.MainActivity
import com.tiborfarago.resource_module.utils.Result
import com.tiborfarago.resource_module.classes.util.NetworkError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend inline fun <T : Any> Call<T>.await(): Result<T> = suspendCoroutine { continuation ->
    enqueue(object : Callback<T> {
        override fun onFailure(call: Call<T>?, t: Throwable) {
            continuation.resume(Result.Error(NetworkError(errorMessage = t.localizedMessage)))
        }

        override fun onResponse(call: Call<T>?, response: Response<T>) {
            if (!response.isSuccessful)
                continuation.resume(Result.Error(NetworkError(response.code(), response.errorBody()?.string().toString())))
            else
                continuation.resume(Result.Success(response.body() as T))
        }
    })
}

fun Fragment.mainActivity() = requireActivity() as MainActivity

fun Activity.app(): App = application as App
fun Fragment.app(): App = requireActivity().app()

fun Fragment.showSnackbar(message: String) {
    this.activity?.currentFocus?.let { Snackbar.make(it, message, Snackbar.LENGTH_SHORT).show() }
}

fun Activity.showSnackbar(message: String) {
    currentFocus?.let { Snackbar.make(it, message, Snackbar.LENGTH_SHORT).show() }
}

fun shortTextChangeListener(
        afterTextChanged: (String.() -> Unit)? = null,
        beforeTextChanged: (String.() -> Unit)? = null,
        onTextChanged: (String.() -> Unit)? = null
) =
        object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                afterTextChanged?.invoke(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                beforeTextChanged?.invoke(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                onTextChanged?.invoke(s.toString())
            }
        }