package com.tiborfarago.eventapp.util

import java.text.DecimalFormat

class UtilFunctions {
    companion object {
        @JvmStatic
        fun toDoubleDigits(number: Int): String = DecimalFormat("00").format(number)
    }
}