package com.tiborfarago.eventapp.util

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.tiborfarago.eventapp.R
import org.joda.time.DateTime


@BindingAdapter(value = ["monthTextFromDate"])
fun monthFromDate(textView: TextView, dateTime: DateTime) {
    textView.text = dateTime.monthOfYear().asShortText
}

@BindingAdapter(value = ["dayNumberFromDate"])
fun TextView.dayNumFromDate(dateTime: DateTime) {
    this.text = dateTime.dayOfMonth.toString()
}

@BindingAdapter(value = ["dayTextFromDate"])
fun TextView.dayTextFromDate(dateTime: DateTime) {
    this.text = this.resources.getStringArray(R.array.week_days)[dateTime.dayOfWeek - 1]
}