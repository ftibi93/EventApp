package com.tiborfarago.eventapp.util

import com.tiborfarago.eventapp.database.ScheduleRow
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.database.daos.ScheduleDao
import org.joda.time.DateTime
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScheduleStringConverter @Inject constructor(private val scheduleDao: ScheduleDao, private val eventDao: EventDao){

    fun scheduleToString(): String {
        val string = StringBuilder()

        scheduleDao.getAll().forEach {
            string
                    .append("${it.start.hourOfDay}:${it.start.minuteOfHour}")
                    .append(SEPARATOR)
                    .append("${it.end.hourOfDay}:${it.end.minuteOfHour}")
                    .append(SEPARATOR)
                    .append(it.title)
                    .append(ROW_SEPARATOR)
        }
        return string.toString().dropLast(ROW_SEPARATOR.length)
    }

    fun stringToSchedule(id: Int): List<ScheduleRow> {
        val string = eventDao.getEventSchedule(id)

        if (string == null || string.contains(SEPARATOR).not()) return emptyList()

        val list = mutableListOf<ScheduleRow>()

        string.split(ROW_SEPARATOR).forEach {
            val row = it.split(SEPARATOR)

            list.add(ScheduleRow(
                    start = DateTime().withTime(row[0].split(":")[0].toInt(), row[0].split(":")[1].toInt(), 0, 0),
                    end = DateTime().withTime(row[1].split(":")[0].toInt(), row[1].split(":")[1].toInt(), 0, 0),
                    title = row[2]
            ))
        }

        return list
    }

    companion object {
        private const val SEPARATOR = "|*|"
        private const val ROW_SEPARATOR = "\n"
    }
}