package com.tiborfarago.eventapp.util

import android.app.Application
import android.util.Log
import com.facebook.stetho.Stetho
import com.tiborfarago.eventapp.BuildConfig
import com.tiborfarago.eventapp.di.*
import timber.log.Timber

class App : Application() {
    lateinit var applicationComponent: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this)

        applicationComponent = DaggerApplicationComponent
                .builder()
                .appModule(AppModule(this))
                .retrofitModule(RetrofitModule())
                .databaseModule(DatabaseModule(this))
                .build()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(object : Timber.Tree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                    if (priority < Log.ERROR) return


                }
            })
        }
    }
}