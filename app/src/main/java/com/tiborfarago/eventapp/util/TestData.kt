package com.tiborfarago.eventapp.util

import com.tiborfarago.eventapp.database.AppDatabase
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.room.UserToEvents
import org.joda.time.DateTime
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class TestData @Inject constructor(private val appDatabase: AppDatabase) {
    fun insertTestData() {
        val events = ArrayList<Event>()
        val users = listOf(User(1, "elso", "elso", 0, DateTime.now()), User(2, "masodik", "masodik", 0, DateTime.now(), false))

        val locations = listOf(Location(1, "elso hely", "Kecskemét", "idWithModDateList utca 1", 31321231231.toBigDecimal(), 2312.toBigDecimal(), DateTime.now()),
                Location(2, "masodik hely", "Szabadszállás", "eee", 222.toBigDecimal(), 233.toBigDecimal()))

        val rand = Random()

        for (i in 0..50) {
            events.add(Event(
                    id = i,
                    locationId = locations[rand.nextInt(locations.size)].id,
                    name = "name$i",
                    date = DateTime.now().plusDays(10),
                    description = "desc$i",
                    schedule = "schedule$i",
                    modDate = DateTime.now()
            ))
        }

        val userToEvents = listOf(UserToEvents(users[0].id, events[0].id, DateTime.now()), UserToEvents(2, events[1].id, DateTime.now()))

        //appDatabase.locationDao().insertAll(locations)
        appDatabase.userDao().insertAll(users)
        //appDatabase.eventDao().insertAll(events)
        appDatabase.userToEventDao().insertAll(userToEvents)
    }
}