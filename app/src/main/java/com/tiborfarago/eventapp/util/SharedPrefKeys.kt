package com.tiborfarago.eventapp.util

class SharedPrefKeys {
    companion object {
        const val PREF_KEY_USER_ID = "pref_key_user_id"
        const val PREF_KEY_USER_MOD_DATE = "pref_key_mod_date"

        const val LOGIN_DIALOG_USER = "login_dialog_user"
    }
}