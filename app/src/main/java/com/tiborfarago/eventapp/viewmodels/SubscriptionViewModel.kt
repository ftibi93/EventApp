package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.UserDao
import javax.inject.Inject

class SubscriptionViewModel @Inject constructor(userDao: UserDao) : ViewModel() {
    val userList = LivePagedListBuilder(userDao.getUsersPaged(), 20).build()
}