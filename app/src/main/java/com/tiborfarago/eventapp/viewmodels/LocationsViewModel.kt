package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.LocationDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationsViewModel @Inject constructor(locationDao: LocationDao) : ViewModel() {
    val locationsPagedList = LivePagedListBuilder(locationDao.getLocationsPagedList(), 20).build()
}