package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tiborfarago.eventapp.util.SharedPreferencesManager
import javax.inject.Inject

class AdminViewModel @Inject constructor(sharedPreferencesManager: SharedPreferencesManager) : ViewModel() {
    var isLoginLayoutVisible = MutableLiveData<Boolean>()

    init {
        isLoginLayoutVisible.postValue(sharedPreferencesManager.adminPass.isEmpty())
    }
}