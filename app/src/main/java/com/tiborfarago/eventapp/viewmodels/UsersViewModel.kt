package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.UserDao
import javax.inject.Inject

class UsersViewModel @Inject constructor(userDao: UserDao) : ViewModel() {
    val users = LivePagedListBuilder(userDao.getAllUsersPaged(), 20).build()
}