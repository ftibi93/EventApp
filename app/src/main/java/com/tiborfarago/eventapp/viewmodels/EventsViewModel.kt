package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.EventDao
import javax.inject.Inject

class EventsViewModel @Inject constructor(eventDao: EventDao) : ViewModel() {
    val events = LivePagedListBuilder(eventDao.getAllEvents(), 20).build()
}