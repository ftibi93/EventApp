package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.CategoryDao
import javax.inject.Inject

class CategoriesViewModel @Inject constructor(categoryDao: CategoryDao) : ViewModel() {
    val categoriesPaged = LivePagedListBuilder(categoryDao.getCategoriesPagedList(), 20).build()
}