package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tiborfarago.eventapp.database.FavoriteRow
import com.tiborfarago.eventapp.database.ScheduleRow
import com.tiborfarago.eventapp.database.daos.*
import com.tiborfarago.resource_module.utils.Result
import com.tiborfarago.eventapp.network.Synchronizer
import com.tiborfarago.eventapp.network.services.WeatherService
import com.tiborfarago.eventapp.util.EventWrapper
import com.tiborfarago.eventapp.util.await
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.Location
import kotlinx.coroutines.*
import org.joda.time.DateTime
import org.joda.time.Duration
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class EventDetailsViewModel @Inject constructor(
        private val eventDao: EventDao,
        private val locationDao: LocationDao,
        private val favoriteDao: FavoriteDao,
        private val categoryDao: CategoryDao,
        private val synchronizer: Synchronizer,
        private val weatherService: WeatherService,
        scheduleDao: ScheduleDao) : ViewModel(), CoroutineScope {
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    private val eventId = MutableLiveData<Int>()

    val event: LiveData<Event> = Transformations.switchMap(eventId) {
        eventDao.getEventLiveDataById(it)
    }

    val location: LiveData<Location> = Transformations.switchMap(event) {
        locationDao.locationLiveData(it.locationId)
    }

    val categoryName: LiveData<String?> = Transformations.switchMap(event) {
        categoryDao.getCategoryNameByIdLiveData(it.categoryId)
    }

    private val favoriteRow: LiveData<FavoriteRow?> = Transformations.switchMap(eventId) {
        favoriteDao.getFavoriteRowLiveData(Event.TABLE_NAME, it)
    }

    val isFavorite: LiveData<Boolean> = Transformations.map(favoriteRow) {
        it != null
    }

    val remainingTime: LiveData<String> = Transformations.map(event) {
        val remainingTime = (DateTime.now().millis - it.date.millis).toBigDecimal()
                .div(BigDecimal(1000 * 60 * 60 * 24)).multiply(BigDecimal(-1))

        if (remainingTime > BigDecimal(14)) "Még ${remainingTime.div(BigDecimal(7))} hét"
        else "Még $remainingTime nap"
    }

    val schedule: LiveData<PagedList<ScheduleRow>> = LivePagedListBuilder(scheduleDao.getAllPaged(), 20).build()

    val weatherText: MutableLiveData<String> = MutableLiveData()

    val messageText: MutableLiveData<EventWrapper<String>> = MutableLiveData()
    val isRefreshing: MutableLiveData<EventWrapper<Boolean>> = MutableLiveData()

    fun setEventId(eventId: Int?) {
        this.eventId.postValue(eventId)
    }

    fun addRemoveFavorite() {
        launch {
            eventId.value?.let {
                val favoriteRow = favoriteDao.getFavoriteRow(Event.TABLE_NAME, it)

                if (favoriteRow == null)
                    favoriteDao.insert(FavoriteRow(Event.TABLE_NAME, it))
                else
                    favoriteDao.delete(favoriteRow)
            }
        }
    }

    suspend fun downloadEventAndLocation(eventId: Int) {
        val response = synchronizer.getEventAndLocationDetails(eventId)
        isRefreshing.postValue(EventWrapper(false))

        when (response) {
            is Result.Error -> {
                messageText.postValue(EventWrapper("Hiba: ${response.networkError.errorCode} - ${response.networkError.errorMessage}"))
                Timber.e(response.networkError.errorMessage)
            }
        }
    }

    suspend fun downloadWeather(location1: Location) {
        delay(500)
        val eventDate = event.value?.date
        if (eventDate == null) {
            weatherText.postValue("N/A")
            return
        }

        val difference = Duration(DateTime.now().toLocalDate().toDateTimeAtStartOfDay(), eventDate.toLocalDate().toDateTimeAtStartOfDay())

        if (difference.standardDays > 4 || difference.standardDays < 0) {
            weatherText.postValue("N/A")
            return
        }

        val result = weatherService.getFiveDayForecast(
                location1.latitude.toString().replace(".", ","),
                location1.latitude.toString().replace(".", ",")).await()

        if (result is Result.Error) {
            Timber.e(result.networkError)
            weatherText.postValue("N/A")
            return
        } else if (result is Result.Success) {
            val weather = result.data.weatherObjects
                    .asSequence()
                    .filter { it.getDate().toLocalDate() == eventDate.toLocalDate() }
                    .firstOrNull { it.getDate().hourOfDay == 12 }
                    ?: result.data.weatherObjects.firstOrNull { it.getDate().toLocalDate() == eventDate.toLocalDate() }

            weatherText.postValue("${weather?.mainObject?.temp?.toInt()} fok, ${weather?.weatherObjectItems?.get(0)?.description}")
            return
        }
        weatherText.postValue("N/A")
    }

    override fun onCleared() {
        super.onCleared()

        job.cancel()
    }
}