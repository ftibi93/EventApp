package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tiborfarago.eventapp.database.daos.CategoryDao
import com.tiborfarago.eventapp.database.daos.EventDao
import com.tiborfarago.eventapp.database.daos.LocationDao
import com.tiborfarago.eventapp.database.views.MainFragmentModel
import com.tiborfarago.resource_module.room.Event
import javax.inject.Inject

data class MainObject(
        var isFavorites: Boolean = false,
        var searchText: String = "%%",
        var orderByField: String = Event.DATE,
        var orderByDirection: String = "ASC",
        var categoryIds: HashSet<Int> = hashSetOf(),
        var locationIds: HashSet<Int> = hashSetOf()
)

class MainFragmentViewModel @Inject constructor(eventDao: EventDao, categoryDao: CategoryDao, locationDao: LocationDao) : ViewModel() {
    val mainObject: MutableLiveData<MainObject> = MutableLiveData()

    init {
        mainObject.value = MainObject()
    }

    val categories = LivePagedListBuilder(categoryDao.getCategoriesPagedList(), 20).build()
    val locations = LivePagedListBuilder(locationDao.getLocationsPagedListSimple(), 20).build()

    val eventList: LiveData<PagedList<MainFragmentModel>> = Transformations.switchMap(mainObject) {
        LivePagedListBuilder(eventDao.getMainRawPaged(
                if (it.isFavorites) listOf(1) else listOf(1, 0),
                it.searchText,
                it.orderByField,
                it.orderByDirection,
                it.categoryIds,
                it.locationIds
        ), 20).build()
    }

    fun negateIsFavorites() {
        mainObject.postValue(mainObject.value?.apply { isFavorites = this.isFavorites.not() })
    }

    fun setFavorites(isFavorites: Boolean) {
        mainObject.postValue(mainObject.value?.apply { this.isFavorites = isFavorites })
    }

    fun setSearchText(text: String) {
        mainObject.postValue(mainObject.value?.apply { this.searchText = "%$text%" })
    }

    fun setOrderByField(fieldName: String) {
        mainObject.postValue(mainObject.value?.apply { this.orderByField = fieldName })
    }

    fun setOrderByDirection(direction: String) {
        mainObject.postValue(mainObject.value?.apply { this.orderByDirection = direction })
    }

    fun addRemoveCategoryId(categoryId: Int, shouldAdd: Boolean) {
        mainObject.postValue(mainObject.value?.apply {
            this.categoryIds = this.categoryIds.apply {
                if (shouldAdd) this.add(categoryId)
                else this.remove(categoryId)
            }
        })
    }

    fun addRemoveLocationId(locationId: Int, shouldAdd: Boolean) {
        mainObject.postValue(mainObject.value?.apply {
            this.locationIds = this.locationIds.apply {
                if (shouldAdd) this.add(locationId)
                else this.remove(locationId)
            }
        })
    }
}