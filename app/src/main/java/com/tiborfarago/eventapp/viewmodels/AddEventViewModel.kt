package com.tiborfarago.eventapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.tiborfarago.eventapp.database.daos.ScheduleDao
import com.tiborfarago.eventapp.database.daos.UserToEventDao
import org.joda.time.DateTime
import javax.inject.Inject

class AddEventViewModel @Inject constructor(userToEventDao: UserToEventDao, scheduleDao: ScheduleDao): ViewModel() {
    val userList = LivePagedListBuilder(userToEventDao.getUserList(), 20).build()
    val scheduleList = LivePagedListBuilder(scheduleDao.getAllPaged(), 20).build()

    val date: MutableLiveData<DateTime?> = MutableLiveData()
    val time: MutableLiveData<DateTime?> = MutableLiveData()

    val coverPath: MutableLiveData<String> = MutableLiveData()
}