package com.tiborfarago.eventapp

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.tiborfarago.eventapp", appContext.packageName)
    }

    @Test
    fun useAppContextFail() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.tiborfarago.eventapp.fail-on-purpose", appContext.packageName)
    }
}
