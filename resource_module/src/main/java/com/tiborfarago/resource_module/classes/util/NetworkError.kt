package com.tiborfarago.resource_module.classes.util

data class NetworkError(
        val errorCode: Int = -1,
        val errorMessage: String
) : Throwable("$errorCode - $errorMessage") {
    constructor(errorMessage: String) : this(-1, errorMessage)
}