package com.tiborfarago.resource_module.classes

import com.fasterxml.jackson.annotation.JsonIgnore
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.UserToEvents

data class LoginRequest(
        val name: String = "",
        val password: String = ""
)

/**
 * Ezt minden frissítős hívásnál lehet használni
 */
data class IdWithModDate(
        val id: Int,
        val modDate: Long
)

data class IdWithModDateRequest(
        val idWithModDateList: List<IdWithModDate> = emptyList()
) {
    @get:JsonIgnore
    val idsString: String
        get() = idWithModDateList.map { it.id }.joinToString()
}

data class InsertEventRequest(
        val event: Event,
        val userToEvents: List<UserToEvents> = emptyList()
)

data class LatestIdAndBasicObjectIds(
        val latestId: Int,
        val basicObjectIds: List<Int>
)
