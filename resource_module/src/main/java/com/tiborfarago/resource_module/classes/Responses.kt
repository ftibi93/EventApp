package com.tiborfarago.resource_module.classes

import com.tiborfarago.resource_module.room.*
import org.joda.time.DateTime

data class LoginResponse(
        val id: Int,
        val modDate: Long,
        val userToEvents: List<UserToEvents>? = null,
        val token: String = ""
)

data class NewEvent(
        val id: Int,
        val locationId: Int,
        val categoryId: Int,
        val name: String,
        val date: DateTime,
        val modDate: DateTime
) {
    interface Interface {
        fun getId(): Int
        fun getLocationId(): Int
        fun getCategoryId(): Int
        fun getName(): String
        fun getDate(): DateTime
        fun getModDate(): DateTime
    }
}

fun NewEvent.Interface.toNewEvent(): NewEvent = NewEvent(getId(), getLocationId(), getCategoryId(), getName(), getDate(), getModDate())

data class NewLocation(
        val id: Int,
        val name: String,
        val city: String,
        val modDate: DateTime
) {
    interface Interface {
        fun getId(): Int
        fun getName(): String
        fun getCity(): String
        fun getModDate(): DateTime
    }
}

fun NewLocation.Interface.toNewLocation(): NewLocation = NewLocation(getId(), getName(), getCity() ,getModDate())

data class NewEventsResponse(
        val newEventList: List<NewEvent> = emptyList(),
        val newLocationList: List<NewLocation> = emptyList(),
        val userToEvents: List<UserToEvents> = emptyList()
)

data class UpdateBaseEventsResponse(
        val eventList: List<NewEvent> = emptyList(),
        val eventsToDelete: List<Int> = emptyList()
)

data class UpdateDetailsEventsResponse(
        val eventList: List<Event> = emptyList(),
        val eventsToDelete: List<Int> = emptyList()
)

data class EventAndLocationDetailsResponse(
        val event: Event,
        val location: Location
)

data class UpdateBaseLocationsResponse(
        val locationList: List<NewLocation> = emptyList(),
        val locationsToDelete: List<Int> = emptyList()
)

data class UpdateLocationDetailsResponse(
        val locationList: List<Location> = emptyList(),
        val locationsToDelete: List<Int> = emptyList()
)

data class UserToEventsResponse(
        val userToEventsList: List<UserToEvents> = emptyList()
)

data class UsersResponse(
        val users: List<User> = emptyList()
)

data class CategoriesResponse(
        val categories: List<Category> = emptyList()
)

data class SimpleResponse(
        val responseText: String
)