package com.tiborfarago.resource_module.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.joda.time.DateTime

class DateTimeMoshiAdapter {
    @ToJson
    fun toJson(dateTime: DateTime): Long = dateTime.millis

    @FromJson
    fun fromJson(millis: Long): DateTime = DateTime(millis)
}