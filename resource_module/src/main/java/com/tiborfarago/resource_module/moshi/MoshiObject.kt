package com.tiborfarago.resource_module.moshi

import com.squareup.moshi.Moshi

object MoshiObject {
    val moshiObject: Moshi = Moshi.Builder()
            .add(DateTimeMoshiAdapter())
            .add(DoubleToLongAdapter())
            .build()
}