package com.tiborfarago.resource_module.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

class DoubleToLongAdapter {
    @FromJson
    fun doubleToBigDecimal(number: Double): BigDecimal = BigDecimal(number)

    @ToJson
    fun bigDecimalToDouble(number: BigDecimal): Double = number.toDouble()
}