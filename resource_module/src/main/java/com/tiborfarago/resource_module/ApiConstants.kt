package com.tiborfarago.resource_module

object ApiConstants {
    const val EXISTING_ID = "existing_id"

    const val NOT_EXISTING_ID_CATEGORY = "not_existing_id_category"
    const val NOT_EXISTING_ID_LOCATION = "not_existing_id_location"

    const val NOT_EXISTING_ID = "not_existing_id"
    const val EXISTING_NAME = "existing_name"

    const val SUCCESS = "success"

    const val WRONG_USERNAME = "wrong_username"
    const val WRONG_PASSWORD = "wrong_password"
}