package com.tiborfarago.resource_module.utils

import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.room.Event
import com.tiborfarago.resource_module.room.User
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt

class TestData {
    companion object {
        fun generateUsers(): List<User> {
            return listOf(User(0, "elso", BCrypt.hashpw("elso", BCrypt.gensalt(10)), 0, DateTime.now()))
        }

        fun generateEvents(): List<Event> {
            val eventList = mutableListOf<Event>()

            for (i in 1..50) {
                eventList.add(Event(
                        categoryId = 2,
                        name = "Event$i",
                        date = DateTime.now(),
                        description = "desc$i",
                        schedule = "schedule$i",
                        modDate = DateTime.now()
                ))
            }

            return eventList
        }

        fun generateCategories(): List<Category> {
            val categoryList = mutableListOf<Category>()

            for (i in 1..5) {
                categoryList.add(Category(
                        id = i,
                        name = "Category$i",
                        modDate = DateTime.now()
                ))
            }

            return categoryList
        }
    }
}