package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime

@Entity(tableName = Category.TABLE_NAME)
data class Category(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = Category.ID) var id: Int = 0,
        @ColumnInfo(name = Category.NAME) var name: String = "",
        @ColumnInfo(name = Category.MOD_DATE) var modDate: DateTime = DateTime(1970, 1, 1, 0, 0)
) : BaseEntity(id, name, modDate) {
    companion object {
        const val TABLE_NAME = "category"

        const val ID = "id"
        const val NAME = "name"
        const val MOD_DATE = "mod_date"
    }
}