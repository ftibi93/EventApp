package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime

@Entity(tableName = Event.TABLE_NAME)
data class Event(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = Event.ID) var id: Int = 0,
        @ColumnInfo(name = Event.LOCATION_ID) var locationId: Int = 0,
        @ColumnInfo(name = Event.CATEGORY_ID) var categoryId: Int = 0,
        @ColumnInfo(name = Event.NAME) var name: String = "",
        @ColumnInfo(name = Event.DATE) var date: DateTime = DateTime(1970, 1, 1, 0, 0),
        @ColumnInfo(name = Event.DESCRIPTION) var description: String? = null,
        @ColumnInfo(name = Event.SCHEDULE) var schedule: String? = null,
        @ColumnInfo(name = Event.MOD_DATE) var modDate: DateTime = DateTime(1970, 1, 1, 0, 0)
) : BaseEntity(id, name, modDate) {
    companion object {
        const val TABLE_NAME = "event"

        const val ID = "id"
        const val LOCATION_ID = "location_id"
        const val CATEGORY_ID = "category_id"
        const val NAME = "name"
        const val DATE = "date"
        const val DESCRIPTION = "description"
        const val SCHEDULE = "schedule"
        const val MOD_DATE = "mod_date"
    }
}