package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import org.joda.time.DateTime

@Entity(tableName = UserToEvents.TABLE_NAME, primaryKeys = [(UserToEvents.USER_ID), (UserToEvents.EVENT_ID)])
data class UserToEvents(
        @ColumnInfo(name = UserToEvents.USER_ID) var userId: Int = 0,
        @ColumnInfo(name = UserToEvents.EVENT_ID) var eventId: Int = 0,
        @ColumnInfo(name = UserToEvents.MOD_DATE) var modDate: DateTime = DateTime(1970, 1, 1, 0, 0)
) {
    companion object {
        const val TABLE_NAME = "user_to_events"

        const val USER_ID = "user_id"
        const val EVENT_ID = "event_id"
        const val MOD_DATE = "mod_date"
    }
}