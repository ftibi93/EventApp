package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = Change.TABLE_NAME, primaryKeys = [Change.CHANGED_TABLE, Change.CHANGED_ROW_ID])
data class Change(
        @ColumnInfo(name = Change.CHANGED_TABLE) var changedTable: String,
        @ColumnInfo(name = Change.CHANGED_ROW_ID) var changedRowId: Int,
        @ColumnInfo(name = Change.CHANGED_TYPE) var type: Int
) {
    object TYPE {
        const val ADD = 1
        const val MODIFY = 0
        const val DELETE = -1
    }

    companion object {
        const val TABLE_NAME = "change"

        const val CHANGED_TABLE = "changed_table"
        const val CHANGED_ROW_ID = "changed_row_id"
        const val CHANGED_TYPE = "changed_type"
    }
}