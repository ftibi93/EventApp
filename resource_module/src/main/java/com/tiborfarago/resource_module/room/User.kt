package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime

@Entity(tableName = User.TABLE_NAME)
data class User(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = User.ID) var id: Int = 0,
        @ColumnInfo(name = User.NAME) var name: String = "",
        @ColumnInfo(name = User.PASSWORD) var password: String = "",
        @ColumnInfo(name = User.PERMISSION) var permission: Int = 0,
        @ColumnInfo(name = User.MOD_DATE) var modDate: DateTime = DateTime(1970, 1, 1, 0, 0),
        @ColumnInfo(name = User.IS_SHOWN) var isShown: Boolean = true
) : BaseEntity(id, name, modDate) {
    companion object {
        const val TABLE_NAME = "user"

        const val ID = "id"
        const val NAME = "name"
        const val PASSWORD = "password"
        const val PERMISSION = "permission"
        const val MOD_DATE = "mod_date"
        const val IS_SHOWN = "is_shown"
    }
}