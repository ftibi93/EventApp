package com.tiborfarago.resource_module.room

import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnore
import org.joda.time.DateTime

abstract class BaseEntity(
        @JsonIgnore @Transient @Ignore var baseId: Int = 0,
        @JsonIgnore @Transient @Ignore var baseName: String = "",
        @JsonIgnore @Transient @Ignore var baseModDate: DateTime = DateTime(1970, 1, 1, 0, 0)
) {
    companion object {
        const val ID = "id"
        const val NAME = "name"
        const val MOD_DATE = "mod_date"
    }
}