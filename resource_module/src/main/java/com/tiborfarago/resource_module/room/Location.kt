package com.tiborfarago.resource_module.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.math.BigDecimal

@Entity(tableName = Location.TABLE_NAME)
data class Location(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = Location.ID) var id: Int = 0,
        @ColumnInfo(name = Location.NAME) var name: String = "",
        @ColumnInfo(name = Location.CITY) var city: String = "",
        @ColumnInfo(name = Location.ADDRESS) var address: String? = null,
        @ColumnInfo(name = Location.LONGITUDE) var longitude: BigDecimal? = null,
        @ColumnInfo(name = Location.LATITUDE) var latitude: BigDecimal? = null,
        @ColumnInfo(name = Location.MOD_DATE) var modDate: DateTime = DateTime(1970, 1,1,0,0)
) : BaseEntity(id, name, modDate) {

    companion object {
        const val TABLE_NAME = "location"

        const val ID = "id"
        const val NAME = "name"
        const val CITY = "city"
        const val ADDRESS = "address"
        const val LONGITUDE = "longitude"
        const val LATITUDE = "latitude"
        const val MOD_DATE = "mod_date"


    }
}