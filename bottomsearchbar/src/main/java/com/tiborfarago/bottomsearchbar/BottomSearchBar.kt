package com.tiborfarago.bottomsearchbar

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.EditText
import androidx.annotation.IdRes
import androidx.appcompat.widget.AppCompatImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.lang.IllegalStateException

class BottomSearchBar constructor(context: Context?, private val attrs: AttributeSet?, private val defStyleAttr: Int) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        this.visibility = View.GONE
        init()
    }

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    lateinit var editText: EditText
        private set
    private lateinit var clearButton: AppCompatImageButton

    @IdRes
    private var bottomAppBarId: Int = 0

    private var isExtended = false

    @IdRes
    private var fabResId: Int = 0
    private var floatingActionButton: FloatingActionButton? = null
    private var fabShouldBeVisible: Boolean = false

    private fun init() {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.BottomSearchBar, defStyleAttr, 0)
        bottomAppBarId = attributes.getResourceId(R.styleable.BottomSearchBar_bottom_bar_id, 0)
        fabResId = attributes.getResourceId(R.styleable.BottomSearchBar_bottom_bar_fab, 0)

        if (bottomAppBarId == 0)
            throw Exception("app:bottom_bar_id attribute must be defined")

        inflate(context, R.layout.layout_search_bar, this)

        editText = findViewById(R.id.search_layout_edit_text)
        clearButton = findViewById(R.id.search_layout_clear)

        editText.hint = attributes.getString(R.styleable.BottomSearchBar_bottom_bar_hint)

        setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))

        clearButton.setOnClickListener {
            editText.setText("")
            hide()
        }

        shouldShow(false, true)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (fabResId != 0)
            floatingActionButton = (this@BottomSearchBar.parent as ViewGroup).findViewById(fabResId)
        else
            throw IllegalStateException("The BottomSearchBar and the FloatingActionButton must have a common parent layout")

        fabShouldBeVisible = floatingActionButton?.isOrWillBeShown == true
    }

    fun show() = shouldShow(true)
    fun hide() = shouldShow(false)

    fun showHide() = shouldShow(!isExtended)

    private fun shouldShow(shouldShow: Boolean, isFirstRun: Boolean = false) {
        if (fabShouldBeVisible) {
            if (floatingActionButton?.isOrWillBeShown == true)
                floatingActionButton?.hide()
            else if (floatingActionButton?.isOrWillBeShown == false)
                floatingActionButton?.show()
        }

        isExtended = shouldShow
        if (shouldShow) {
            val layoutParams = (this.layoutParams as CoordinatorLayout.LayoutParams).apply {
                gravity = Gravity.TOP
                anchorGravity = Gravity.TOP
                anchorId = bottomAppBarId
                bottomMargin = 0
            }

            this.layoutParams = layoutParams
        }

        val translateAnim = TranslateAnimation(0F, 0F, dpToPx(if (shouldShow) 56F else 0f), dpToPx(if (shouldShow) 0F else 56F)).apply {
            isFillEnabled = true
            fillAfter = true
            duration = 200

            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    if (!shouldShow) {
                        val layoutParams = (this@BottomSearchBar.layoutParams as CoordinatorLayout.LayoutParams).apply {
                            gravity = Gravity.BOTTOM
                            anchorGravity = Gravity.BOTTOM
                            bottomMargin = dpToPx(-56F).toInt()
                        }

                        this@BottomSearchBar.layoutParams = layoutParams
                        editText.clearFocus()

                        if (isFirstRun) this@BottomSearchBar.visibility = View.VISIBLE
                    }
                }

                override fun onAnimationStart(p0: Animation?) {
                }

            })
        }

        this.startAnimation(translateAnim)
    }

    fun setupFloatingActionButton(floatingActionButton: FloatingActionButton) {
        this.floatingActionButton = floatingActionButton
        this.fabShouldBeVisible = floatingActionButton.isOrWillBeShown
    }

    private fun dpToPx(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
}