package com.tiborfarago.event_api_spring

import com.tiborfarago.event_api_spring.businesslogic.CategoryService
import com.tiborfarago.event_api_spring.database.repositories.CategoryRepository
import com.tiborfarago.event_api_spring.database.tables.toSpring
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.classes.util.NetworkError
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.utils.Result
import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@DataJpaTest
class Tests {

    @Autowired
    private lateinit var entityManager: TestEntityManager

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    private val categoryService: CategoryService by lazy { CategoryService(categoryRepository) }

    private val category = Category(1, "Cat1", DateTime.now())

    @Test
    fun test_Category_insert() {
        val result = categoryService.insert(category)

        if (result is Result.Success) {
            Assert.assertEquals(
                    Result.Success(SimpleResponse(ApiConstants.SUCCESS)),
                    result
            )

            Assert.assertEquals(category.toSpring(), categoryRepository.findByIdOrNull(category.id))

        } else
            Assert.fail("Category insertion was unsuccessful")
    }

    @Test
    fun test_Category_insert_with_same_id() {
        test_Category_insert()

        val result = categoryService.insert(category)

        if (result is Result.Error)
            Assert.assertEquals(
                    NetworkError(ApiConstants.EXISTING_ID),
                    result.networkError
            )
        else
            Assert.fail("Category insertion should not have been successful")

    }

}