 CREATE DATABASE IF NOT EXISTS `event_database` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
 USE `event_database`;

 CREATE TABLE `category` (
   `id` int(11) NOT NULL,
   `name` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
   `mod_date` datetime(6) NOT NULL
 ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

 CREATE TABLE `event` (
   `id` int(11) NOT NULL,
   `location_id` int(11) NOT NULL,
   `category_id` int(11) NOT NULL,
   `name` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
   `date` datetime(6) NOT NULL,
   `description` varchar(1000) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
   `schedule` varchar(1000) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
   `mod_date` datetime(6) NOT NULL
 ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

 CREATE TABLE `location` (
   `id` int(11) NOT NULL,
   `name` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
   `city` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
   `address` varchar(50) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
   `longitude` decimal(13,10) DEFAULT NULL,
   `latitude` decimal(13,10) DEFAULT NULL,
   `mod_date` datetime(6) NOT NULL
 ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `mod_date` datetime(6) NOT NULL,
  `permission` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

CREATE TABLE `user_to_events` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `mod_date` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_event_location_id_id` (`location_id`),
  ADD KEY `fk_event_category_id_id` (`category_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_to_events`
--
ALTER TABLE `user_to_events`
  ADD PRIMARY KEY (`user_id`,`event_id`),
  ADD KEY `fk_user_to_events_event_id_id` (`event_id`);

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;