-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 13, 2019 at 03:40 PM
-- Server version: 10.3.12-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event_database`
--

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `mod_date`) VALUES
(6, 'Kzssegi', '2018-09-24 00:00:00.000000'),
(3, 'Kulturális', '2018-09-15 14:07:10.353000'),
(4, 'Szabadtéri', '2018-09-15 14:07:10.353000'),
(2, 'Városi', '2018-09-15 14:07:10.353000'),
(7, 'Közösségi új ', '2019-02-10 20:50:47.000000');

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `location_id`, `category_id`, `name`, `date`, `description`, `schedule`, `mod_date`) VALUES
(1, 1, 2, 'Vásár #1mod', '2018-09-01 11:43:00.000000', '', '', '2019-02-10 21:00:24.000000'),
(2, 1, 2, 'Vásár #2', '2018-09-01 10:14:21.441000', 'desc2', 'schedule2', '2018-11-22 11:43:35.378000'),
(3, 1, 2, 'Vásár #3', '2018-12-13 11:14:21.441000', 'desc3_mod2', 'schedule3', '2018-11-22 11:43:35.378000'),
(4, 2, 2, 'Vásár #4', '2019-01-24 10:14:21.441000', 'desc4', 'schedule4', '2018-11-22 11:43:35.378000'),
(5, 1, 2, 'Vásár #5', '2019-05-15 10:14:21.441000', 'desc5', 'schedule5', '2018-11-22 11:43:35.378000'),
(31, 1, 2, 'Húsvéti programok', '2019-07-26 10:14:21.441000', 'desc31', 'schedule31', '2018-11-22 11:43:35.378000'),
(32, 1, 2, 'Városi napok', '2018-11-27 10:14:21.441000', 'desc32', 'schedule32', '2018-11-22 11:43:35.378000'),
(33, 1, 2, 'Majális', '2019-05-09 10:14:21.441000', 'desc33', 'schedule33', '2018-11-22 11:43:35.378000'),
(34, 1, 2, 'Event34', '2018-09-01 10:14:21.441000', 'desc34', 'schedule34', '2018-09-01 10:14:21.441000'),
(35, 1, 2, 'Event35', '2018-09-01 10:14:21.441000', 'desc35', 'schedule35', '2018-09-01 10:14:21.441000'),
(103, 2, 7, 'Spring event ', '2019-02-28 21:20:00.000000', 'It is written ', '8:0|*|9:0|*|First and first \n9:0|*|10:0|*|Second ', '2019-02-10 21:02:13.000000'),
(37, 1, 2, 'Event37', '2018-09-01 10:14:21.441000', 'desc37', 'schedule37', '2018-09-01 10:14:21.441000'),
(94, 3, 2, 'kettos', '2018-11-14 13:35:00.000000', '111', '', '2018-09-29 20:14:13.970000'),
(95, 2, 2, 'jó dátum', '2018-10-03 14:45:00.000000', 'leírás', '', '2018-10-03 18:00:34.679000'),
(96, 4, 2, 'Horgászverseny', '2018-10-26 21:43:00.000000', 'leiras', '10:0|*|11:0|*|er', '2018-11-22 22:40:10.341000'),
(97, 3, 2, 'MOSTANI', '2018-10-24 17:14:00.000000', '', '', '2018-10-23 19:15:01.931000'),
(98, 1, 3, 'Filmvetítés #1', '2018-12-02 18:12:00.000000', 'Példa \n\nJegyár: 100 Ft \n\nItalok: 50 Ft ', '10:0|*|11:0|*|Gyülekezés \n13:0|*|18:0|*|Film vetítése ', '2018-11-30 12:44:38.745000');

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `city`, `address`, `longitude`, `latitude`, `mod_date`) VALUES
(3, 'Healing Waters Ministries', 'Holly Pond', 'Brooklyn Road 342.', '-86.6156032000', '34.1794893000', '2018-09-24 21:36:22.525000'),
(2, 'Malom Központ', 'Kecskemét', 'Csányi utca 2.', '19.6890870000', '46.9077410000', '2018-09-09 18:10:42.379000'),
(1, 'Malom Mozi', 'Kecskemét', 'Csányi utca 2.', '19.6890870000', '46.9077410000', '2018-09-09 18:10:42.379000'),
(4, 'Heart of Dixie Printers', 'Holly Pond', 'U.S. 278 11187.', '-86.6101985000', '34.1748905000', '2018-09-29 11:12:36.813000'),
(5, 'Harris Auto Repair & Wrecker', 'Holly Pond', 'U.S. 278 10950.', '-86.6144200000', '34.1737550000', '2018-09-29 20:09:51.938000'),
(8, 'Könyvkosár Bt.', 'Szabadszállás', 'Orgona utca 18.', '19.2341465000', '46.8736767000', '2019-02-08 20:31:40.000000'),
(9, 'Robiol Kft.', 'Szabadszállás', 'Petőfi Sándor út 37.', '19.2325304000', '46.8706711000', '2019-02-10 20:59:38.000000');

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `mod_date`, `permission`) VALUES
(104, 'admin', '$2a$10$k6OjA1ENcI6dH1YNb0nnvOcmOzYr9UcUwBExuj5E0rUwzWsNnI3uy', '2018-08-31 22:08:10.900000', 999),
(103, 'elso', '$2a$10$FCp9mKOMq1w24c0F9nE8zun0Tn1sP/uHsCBBkxruTrVk4AAu0Msai', '2018-08-31 22:08:10.771000', 0),
(1000, 'dezso2', '$2a$10$HMy0A/uklDMFNF3DFttmz.zyCHankWAorkBBs0i9PghzUHZ252EgW', '1970-01-01 00:00:00.000000', 0);

--
-- Dumping data for table `user_to_events`
--

INSERT INTO `user_to_events` (`user_id`, `event_id`, `mod_date`) VALUES
(1, 2, '2018-07-14 00:00:00.000000'),
(104, 94, '2018-09-29 20:14:13.987000'),
(103, 94, '2018-09-29 20:14:13.982000'),
(103, 98, '2018-11-30 12:44:38.749000');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
