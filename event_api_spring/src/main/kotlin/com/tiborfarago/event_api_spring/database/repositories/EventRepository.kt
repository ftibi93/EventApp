package com.tiborfarago.event_api_spring.database.repositories

import com.tiborfarago.event_api_spring.database.tables.Event
import com.tiborfarago.resource_module.classes.NewEvent
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface EventRepository : CrudRepository<Event, Int> {
    @Query("SELECT event FROM Event event WHERE event.id IN ?1 AND event.id > ?2")
    fun allAdminEvents(ids: List<Int>, latestId: Int): List<Event>

    @Query("""
        SELECT e.id AS id, e.location.id AS location, e.category.id as categoryId,
        e.name AS name, e.date as date, e.modDate as modDate
        FROM Event e
        WHERE e.id IN ?1""")
    fun getBaseEventsWithIds(ids: List<Int>): List<NewEvent.Interface>

    @Query("""
        SELECT e.id AS id, e.location.id as locationId, e.category.id as categoryId,
        e.name as name, e.date as date, e.modDate as modDate
        FROM Event e
        WHERE e.id > ?1
    """)
    fun getNewEvents(latestId: Int): List<NewEvent.Interface>
}