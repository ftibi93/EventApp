package com.tiborfarago.event_api_spring.database.tables

import com.tiborfarago.event_api_spring.database.repositories.CategoryRepository
import com.tiborfarago.event_api_spring.database.repositories.LocationRepository
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.room.Event
import org.joda.time.DateTime
import org.springframework.data.repository.findByIdOrNull
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = Event.TABLE_NAME)
data class Event(
        @Id
        @Column(name = Event.ID)
        var id: Int,
        @NotEmpty
        @Column(name = Event.NAME)
        var name: String,
        @NotEmpty
        @Column(name = Event.DATE)
        var date: DateTime,
        @Column(name = Event.DESCRIPTION)
        var description: String?,
        @Column(name = Event.SCHEDULE)
        var schedule: String?,
        @NotEmpty
        @Column(name = Event.MOD_DATE)
        var modDate: DateTime
) {
    @ManyToOne
    @JoinColumn(name = Event.CATEGORY_ID)
    lateinit var category: Category

    @ManyToOne
    @JoinColumn(name = Event.LOCATION_ID)
    lateinit var location: Location
}

fun com.tiborfarago.event_api_spring.database.tables.Event.toRoom(): Event =
        Event(id, location.id, category.id, name, date, description, schedule, modDate)

@Throws(ResourceNotFoundException::class)
fun Event.toSpring(
        categoryRepository: CategoryRepository,
        locationRepository: LocationRepository
): com.tiborfarago.event_api_spring.database.tables.Event =
        com.tiborfarago.event_api_spring.database.tables.Event(id, name, date, description, schedule, modDate).apply {
            category = categoryRepository.findByIdOrNull(categoryId)
                    ?: throw ResourceNotFoundException(ApiConstants.NOT_EXISTING_ID_CATEGORY)

            location = locationRepository.findByIdOrNull(locationId)
                    ?: throw ResourceNotFoundException(ApiConstants.NOT_EXISTING_ID_LOCATION)
        }