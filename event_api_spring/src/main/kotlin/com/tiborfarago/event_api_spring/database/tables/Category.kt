package com.tiborfarago.event_api_spring.database.tables

import com.tiborfarago.resource_module.room.Category
import org.hibernate.annotations.Type
import org.joda.time.DateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "category")
data class Category(
        @Id
        //@GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Int,
        @Column(name = "name")
        @NotEmpty
        var name: String,
        @Column(name = "mod_date")
        @NotEmpty
        var modDate: DateTime
) {
        @OneToMany(mappedBy = "category", cascade = [CascadeType.ALL])
        lateinit var events: Set<Event>
}

fun com.tiborfarago.event_api_spring.database.tables.Category.toRoom(): Category = Category(id, name, modDate)
fun Category.toSpring(): com.tiborfarago.event_api_spring.database.tables.Category =
        com.tiborfarago.event_api_spring.database.tables.Category(id, name, modDate)