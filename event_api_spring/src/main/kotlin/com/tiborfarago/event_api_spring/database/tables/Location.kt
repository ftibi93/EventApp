package com.tiborfarago.event_api_spring.database.tables

import org.joda.time.DateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = com.tiborfarago.resource_module.room.Location.TABLE_NAME)
data class Location(
        @Id
        @Column(name = com.tiborfarago.resource_module.room.Location.ID)
        var id: Int,
        @NotEmpty
        @Column(name = com.tiborfarago.resource_module.room.Location.NAME)
        var name: String,
        @NotEmpty
        @Column(name = com.tiborfarago.resource_module.room.Location.CITY)
        var city: String,
        @Column(name = com.tiborfarago.resource_module.room.Location.ADDRESS)
        var address: String?,
        @Column(name = com.tiborfarago.resource_module.room.Location.LONGITUDE)
        var longitude: Double?,
        @Column(name = com.tiborfarago.resource_module.room.Location.LATITUDE)
        var latitude: Double?,
        @Column(name = com.tiborfarago.resource_module.room.Location.MOD_DATE)
        var modDate: DateTime
) {
        @OneToMany(mappedBy = "location", cascade = [CascadeType.ALL])
        lateinit var events: Set<Event>
}

fun Location.toRoom(): com.tiborfarago.resource_module.room.Location =
        com.tiborfarago.resource_module.room.Location(id, name, city, address, longitude?.toBigDecimal(), latitude?.toBigDecimal(), modDate)

fun com.tiborfarago.resource_module.room.Location.toSpring(): Location =
        Location(id, name, city, address, longitude?.toDouble(), latitude?.toDouble(), modDate)