package com.tiborfarago.event_api_spring.database.repositories

import com.tiborfarago.event_api_spring.database.tables.UserToEvents
import com.tiborfarago.event_api_spring.database.tables.UserToEventsCompositeKey
import org.springframework.data.repository.CrudRepository

interface UserToEventsRepository : CrudRepository<UserToEvents, UserToEventsCompositeKey> {
    fun findByUserId(userId: Int): List<UserToEvents>
}