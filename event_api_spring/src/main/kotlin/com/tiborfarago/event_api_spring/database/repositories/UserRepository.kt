package com.tiborfarago.event_api_spring.database.repositories

import com.tiborfarago.event_api_spring.database.tables.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Int> {
    fun findByName(name: String): List<User>

    fun findByNameAndPermission(name: String, permission: Int): List<User>
}