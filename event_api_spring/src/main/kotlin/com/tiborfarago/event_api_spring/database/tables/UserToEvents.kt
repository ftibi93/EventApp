package com.tiborfarago.event_api_spring.database.tables

import com.tiborfarago.resource_module.room.UserToEvents
import org.joda.time.DateTime
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = UserToEvents.TABLE_NAME)
@IdClass(UserToEventsCompositeKey::class)
data class UserToEvents(
        @Id
        @Column(name = UserToEvents.USER_ID)
        var userId: Int,
        @Id
        @Column(name = UserToEvents.EVENT_ID)
        var eventId: Int,
        @NotEmpty
        @Column(name = UserToEvents.MOD_DATE)
        var modDate: DateTime
)

data class UserToEventsCompositeKey(
        var userId: Int = 0,
        var eventId: Int = 0
) : Serializable

fun com.tiborfarago.event_api_spring.database.tables.UserToEvents.toRoom(): UserToEvents = UserToEvents(userId, eventId, modDate)

fun UserToEvents.toSpring(): com.tiborfarago.event_api_spring.database.tables.UserToEvents =
        com.tiborfarago.event_api_spring.database.tables.UserToEvents(userId, eventId, modDate)