package com.tiborfarago.event_api_spring.database.tables

import org.joda.time.DateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "user")
data class User(
        @Id
        @Column(name = "id")
        var id: Int,
        @NotEmpty
        @Column(name = "name")
        var name: String,
        @NotEmpty
        @Column(name = "password")
        var password: String,
        @NotEmpty
        @Column(name = "mod_date")
        var modDate: DateTime,
        @NotEmpty
        @Column(name = "permission")
        var permission: Int
)

fun User.toRoom(): com.tiborfarago.resource_module.room.User = com
        .tiborfarago.resource_module.room.User(id, name, password, permission, modDate)
fun com.tiborfarago.resource_module.room.User.toSpring(): User = User(id, name, password, modDate, permission)