package com.tiborfarago.event_api_spring.database.repositories

import com.tiborfarago.event_api_spring.database.tables.Location
import com.tiborfarago.resource_module.classes.NewLocation
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface LocationRepository : CrudRepository<Location, Int> {
    @Query("SELECT loc from Location loc WHERE loc.id IN ?1 OR loc.id > ?2")
    fun allAdminLocations(ids: List<Int>, latestId: Int): List<Location>

    @Query("""
        SELECT l.id as id, l.name as name, l.city as city, l.modDate as modDate
        FROM Location l
        WHERE l.id IN ?1
    """)
    fun getNewLocationsById(ids: List<Int>): List<NewLocation.Interface>
}