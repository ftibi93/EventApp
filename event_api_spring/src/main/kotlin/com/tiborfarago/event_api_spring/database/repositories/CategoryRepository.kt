package com.tiborfarago.event_api_spring.database.repositories

import com.tiborfarago.event_api_spring.database.tables.Category
import org.springframework.data.repository.CrudRepository

interface CategoryRepository : CrudRepository<Category, Int>