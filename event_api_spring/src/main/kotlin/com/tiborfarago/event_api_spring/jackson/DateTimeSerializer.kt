package com.tiborfarago.event_api_spring.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.joda.time.DateTime
import org.springframework.boot.jackson.JsonComponent

@JsonComponent
class DateTimeSerializer : JsonSerializer<DateTime>() {
    override fun serialize(value: DateTime?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeNumber((value?.millis ?: 0))
    }

}