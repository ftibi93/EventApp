package com.tiborfarago.event_api_spring.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import org.joda.time.DateTime
import org.springframework.boot.jackson.JsonComponent

/*
@JsonComponent
class DateTimeDeserializer : JsonDeserializer<DateTime>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): DateTime {
        return DateTime(p?.valueAsLong)
    }
}*/
