package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.businesslogic.LocationService
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.utils.Result
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/location")
class LocationController constructor(
        private val locationService: LocationService
) {

    @PostMapping("insertLocation")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun insertLocation(@RequestBody request: Location): SimpleResponse =
            when (val response = locationService.insertLocation(request)) {
                is Result.Success -> response.data
                is Result.Error -> throw ResourceNotFoundException(response.networkError.errorMessage)
            }

    @PostMapping("modifyLocation")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun modifyLocation(@RequestBody request: Location): SimpleResponse =
            when (val response = locationService.modifyLocation(request)) {
                is Result.Success -> response.data
                is Result.Error -> throw ResourceNotFoundException(response.networkError.errorMessage)
            }

    @PostMapping("deleteLocation")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun deleteLocation(@RequestParam("id") id: Int): SimpleResponse = locationService.deleteLocation(id)

}