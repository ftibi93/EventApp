package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.SimpleResponse
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.InputStream
import java.io.OutputStream

@Controller
@RequestMapping("/api/files")
class FileController {

    @GetMapping("downloadCover")
    fun downloadCover(@RequestParam id: String): ResponseEntity<Resource> {
        val folder = File("${System.getProperty("user.home")}/event_covers")
        val file = folder.listFiles().firstOrNull { f -> f.nameWithoutExtension == id } ?: throw ResourceNotFoundException(HttpStatus.NOT_FOUND.name)

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=${file.name}").body(UrlResource(file.toURI()))
    }

    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    @PostMapping("uploadCover")
    fun uploadCover(@RequestParam("file") file: MultipartFile, @RequestParam("id") id: String): ResponseEntity<SimpleResponse> {
        val fileOut = File("${System.getProperty("user.home")}/event_covers", "$id.jpg")

        fileOut.outputStream().buffered().use {
            file.inputStream.convertToString(it)
        }

        return ResponseEntity.ok(SimpleResponse(ApiConstants.SUCCESS))
    }

    private fun InputStream.convertToString(
            outputStream: OutputStream
    ) {
        val buffer = ByteArray(1024)

        while (true) {
            val byteCount = this.read(buffer)
            if (byteCount < 0) break
            outputStream.write(buffer, 0, byteCount)
        }
    }
}