package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.businesslogic.UserService
import com.tiborfarago.resource_module.classes.LoginRequest
import com.tiborfarago.resource_module.classes.LoginResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/useradmin")
class UserAdminController constructor(
        private val userService: UserService
) {
    @PostMapping("login")
    fun login(@RequestBody request: LoginRequest): LoginResponse = userService.userAdminLogin(request)
}