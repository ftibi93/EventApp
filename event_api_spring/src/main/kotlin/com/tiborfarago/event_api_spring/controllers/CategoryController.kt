package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.businesslogic.CategoryService
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.classes.CategoriesResponse
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.utils.Result
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/category")
class CategoryController constructor(
        private val categoryService: CategoryService
) {

    @GetMapping("getAllCategories")
    fun getAllCategories(): CategoriesResponse = categoryService.getAllCategories()

    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    @PostMapping("insertCategory")
    fun insertCategory(@RequestBody category: Category): SimpleResponse = when (val result = categoryService.insert(category)) {
        is Result.Success -> result.data
        is Result.Error -> throw ResourceNotFoundException(result.networkError.errorMessage)
    }

    @PostMapping("modifyCategory")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun modifyCategory(@RequestBody category: Category): SimpleResponse = when (val result = categoryService.update(category)) {
        is Result.Success -> result.data
        is Result.Error -> throw ResourceNotFoundException(result.networkError.errorMessage)
    }

    @PostMapping("deleteCategory")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun deleteCategory(@RequestParam("id") id: Int): SimpleResponse = categoryService.delete(id)
}