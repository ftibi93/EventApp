package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.businesslogic.EventService
import com.tiborfarago.event_api_spring.businesslogic.LocationService
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.utils.Result
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/event")
class EventController constructor(
        private val eventService: EventService,
        private val locationService: LocationService
) {

    @PostMapping("getEventsBaseUpdate")
    fun getEventsBaseUpdate(@RequestBody request: IdWithModDateRequest): UpdateBaseEventsResponse =
            eventService.getEventsBaseUpdate(request)

    @PostMapping("getEventsDetailsUpdate")
    fun getEventsDetailsUpdate(@RequestBody request: IdWithModDateRequest): UpdateDetailsEventsResponse =
            eventService.getEventsDetailsUpdate(request)

    @GetMapping("getNewEvents")
    fun getNewEvents(@RequestParam("latestId") latestId: Int): NewEventsResponse =
            eventService.getNewEvents(latestId)

    @GetMapping("getEventDetails")
    fun getEventDetails(@RequestParam("eventId") request: Int): EventAndLocationDetailsResponse =
            eventService.getEventDetails(request)

    @PostMapping("getLocationsBaseUpdate")
    fun getLocationsBaseUpdate(@RequestBody request: IdWithModDateRequest): UpdateBaseLocationsResponse =
            locationService.getLocationsBaseUpdate(request)

    @PostMapping("getLocationsDetailUpdate")
    fun getLocationsDetailsUpdate(@RequestBody request: IdWithModDateRequest): UpdateLocationDetailsResponse =
            locationService.getLocationsDetailsUpdate(request)

    @PostMapping("insertEvent")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun insertEvent(@RequestBody request: InsertEventRequest): SimpleResponse =
            eventService.insert(request)

    @PostMapping("modifyEvent")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun modifyEvent(@RequestBody request: InsertEventRequest): SimpleResponse =
            eventService.modify(request)

    @PostMapping("deleteEvent")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun deleteEvent(@RequestParam("id") id: Int): SimpleResponse =
            eventService.delete(id)

}