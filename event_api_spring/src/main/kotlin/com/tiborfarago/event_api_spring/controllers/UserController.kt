package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.businesslogic.UserService
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.room.User
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/user")
class UserController constructor(
        private val userService: UserService
) {

    @PostMapping("insertUser")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun insertUser(@RequestBody user: User): SimpleResponse = userService.insert(user)

    @PostMapping("deleteUser")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun deleteUser(@RequestParam("id") id: Int): SimpleResponse = userService.delete(id)

}