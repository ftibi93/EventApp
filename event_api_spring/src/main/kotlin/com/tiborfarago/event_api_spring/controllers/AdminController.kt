package com.tiborfarago.event_api_spring.controllers

import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.resource_module.classes.*
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/admin")
class AdminController constructor(
        private val userService: com.tiborfarago.event_api_spring.businesslogic.UserService
) {

    @PostMapping("login")
    fun login(@RequestBody loginRequest: LoginRequest): LoginResponse = userService.loginAdminWithSpring(loginRequest)

    @PostMapping("getAllLocations")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun getAllLocations(@RequestBody request: LatestIdAndBasicObjectIds): UpdateLocationDetailsResponse =
            userService.allLocations(request)

    @PostMapping("getAllEvents")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun getAllEvents(@RequestBody request: LatestIdAndBasicObjectIds): UpdateDetailsEventsResponse =
            userService.allEvents(request)

    @GetMapping("getAllUsersToEvents")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun getAllUsersToEvents(): UserToEventsResponse = userService.allUserToEvents()

    @GetMapping("getAllUsers")
    @PreAuthorize("hasRole('${UserPrinciple.ADMIN_AUTHORITY}')")
    fun getAllUsers(): UsersResponse = userService.allUsers()

    /*@PostMapping("loginTest")
    fun testLogin(@RequestBody loginRequest: LoginRequest): SimpleResponse {
        val auth = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(
                loginRequest.name,
                loginRequest.password
        ))

        SecurityContextHolder.getContext().authentication = auth

        val jwt = jwtProvider.generateJwtToken(auth)

        return SimpleResponse(jwt)
    }*/
}