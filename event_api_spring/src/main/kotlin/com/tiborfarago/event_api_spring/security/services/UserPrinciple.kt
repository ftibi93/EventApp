package com.tiborfarago.event_api_spring.security.services

import com.fasterxml.jackson.annotation.JsonIgnore
import com.tiborfarago.event_api_spring.database.tables.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class UserPrinciple(
        val id: Long,
        val userName: String,
        @JsonIgnore
        val passwordd: String,
        val authoritiess: MutableCollection<GrantedAuthority> = mutableListOf(SimpleGrantedAuthority(ADMIN_AUTHORITY))
) : UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return authoritiess
    }

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = userName

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = passwordd

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    companion object {
        const val ADMIN_AUTHORITY = "ROLE_ADMIN"

        fun build(user: User): UserPrinciple {
            // TODO filter out the users authorities from the database (there is no such table yet)

            return UserPrinciple(user.id.toLong(), user.name, user.password)
        }
    }
}