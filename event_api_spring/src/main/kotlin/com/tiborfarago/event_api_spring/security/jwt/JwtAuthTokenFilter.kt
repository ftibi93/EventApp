package com.tiborfarago.event_api_spring.security.jwt

import com.tiborfarago.event_api_spring.security.services.UserDetailsServiceImpl
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthTokenFilter : OncePerRequestFilter() {

    @Autowired
    private lateinit var tokenProvider: JwtProvider

    @Autowired
    private lateinit var userDetailsService: UserDetailsServiceImpl

    private val logger2 = LoggerFactory.getLogger(JwtAuthTokenFilter::class.java)

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        try {
            val jwt = getJwt(request)

            if (jwt == null || tokenProvider.validateJwtToken(jwt).not()) {
                filterChain.doFilter(request, response)
                return
            }

            val userName = tokenProvider.getUserNameFromJwtToken(jwt)
            logger2.info(userName)

            if (userName == null) {
                filterChain.doFilter(request, response)
                return
            }

            val userDetails = userDetailsService.loadUserByUsername(userName)

            val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
            authentication.details = WebAuthenticationDetailsSource().buildDetails(request)

            SecurityContextHolder.getContext().authentication = authentication
        } catch (e: Exception) {
            logger2.error("Can NOT set user authentication -> Message: $e")
        }

        filterChain.doFilter(request, response)
    }

    private fun getJwt(request: HttpServletRequest): String? {
        val authHeader: String? = request.getHeader("Authorization") ?: return null
        logger2.info("header $authHeader")

        if (authHeader?.startsWith("Bearer ") == true)
            return authHeader.replace("Bearer ", "")

        return null
    }

}