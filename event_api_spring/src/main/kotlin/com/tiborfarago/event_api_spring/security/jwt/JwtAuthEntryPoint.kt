package com.tiborfarago.event_api_spring.security.jwt

import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthEntryPoint : AuthenticationEntryPoint {
    override fun commence(request: HttpServletRequest?, response: HttpServletResponse?, authException: AuthenticationException?) {
        LoggerFactory.getLogger(JwtAuthEntryPoint::class.java).error("Unauthorized error. Message - ${authException?.message}")
        response?.sendError(HttpServletResponse.SC_UNAUTHORIZED)
    }
}