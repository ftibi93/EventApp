package com.tiborfarago.event_api_spring.security.jwt

import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtProvider {

    private val logger = LoggerFactory.getLogger(JwtProvider::class.java)

    @Value("\${com.tiborfarago.event_api_spring.jwtSecret}")
    private lateinit var jwtSecret: String

    @Value("\${com.tiborfarago.event_api_spring.jwtExpiration}")
    private var jwtExpiration: Int = 0

    fun generateJwtToken(authentication: Authentication): String {
        val userPrincipal = authentication.principal as UserPrinciple

        logger.info("exp $jwtExpiration")
        logger.info("sec $jwtSecret")

        return Jwts.builder()
                .setSubject(userPrincipal.userName)
                .setIssuedAt(Date())
                .setExpiration(Date(Date().time + jwtExpiration * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact()
    }

    fun validateJwtToken(authToken: String): Boolean {
        try {
            LoggerFactory.getLogger("ASD").info("HERE")
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            logger.error("Invalid JWT signature -> Message: {} ", e)
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token -> Message: {}", e)
        } catch (e: ExpiredJwtException) {
            logger.error("Expired JWT token -> Message: {}", e)
        } catch (e: UnsupportedJwtException) {
            logger.error("Unsupported JWT token -> Message: {}", e)
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty -> Message: {}", e)
        }

        return false
    }

    fun getUserNameFromJwtToken(token: String): String? = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .body
            .subject

}