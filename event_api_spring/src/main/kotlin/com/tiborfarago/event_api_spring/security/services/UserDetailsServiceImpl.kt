package com.tiborfarago.event_api_spring.security.services

import com.tiborfarago.event_api_spring.database.repositories.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl constructor(
        private val userRepository: UserRepository
) : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByNameAndPermission(username, 999).firstOrNull()
                ?: throw UsernameNotFoundException("User not found with username: $username")

        return UserPrinciple.build(user)
    }
}