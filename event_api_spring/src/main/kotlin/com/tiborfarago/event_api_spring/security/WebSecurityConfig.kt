package com.tiborfarago.event_api_spring.security

import com.tiborfarago.event_api_spring.database.repositories.UserRepository
import com.tiborfarago.event_api_spring.security.jwt.JwtAuthEntryPoint
import com.tiborfarago.event_api_spring.security.jwt.JwtAuthTokenFilter
import com.tiborfarago.event_api_spring.security.services.UserDetailsServiceImpl
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var unauthorizedHandler: JwtAuthEntryPoint
    @Autowired
    private lateinit var userDetailsService: UserDetailsServiceImpl

    @Bean
    fun authenticationJwrTokenFilter(): JwtAuthTokenFilter = JwtAuthTokenFilter()

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth
                ?.userDetailsService(userDetailsService)
                ?.passwordEncoder(passwordEncoder())
    }

    override fun configure(http: HttpSecurity?) {
        http?.let { http ->
            http.cors().and().csrf().disable()
                    .authorizeRequests()
                    //.antMatchers("/api/admin/**").hasAuthority(UserPrinciple.ADMIN_AUTHORITY)
                    .anyRequest().permitAll()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

            http.addFilterBefore(authenticationJwrTokenFilter(), UsernamePasswordAuthenticationFilter::class.java)
        }
    }
}