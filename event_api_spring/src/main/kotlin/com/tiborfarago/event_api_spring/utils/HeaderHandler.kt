package com.tiborfarago.event_api_spring.utils

import com.tiborfarago.event_api_spring.database.repositories.UserRepository
import org.springframework.stereotype.Component
import java.lang.Exception
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class HeaderHandler constructor(
        private val userRepository: UserRepository
) : Filter {
    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        /*val url = (request as HttpServletRequest?)?.requestURL?.toString()

        if (url == null || url.contains("/event/").not()) {
            chain?.doFilter(request, response)
            return
        }*/

        val userHeaders = try {
            (request as HttpServletRequest?)
                    ?.getHeader("User-Ids")
                    ?.split(",")?.map { it.trim().toInt() }?.toMutableList()
        } catch (e: Exception) {
            null
        }

        if (userHeaders == null) {
            chain?.doFilter(request, response)
            return
        }


        val existingUserIds = userRepository.findAllById(userHeaders).map { it.id }
        userHeaders.removeAll(existingUserIds)

        (response as HttpServletResponse?)?.setHeader("User-Ids", userHeaders.joinToString())

        chain?.doFilter(request, response)
    }
}