package com.tiborfarago.event_api_spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EventApiSpringApplication

fun main(args: Array<String>) {
	runApplication<EventApiSpringApplication>(*args)
}

