package com.tiborfarago.event_api_spring.businesslogic

import com.tiborfarago.event_api_spring.database.repositories.CategoryRepository
import com.tiborfarago.event_api_spring.database.tables.toRoom
import com.tiborfarago.event_api_spring.database.tables.toSpring
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.CategoriesResponse
import com.tiborfarago.resource_module.classes.SimpleResponse
import com.tiborfarago.resource_module.classes.util.NetworkError
import com.tiborfarago.resource_module.room.Category
import com.tiborfarago.resource_module.utils.Result
import org.springframework.stereotype.Service

@Service
class CategoryService constructor(
        private val categoryRepository: CategoryRepository
) {
    fun getAllCategories(): CategoriesResponse = CategoriesResponse(categoryRepository.findAll().map {
        it.toRoom()
    })

    fun insert(category: Category): Result<SimpleResponse> {
        if (categoryRepository.existsById(category.id))
            return Result.Error(NetworkError(ApiConstants.EXISTING_ID))

        categoryRepository.save(category.toSpring())

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun update(category: Category): Result<SimpleResponse> {
        if (categoryRepository.existsById(category.id).not())
            return Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID))

        categoryRepository.save(category.toSpring())

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun delete(id: Int): SimpleResponse {
        categoryRepository.deleteById(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }
}