package com.tiborfarago.event_api_spring.businesslogic

import com.tiborfarago.event_api_spring.database.repositories.LocationRepository
import com.tiborfarago.event_api_spring.database.tables.toRoom
import com.tiborfarago.event_api_spring.database.tables.toSpring
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.classes.util.NetworkError
import com.tiborfarago.resource_module.room.Location
import com.tiborfarago.resource_module.utils.Result
import org.springframework.stereotype.Service

@Service
class LocationService constructor(
        private val locationRepository: LocationRepository
) {
    fun getLocationsBaseUpdate(request: IdWithModDateRequest): UpdateBaseLocationsResponse {
        val locations = locationRepository.getNewLocationsById(request.idWithModDateList.map { it.id }
                .distinct()).map { it.toNewLocation() }

        val notExistingLocations = request.idWithModDateList.map { it.id }.minus(locations.map { it.id })

        val neededLocations = locations.filter { newLocation ->
            newLocation.modDate.millis > request.idWithModDateList.first { locationInRequest -> locationInRequest.id == newLocation.id }.modDate
        }

        return UpdateBaseLocationsResponse(neededLocations, notExistingLocations)
    }

    fun getLocationsDetailsUpdate(request: IdWithModDateRequest): UpdateLocationDetailsResponse {
        val locations = locationRepository.findAllById(request.idWithModDateList.map { it.id }.distinct()).map { it.toRoom() }

        val notExistingLocations = request.idWithModDateList.map { it.id }.minus(locations.map { it.id })

        val neededLocations = locations.filter { location ->
            location.modDate.millis > request.idWithModDateList.first { locationInRequest -> locationInRequest.id == location.id }.modDate
        }

        return UpdateLocationDetailsResponse(neededLocations, notExistingLocations)
    }

    fun insertLocation(location: Location): Result<SimpleResponse> {
        if (locationRepository.existsById(location.id))
            return Result.Error(NetworkError(ApiConstants.EXISTING_ID))

        locationRepository.save(location.toSpring())

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun modifyLocation(location: Location): Result<SimpleResponse> {
        if (locationRepository.existsById(location.id).not())
            return Result.Error(NetworkError(ApiConstants.NOT_EXISTING_ID))

        locationRepository.save(location.toSpring())

        return Result.Success(SimpleResponse(ApiConstants.SUCCESS))
    }

    fun deleteLocation(id: Int): SimpleResponse {
        locationRepository.deleteById(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }

}