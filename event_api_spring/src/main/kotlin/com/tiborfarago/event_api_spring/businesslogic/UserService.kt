package com.tiborfarago.event_api_spring.businesslogic

import com.tiborfarago.event_api_spring.database.repositories.EventRepository
import com.tiborfarago.event_api_spring.database.repositories.LocationRepository
import com.tiborfarago.event_api_spring.database.repositories.UserRepository
import com.tiborfarago.event_api_spring.database.repositories.UserToEventsRepository
import com.tiborfarago.event_api_spring.database.tables.toRoom
import com.tiborfarago.event_api_spring.database.tables.toSpring
import com.tiborfarago.event_api_spring.security.jwt.JwtProvider
import com.tiborfarago.event_api_spring.security.services.UserPrinciple
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.*
import com.tiborfarago.resource_module.classes.util.NetworkError
import com.tiborfarago.resource_module.room.User
import com.tiborfarago.resource_module.utils.Result
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import kotlin.streams.toList

@Service
class UserService constructor(
        private val userRepository: UserRepository,
        private val locationRepository: LocationRepository,
        private val eventRepository: EventRepository,
        private val userToEventsRepository: UserToEventsRepository,
        private val authenticationManager: AuthenticationManager,
        private val jwtProvider: JwtProvider
) {

    @Deprecated("Log in with Spring Security's authentication instead", replaceWith = ReplaceWith("loginAdminWithSpring(loginRequest)", "asd"))
    fun login(loginRequest: LoginRequest): Result<LoginResponse> {
        val user = userRepository.findByName(loginRequest.name)
                .also {
                    if (it.isEmpty()) return Result.Error(NetworkError(ApiConstants.WRONG_USERNAME))
                }
                .parallelStream()
                .filter {
                    BCrypt.checkpw(loginRequest.password, it.password)
                }.toList().firstOrNull() ?: return Result.Error(NetworkError(ApiConstants.WRONG_PASSWORD))

        return Result.Success(LoginResponse(user.permission, 0))
    }

    fun loginAdminWithSpring(loginRequest: LoginRequest): LoginResponse {
        val auth = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(
                loginRequest.name,
                loginRequest.password
        ))

        SecurityContextHolder.getContext().authentication = auth

        val jwt = jwtProvider.generateJwtToken(auth)
        val userPrincipal = auth.principal as UserPrinciple

        return LoginResponse(userPrincipal.id.toInt(), 0, emptyList(), jwt)
    }

    fun allLocations(objects: LatestIdAndBasicObjectIds): UpdateLocationDetailsResponse =
            UpdateLocationDetailsResponse(locationRepository.allAdminLocations(objects.basicObjectIds, objects.latestId).map { it.toRoom() })

    fun allEvents(objects: LatestIdAndBasicObjectIds): UpdateDetailsEventsResponse =
            UpdateDetailsEventsResponse(eventRepository.allAdminEvents(objects.basicObjectIds, objects.latestId).map { it.toRoom() })

    fun allUserToEvents(): UserToEventsResponse = UserToEventsResponse(userToEventsRepository.findAll().map { it.toRoom() })

    fun allUsers(): UsersResponse = UsersResponse(userRepository.findAll().map { it.toRoom() })

    fun userAdminLogin(loginRequest: LoginRequest): LoginResponse {
        val check = userRepository.findByName(loginRequest.name).parallelStream().filter { user ->
            loginRequest.name == user.name && BCrypt.checkpw(loginRequest.password, user.password)
        }.toList().firstOrNull()

        val modDate = check?.modDate?.millis ?: DateTime(1970, 1, 1, 0, 0, 0).millis
        val id = check?.id ?: -1

        val userToEvents = userToEventsRepository.findByUserId(id).map { it.toRoom() }

        return LoginResponse(id, modDate, userToEvents)
    }

    fun insert(user: User): SimpleResponse {
        userRepository.save(user.toSpring())

        return SimpleResponse(ApiConstants.SUCCESS)
    }

    fun delete(id: Int): SimpleResponse {
        userRepository.deleteById(id)

        return SimpleResponse(ApiConstants.SUCCESS)
    }

}