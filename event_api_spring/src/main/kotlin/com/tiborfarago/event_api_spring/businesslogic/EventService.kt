package com.tiborfarago.event_api_spring.businesslogic

import com.tiborfarago.event_api_spring.database.repositories.CategoryRepository
import com.tiborfarago.event_api_spring.database.repositories.EventRepository
import com.tiborfarago.event_api_spring.database.repositories.LocationRepository
import com.tiborfarago.event_api_spring.database.repositories.UserToEventsRepository
import com.tiborfarago.event_api_spring.database.tables.toRoom
import com.tiborfarago.event_api_spring.database.tables.toSpring
import com.tiborfarago.event_api_spring.utils.ResourceNotFoundException
import com.tiborfarago.resource_module.ApiConstants
import com.tiborfarago.resource_module.classes.*
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.io.File

@Service
class EventService constructor(
        private val eventRepository: EventRepository,
        private val locationRepository: LocationRepository,
        private val userToEventsRepository: UserToEventsRepository,
        private val categoryRepository: CategoryRepository
) {
    fun getEventsBaseUpdate(request: IdWithModDateRequest): UpdateBaseEventsResponse {
        val events = eventRepository.getBaseEventsWithIds(request.idWithModDateList.map { it.id }).map { it.toNewEvent() }

        val notExistingEvents = request.idWithModDateList.map { it.id }.minus(events.map { it.id })

        val neededEvents = events.filter { newEvent ->
            newEvent.modDate.millis > request.idWithModDateList.first { eventInRequest -> eventInRequest.id == newEvent.id }.modDate
        }

        return UpdateBaseEventsResponse(neededEvents, notExistingEvents)
    }

    fun getEventsDetailsUpdate(request: IdWithModDateRequest): UpdateDetailsEventsResponse {
        val events = eventRepository.findAllById(request.idWithModDateList.map { it.id }).map { it.toRoom() }

        val notExistingEvents = request.idWithModDateList.map { it.id }.minus(events.map { it.id })

        val neededEvents = events.filter { newEvent ->
            newEvent.modDate.millis > request.idWithModDateList.first { eventInRequest -> eventInRequest.id == newEvent.id }.modDate
        }

        return UpdateDetailsEventsResponse(neededEvents, notExistingEvents)
    }

    fun getNewEvents(latestId: Int): NewEventsResponse {
        val newEvents = eventRepository.getNewEvents(latestId).map { it.toNewEvent() }

        if (newEvents.isEmpty())
            return NewEventsResponse()

        val newLocations = locationRepository.getNewLocationsById(newEvents.map { it.locationId }.distinct()).map { it.toNewLocation() }

        return NewEventsResponse(newEvents, newLocations, userToEventsRepository.findAll().map { it.toRoom() })
    }

    fun getEventDetails(eventId: Int): EventAndLocationDetailsResponse {
        val event = eventRepository.findByIdOrNull(eventId)
                ?: throw ResourceNotFoundException("event")

        return EventAndLocationDetailsResponse(event.toRoom(), event.location.toRoom())
    }

    fun insert(request: InsertEventRequest): SimpleResponse {
        if (eventRepository.existsById(request.event.id))
            throw ResourceNotFoundException(ApiConstants.EXISTING_ID)

        eventRepository.save(request.event.toSpring(categoryRepository, locationRepository))
        userToEventsRepository.saveAll(request.userToEvents.map { it.toSpring() })

        return SimpleResponse(ApiConstants.SUCCESS)
    }

    fun modify(request: InsertEventRequest): SimpleResponse {
        eventRepository.save(request.event.toSpring(categoryRepository, locationRepository))
        userToEventsRepository.saveAll(request.userToEvents.map { it.toSpring() })

        return SimpleResponse(ApiConstants.SUCCESS)
    }

    fun delete(id: Int): SimpleResponse {
        eventRepository.deleteById(id)

        val file = File("${System.getProperty("user.home")}/event_covers", "$id.jpg")
        file.delete()

        return SimpleResponse(ApiConstants.SUCCESS)
    }
}